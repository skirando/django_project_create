#!/usr/bin/env python
# coding:utf-8
"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""


from pathlib import Path
import argparse

import create_django_env.repo_clone as repo_clone
import create_django_env.mysql_init as mysql_init
import create_django_env.install_env as install_env
import create_django_env.conf_dma as conf_dma
import create_django_env.conf_guni as conf_guni
import create_django_env.pstartproject as pstartproject


class PackageParser:

    def __init__(self, commands_list, metavar):
        self.parser = argparse.ArgumentParser(
            description='''commands to create a duplicable Django environment or deploy or duplicate an existing environment for a django project :
                to duplicate an environment use the 3 commands in this order : repo_clone, mysql_init, install_env for dev or test configuration.
                2 additional commands are to be used for production environment : conf_dma and conf_guni
                to create an environment fromscratch use pstartproject command
                ''',
            usage=f'''python3  {Path(__file__).parent} command arguments''',
        )
        self.subparsers = self.parser.add_subparsers(title='subcommands',
                                                     description=f'valid subcommands: {commands_list}',
                                                     help=f"""{commands_list[0:2]} must be called in this order;
                                                     {commands_list[3:-2]} are used only for production environment
                                                     {commands_list[-1]} is_used to create an environment from scratch  
                                                     """,
                                                     )

        self.parser_repo_clone = self.subparsers.add_parser('repo_clone',
                                                            help='clones the repository of the django project, creates the env.cfg file used by settings',
                                                            )
        repo_clone.subparser_add_args(self.parser_repo_clone)

        self.parser_mysql_init = self.subparsers.add_parser('mysql_init',
                                                            help='''creates the mysql database and the admin_user if it does not exist,
                                                            completes the env.cfg
                                                            ''',
                                                            )
        mysql_init.subparser_add_args(self.parser_mysql_init)
        self.parser_install_env = self.subparsers.add_parser('install_env',
                                                             help='''completes the env.cfg file, creates the django admin superuser, makes migrations,
                                                             dispatches the files into the appropriate directories for prod environment,
                                                             finally loads the database if requested
                                                             '''
                                                             )
        install_env.subparser_add_args(self.parser_install_env)

        self.parser_conf_dma = self.subparsers.add_parser('conf_dma',
                                                          help='''configures DragonFlyMailAgent(DMA) for django ''',
                                                          )
        conf_dma.subparser_add_args(self.parser_conf_dma)

        self.parser_conf_guni = self.subparsers.add_parser('conf_guni',
                                                           help='''configures gunicorn for django ''',
                                                           )
        conf_guni.subparser_add_args(self.parser_conf_guni)
        self.parser_pstartproject = self.subparsers.add_parser('pstartproject',
                                                               help='''starts a project from scratch and creates the configuration files required to use this package 
                                                               to duplicate the project into different environments (prod dev or test)''',
                                                               )
        pstartproject.subparser_add_args(self.parser_pstartproject)

    def get_module_parser(self, command):
        return getattr(self, 'parser_' + command)


if __name__ == '__main__':

    PackageParser()




