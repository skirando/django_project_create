#!/usr/bin/env python
# coding:utf-8
"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""
from pathlib import Path
import configparser
import subprocess
import os

DEFAULT_VENV_NAME = 'myvenv'


def get_virtual_env():
    """ returns the environment variable"""

    try:
        venv = os.environ['VIRTUAL_ENV']
        return Path(venv)
    except KeyError:
        return None


def is_same_stat(path1, path2):
    """ return True if path1 and path2 have the same mtime"""
    listofstat = [
        ('st_mode', True),
        ('st_ino', False),
        ('st_dev', False),
        ('st_nlink', False),
        ('st_uid', True),
        ('st_gid', True),
        ('st_size', True),
        ('st_atime', False),
        ('st_mtime', True),
        ('st_ctime', False)
    ]
    return os.stat(path1).st_mtime == os.stat(path2).st_mtime


class PathConfigParser (configparser.ConfigParser):
    """ this class is used only in django_management modules
    in django_settings we use standard configparser class
    so as not to import it"""

    @staticmethod
    def create_from_file(cfg_path, sections_list=None, initvalues=None):
        """create an object config from a file and create default sections"""
        config = PathConfigParser()
        try:
            with open(cfg_path, 'r') as f:
                config.read_file(f)
        except FileNotFoundError:
            raise FileNotFoundError(f'could not find cfg file at {cfg_path}; repo clone is the command to create env.cfg')
        #check that all sections are created
        if sections_list:
            for sect in sections_list:
                try:
                    config.add_section(sect)
                except configparser.DuplicateSectionError:
                    pass
        return config

    def getp(self, section, option):
        """transforms a value into a Path if it is from a paths secion or repo_paths section"""
        value = super().get(section, option)
        if value == '':
            return value
        if section == 'paths' or section == 'repo_paths':
            return Path(value)
        else:
            return value

    def setp(self, section, option, value):
        if section == 'paths' or section == 'repo_paths':
            svalue = str(value)
        else:
            svalue = value
        super().set(section, option, svalue)

    def __init__(self, sections_list=None, options_list=None, default_values={}, **kwargs):
        """ create a Pathconfig from a list of sections, a list of options and kwargs giving initial values"""
        super().__init__()
        if sections_list:
            for sect in sections_list:
                self.add_section(sect)
        if not options_list:
            return
        # set value for options which are in kwargs

        def set_value(koption, value):
            try:
                list = [(section, option) for (section, option) in options_list if option == koption]
                if len(list) > 1:
                    raise ValueError(f'{koption} is ambiguous as it exists in several sections')
                elif len(list) == 0:
                    raise ValueError(f'no section found for {koption}')
                else:
                    section, option = list[0]
                    self.setp(section, option, value)

            except StopIteration as e:
                raise ValueError(f'incorrect value in kwargs : {koption} option does not exist ') from e
        for koption, value in default_values.items():
            set_value(koption, value)

        for koption, value in kwargs.items():
            set_value(koption, value)


def get_parent(directory, filename):
    """ returns the subdirectory of directory which contains a file whose name is filename"""
    if is_under(directory, filename):
        return directory
    res = [child for child in directory.iterdir() if child.is_dir() and
           any(file.is_file() and file.match(filename) for file in child.iterdir())
           ]
    if len(res) > 1:
        print(f'several directories under {directory} contain a  {filename}')
        print(f'check that you are in the project directory')
        return None
    if not res:
        print(f'could not find a parent directory for {filename} in {directory}. check that you are in the project directory ')
        return None
    if res[0].exists:
        return res[0]
    else:
        raise FileNotFoundError(f'cannot find {filename} in {directory}')


def is_under(directory, filename):
    """returns True if directory contains a file whose name is filename"""
    return any(file.is_file and file.match(filename) for file in directory.iterdir())


def get_project_root_repo(base_dir_repo):
    """return the sub directory in base_dir_repo which contains a file settings.py"""
    return get_parent(base_dir_repo, 'settings.py')


def get_base_dir_repo(directory):
    """returns the sub directory in directory which contains a file manage.py"""
    return get_parent(directory, 'manage.py')


def get_cfg_path_repo(project_root):
    return Path(project_root, 'env.cfg')


def check_root():
    import os
    uid = os.getuid()
    if uid != 0:
        print("""for a production environment, or for mysql_init command you must run this python script as root;""")
        print("""type in  su root or sudo """)
        return False
    else:
        return True


def get_pwd(message):
    import getpass
    dbpwd = getpass.getpass(message)
    return dbpwd


def is_empty(dirfile):
    """checks if a dir is empty"""
    if dirfile.is_dir():
        try:
            _ = next(dirfile.iterdir())
        except StopIteration:
            return True
        else:
            return False
    else:
        return False


def call_bash(script, message, errormess,
              okerror=None,
              fail_action=None,
              arguments=[],
              ):
    """ call subprocess to run a script"""
    try:
        completedProcess = subprocess.run(['bash', script],
                                          check=True,
                                          encoding='utf-8',
                                          capture_output=True,
                                          )

        if message:
            print(message)
        return completedProcess.returncode
    except subprocess.CalledProcessError as err:
        if okerror and okerror == err.returncode:
            print(f"error : {err.stderr if err.stderr else ''} in {err.cmd}")
            print(f'arguments : {err.args}')
            if errormess:
                print(f'{errormess}')
            return 0
        if fail_action:
            fail_action(*arguments)
        print(f"error : {err.stderr if err.stderr else ''} in {err.cmd}")
        print(f'arguments : {err.args}')
        if errormess:
            print(f'{errormess}')
        return err.returncode


def update_pip(venv_path):
    """ make sure pip is latest version"""
    print('check that pip is at least v22.2 version')
    try:
        completedProcess0 = subprocess.run([f'{venv_path}/bin/python3', '-m', 'pip', 'install', '--upgrade', 'pip'],
                                           encoding='utf-8',
                                           check=True,
                                           capture_output=True)
        print(completedProcess0.stdout)
        return completedProcess0.returncode
    except subprocess.CalledProcessError as e:
        print('subprocess error for pip install')
        print(e.output)
        print(e.stderr)
        return e.returncode


def call_pip(venv_path, pipargs, store_action=None, wprint=True):
    """ call pip with pipargs as arguments and eventually stores information extracted from completed process output"""
    args = [f'{venv_path}/bin/python3', '-m', 'pip'] + pipargs
    try:
        completedProcess = subprocess.run(args,
                                          encoding='utf-8',
                                          check=True,
                                          capture_output=True)
        if store_action:
            store_action(completedProcess.stdout)
        if wprint:
            print(completedProcess.stdout)
        return completedProcess.returncode
    except subprocess.CalledProcessError as e:
        print('subprocess error for pip install')
        print(e.output)
        print(e.stderr)
        #test if it is a problem with pip that is exit code 1
        return e.returncode


def with_site_packages(venv_path):
    """ return True if the venv alllows access to site-packages"""
    cfg_file = Path(venv_path, 'pyvenv.cfg')
    try:
        with open(cfg_file, 'r') as f:
            lines = f.readlines()
    except FileNotFoundError as e:
        raise ValueError(f'wrong_path for venv: {venv_path}') from e
    myline = next(l for l in lines if l.startswith('include-system-site-packages'))
    #value = myline[-6:-1]
    if 'false' in myline:
        return False
    elif 'true' in myline:
        return True
    else:
        raise ValueError(f'could not read value of with-site-packages in {cfg_file}')


def valid_message(message):
    print(message)
    string = input(f'type Yes to continue or enter to exit command : \n')
    if string[0:1] == 'Y':
        return 0
    else:
        return 1


def file_from_template_OK(path):
    with open(path, 'r') as f:
        lines = f.readlines()

    for line in lines:
        if '{{' in line or '}}' in line:
            return False
    return True


def prepare_for_virtual_env(config, command):
    """prints messages for guiding the user about activating venv"""
    package = Path(__file__).parent
    base_dir = config.getp('repo_paths', 'base_dir')
    venv_path = config.getp('paths', 'venv')
    venvname = config.getp('pnames', 'venvname')
    if config.get('configuration', 'env_type') == 'prod' and command == 'mysql_init':
        print(f'''cd {str(base_dir)}
        sudo {Path(venv_path, 'bin', 'python3')} {package} {command} -h''')
    elif config.get('configuration', 'env_type') == 'prod':
        print(f'''sudo -i
        cd {str(venv_path)}
        source bin/activate
        cd {str(base_dir)}
        {Path(venv_path, 'bin', 'python3')} {package} {command} -h
        ''')
    elif command == 'mysql_init':
        print('activate virtualenv by typing')
        print(f'''cd {str(venv_path.parent)}''')
        print(f'''source {venvname}/bin/activate''')
        print(f'''mysql_init command must be ran as root from virtual env; type in 
        sudo -E {Path(venv_path, 'bin', 'python3')} {package} {command} -h''')
    else:
        print('activate virtualenv by typing if not already activated')
        print(f'''source {venvname}/bin/activate''')
        print(f'''cd {str(base_dir)}''')
        print(f'''{Path(venv_path, 'bin', 'python3')} {package} {command} -h''')


def activate_venv(venv_path):
    """activate a virtual env in the current interpreter"""
    venv_bin = Path(venv_path, 'bin', 'activate_this.py')
    #exec(open(venv_bin).read(), {'__file__': venv_bin})
    with open(venv_bin) as f:
        code = compile(f.read(), str(venv_bin), 'exec')
        exec(code)














