"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL

"""


from pathlib import Path
import shutil as sh
import os
import json

import create_django_env.helpers as h


def subparser_add_args(subparser):
    """ creates the parser"""

    subparser.add_argument('project_name',
                           help="""project name for the dango project; will be used to create the package directory containing manage.py"""
                           )

    subparser.add_argument('--directory',
                           nargs="?",
                           default='DEFAULT',
                           help="""existing directory under which the django project structure will be created;
                           '.' means the current directory will be used to create the project structure;
                           """,
                           )

    subparser.add_argument('--configuration_directory',
                           nargs="?",
                           default='DEFAULT',
                           help="""name for the configuration directory containing settings.py and env.cfg;
                           DEFAULT means the project-name will be used;
                           """,
                           )

    subparser.add_argument('--app_name',
                           nargs="?",
                           default='.',
                           help="""the name of the django application; it will contain the models, forms, tests, fixtures, ...
                           default is app_project_name""",
                           )

    subparser.add_argument('--database_type',
                           nargs="?",
                           default='sqlite',
                           help="""type of the database to use; default is sqlite""",
                           choices=['sqlite', 'mysql']
                           )


def parse_args(subparser, arguments):
    return subparser.parse_args(arguments)


def django_start_project(project, stdout=None):
    """create a custom django project hierarchy"""

    from django.core.management.templates import TemplateCommand

    class StartProject(TemplateCommand):

        def __init__(self, extras, stdout):
            """add extra parameters which will be passed to the template creation context
            only keyword arguments in django Base command
            no __init__ in TemplateCommand"""
            self.extras = extras
            super().__init__(stdout=stdout)

        def handle(self, name, **options):
            options.update(self.extras)
            self.rewrite_template_suffixes = (
                (".py-tpl", ".py"),
                (".txt-tpl", ".txt"),
                (".cfg-tpl", ".cfg"),
            )
            target = options['directory']
            super().handle("project", name, target=target, **options)

    #directory = project.directory
    #Path(project.top_dir, directory).mkdir(exist_ok=True)
    #project_name = project.name
    project.extras.update({'venv_name': project.venv_name,
                           'venv_path': project.venv_path})
    source_dir = project.sourceproj_tmp_matrix_top
    options = {'verbosity': 2,
               'template': str(source_dir),
               'extensions': project.sourceproj_matrix.generated_suffixes,
               }

    command = StartProject(project.extras, stdout=stdout)

    #do not pass base_dir as argument otherwise handle raises an exception because the base_dir already exists
    args = [command, project.name, project.base_dir]
    from django.core import management
    management.call_command(*args,
                            **options
                            )
    return


def django_start_app(project, stdout=None):
    """create a custom django application"""

    from django.core.management.templates import TemplateCommand

    class StartApp(TemplateCommand):

        def __init__(self, extras, stdout=stdout):
            """add extra parameters which will be passed to the template creation context"""
            self.extras = extras
            super().__init__(stdout=stdout)

        def handle(self, name, **options):
            options.update(self.extras)
            target = options['directory']
            super().handle('app', name, target=target, **options)

    project.app_directory.mkdir(exist_ok=True)
    #app_name = project.app_name
    source_dir = project.sourceapp_tmp_matrix_top
    #extras = project.extras
    options = {'verbosity': 2,
               'template': str(source_dir),
               'extensions': project.sourceapp_matrix.generated_suffixes,
               'exclude': []
               }
    command = StartApp(project.extras, stdout=stdout)

    #do not pass base_dir as argument otherwise handle raises an exception because the base_dir already exists
    args = [command, project.app_name, project.app_directory]
    from django.core import management
    management.call_command(*args,
                            **options
                            )
    return


class Project_to_build:
    """describes the project to build"""

    def __init__(self, namespace, cwd):
        """insertion point for the matrix must not be under base_dir
        otherwise Django will detect existing modules and will crash the start project command"""
        self.name = vars(namespace)['project_name']
        self.directory = vars(namespace)['directory']
        try:
            conf_name = vars(namespace)['configuration_directory']
        except KeyError:
            conf_name = vars(namespace)['directory']
        #if self.directory == 'DEFAULT':
            #self.directory = self.name
        if self.directory == '.':
            self.matrixinsert = cwd
        elif self.directory == 'DEFAULT':
            self.matrixinsert = Path(cwd, self.name)

        else:
            self.matrixinsert = Path(self.directory)

        self.base_dir = self.matrixinsert
        #cwd will be used to create venv is necessary
        self.cwd = cwd
        self.base_dir.mkdir(exist_ok=True)

        ##this attribute has 2 names. the short one is used in code
        ##the long one is used in the package project_template
        if conf_name == 'DEFAULT':
            conf_name = self.name

        self.conf_dir = Path(self.base_dir, conf_name)
        self.configuration_directory = self.conf_dir

        self.app_name = vars(namespace)['app_name']
        if self.app_name == 'DEFAULT' or self.app_name == '.':
            self.app_name = 'app_' + self.name
        self.app_directory = Path(self.base_dir, self.app_name)

        self.dbtype = vars(namespace)['database_type']

        #extras contains all data necessary for *-tpl files of the package which will be automatically generated by the startproject command
        #use project root for project_configuration as this is what is used in django documentation
        self.extras = {'project_root': self.conf_dir,
                       #app_name is needed because the first app is automatically generated
                       'app_name': self.app_name,
                       'base_dir': self.base_dir,
                       'dbtype': self.dbtype,
                       'environment': 'development',
                       'conf_name': self.conf_dir.parts[-1],
                       #in dev environment, domain is the IPaddress
                       #'host_ip': h.get_IPaddress(),
                       }
        #in case tasks are required by the user after the command is terminated, they are stored here
        self.post_tasks = []

    def check_directories(self):
        """returns 0 if directories exist """
        if not self.base_dir.exists():
            print(f'directory {self.base_dir} does not exist')
            return 1
        if self.base_dir == self.cwd:
            #check permission to create in parent
            if not os.access(self.base_dir.parent, os.W_OK):
                print(f' cannot create dir in {self.base_dir}')
                return 1
        return 0

    def insert_matrix(self, matrix):
        """ copy the matrix into a temporary file ; django will use this directory as source for the startproject command
        rename files which need to be renamed"""
        source = matrix.top_dir
        dest = Path(self.matrixinsert, matrix.tmp_top_dir_name)
        if matrix.type == 'project':
            self.sourceproj_tmp_matrix_top = dest
            self.sourceproj_matrix = matrix
        elif matrix.type == 'app':
            self.sourceapp_matrix = matrix
            self.sourceapp_tmp_matrix_top = dest
        #remove existing temporary matrix if it exists
        sh.rmtree(dest, ignore_errors=True)
        print(source)
        print(dest)
        sh.copytree(source, dest)
        #rename the filedirs according to attribute names of the matrix
        for filedir in dest.iterdir():
            try:
                thepart = next((part for part in filedir.parts if part in matrix.tobe_renamed))
            except StopIteration:
                continue
            index = filedir.parts.index(thepart)
            newname = getattr(self, thepart).name
            target_path = Path(*filedir.parts[0:index], newname, *filedir.parts[index + 1:])
            filedir.rename(target_path)

        return 0

    def clean(self, matrix, dir_to_clean, projectapp):
        """remove from directory dir_to_clean the files and dirs which are in the matrix and are already copied into the directory
        at this time temporary matrix is not yet inserted therefore must clean temporary matrix and
        the files which might have been installed by a preceding startproject django command"""
        #manage_dir = dir_to_clean.parent
        #manage_path = Path(manage_dir, 'manage.py')
        tmp_top_dir = Path(self.matrixinsert, matrix.tmp_top_dir_name)
        if dir_to_clean.exists() and not h.is_empty(dir_to_clean):
            print(f'{projectapp} directory {dir_to_clean} already exists and is not empty \n')
            message = f'''creating a new {projectapp} over an old one will not update the project
            do you want to delete files from {dir_to_clean}'''
            returncode = h.valid_message(message)
            if returncode != 0:
                return returncode
            if not tmp_top_dir.exists():
                self.insert_matrix(matrix)
            #remove files in the dir_to_clean whose name is in temporary matrix
            #to catch manage.py in project need to look into dir_to_clean.parent
            if projectapp == 'project':
                root = Path(dir_to_clean.parent)
            else:
                root = Path(dir_to_clean)
            for fd in tmp_top_dir.iterdir():
                ptoclean = Path(root, fd.relative_to(tmp_top_dir))
                if ptoclean.exists() and ptoclean.is_dir():
                    sh.rmtree(ptoclean)
                # here look for files in-tpl because generated files have different suffix
                if fd.is_file():
                    if ptoclean.exists() and ptoclean.is_file():
                        ptoclean.unlink()
                    elif fd.suffix in matrix.template_suffixes:
                        genfile = ptoclean.with_suffix(fd.suffix[0:-4])
                        if genfile.exists():
                            genfile.unlink()
        #then_remove temporary matrix
        sh.rmtree(tmp_top_dir, ignore_errors=True)
        return 0

    def get_venv(self):
        """ if virtual env activated use this virtual_env
        else ask if it should be created
        return [boolean, venv_path] boolean is True if venv created, False if existing venv
        None if could not create venv
        create environment variable VIRTUAL_ENV"""
        self.venv_with_with_system = None
        venv_path = h.get_virtual_env()
        if venv_path == None:
            print('no virtual env is activated ')
            message = f'do you want to create a new_one in {self.cwd}?'
            returncode = h.valid_message(message)
            if returncode != 0:
                return None
            venv_path = Path(self.cwd, h.DEFAULT_VENV_NAME)
            os.environ['VENV_PATH'] = str(venv_path)
            script = Path(Path(__file__).parent, 'script_create_venv.bash')
            returncode = h.call_bash(script, f'virtual env {venv_path} created',
                                     f'''could not create the virtual env make sure virtualenv exists in your python environment;
                                     if not do :sudo apt install virtualenv''',
                                     )
            if returncode == 0:
                #activate virtual env
                h.activate_venv(venv_path)
                self.venv_with_system = False
                #self.cfg_lnk = False
                return True, venv_path
            return None

        else:
            print(f'venv (that is virtual env) was found in {venv_path}')
            print(f'you may want to use a separate venv to run your django project rather than this virtualenv')
            message = f'do you want to use {venv_path} for your django project?'
            returncode = h.valid_message(message)
            if returncode != 0:
                return None
            if h.with_site_packages(venv_path):
                self.venv_with_system = True
            ##check that venv is under project.cwd
            #if not venv_path.parent == self.base_dir:
                #self.cfg_lnk = True
            return False, Path(venv_path)

    def save_requirements_from_inspect(self, stdout):
        """ extract requirements from pip inspect stdout"""
        keyword = 'installed'
        self.extract_requirements(stdout, keyword)

    def save_requirements_from_install(self, stdout):
        keyword = 'install'
        self.extract_requirements(stdout, keyword)

    def extract_requirements(self, stdout, keyword):
        report = json.loads(stdout)[keyword]
        self.requirements = {d['metadata']['name']: {'version': d.get('metadata').get('version', None),
                                                     'python_version': d.get('metadata').get('requires_python', None),
                                                     'requires': d.get('metadata').get('requires_dist', None),
                                                     'location': d['metadata_location'],
                                                     'installer': d['installer'],
                                                     'others': d['metadata']}
                             for d in report if d.get('requested')}

        if self.requirements:
            self.django_requirements = {'Django': self.requirements.get('Django', None), }
            del self.requirements['Django']
        else:
            self.django_requirements = {}

    def create_project_requirements(self, test=False):
        """create project requirements and django_requirements
        if django is not installed, install it"""
         # check that django is installed if not install it;  if it is present it must be in the virtualenvv
        returncode = self.install_django(test)
        if returncode != 0 and not test:
            return returncode
        #get project requirements
        pipargs = ['inspect', '--local']
        returncode = h.call_pip(self.venv_path, pipargs, store_action=self.save_requirements_from_inspect, wprint=False)
        if returncode != 0:
            return returncode
        if not self.django_requirements:
            returncode = 1
            return returncode
        else:
            #add requirements to the extras dict so that template variables are accessible for template generation by django command
            self.extras.update({'project_requirements': self.requirements,
                                'django_requirements': self.django_requirements})
            return 0

    def check_venv_with_sites_packages(self):
        try:
            if h.with_site_packages(self.venv_path):
                print(f'''the existing virtual environment {self.venv_path} allows access to system site packages;
                this is not optimal for a duplicable project''')
                print(f'''to make your project duplicable we recommend to have a venv configuration which does not allow  use of
                system site-packages. If you would like to change this :
            open {Path(self.venv_path, 'pyvenv.cfg')}  and set include-system-site-packages to false
            if your project includes python code, you must then run your code and install missing packages in the activated venv;
            missing packages are packages which will raise import error;
            to activate venv : in {Path(self.venv_path).parent } type in source {self.venv_name}/bin/activate
            when your python code runs OK you can call this command again''')
                return 1
            else:
                return 0
        except (ValueError, FileNotFoundError) as e:
            print(e.args)
            return 1

    def create_secret_key(self):
        from django.core.management.utils import get_random_secret_key
        #create secret_keyfile
        secret_key = get_random_secret_key()
        key_file = Path(self.conf_dir, 'secret_key')
        with open(key_file, 'w') as f:
            f.write(secret_key)

    def install_django(self, test=False):
        """ install latest django in virtual_env; """
        """ for tests we want to force exception """
        pipargs = ['install', '--report', '-', 'django']
        dry_run_args = ['--dry-run']
        dry_run = False
        try:
            if not test:
                import django
                print('django already installed')
            else:
                #force for tests thus use dry-run
                dry_run = True
                raise ImportError
        except ImportError:
            #django is not present install it
            print(f'installing django in {self.venv_path}')
            if dry_run:
                args = pipargs + dry_run_args
            else:
                args = pipargs
            returncode = h.call_pip(self.venv_path, args, wprint=False)
            if returncode != 0:
                return returncode
        finally:
            #import django
            try:
                import django
                print('successfully imported django')
                return 0
            except ImportError:
                print(f'cannot import django from {self.venv_path}')
                return 1

    def adjust_config(self):
        import re
        """env.cfg is generated from template adjust it"""
        config = h.PathConfigParser()
        cfg_path = Path(self.configuration_directory, 'env.cfg')
        with open(cfg_path, 'r') as f:
                config.read_file(f)
        venv_parent = self.venv_path.parent
        if venv_parent != self.base_dir:
            #is it necessary that venv_path be under project_directory?
            #if venv_parent != self.cwd:
                #print(f'badly configured {self.venv_path} should be a child of base_dir or top_dir ; base_dir is {self.base_dir} top_dir is {self.cwd}')
                #return 1
            config.setp('repo_paths', 'top_dir', self.cwd)
        #add user_admin and its email
        help = f'''login of the user who will be administrator of the mysql database and a super user of the django application;
                                   this is not the django_user who will be executing the django application'''
        print("user_admin is the " + help)
        user_admin = input('enter user_admin :')
        regex = re.compile(
            r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*"  # dot-atom
            r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-011\013\014\016-\177])*"'  # quoted-string
            r')@(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?$', re.IGNORECASE)  # domain
        email_admin = ''
        while not re.fullmatch(regex, email_admin):
            email_admin = input('enter a correct email for user_admin :')
        config.set('users', 'user_admin', user_admin)
        config.set('users', 'email_admin', email_admin)
        #set pnames
        config.set('pnames', 'venvname', self.venv_path.stem)
        config.set('pnames', 'topname', self.cwd.parts[-1])
        with open(cfg_path, 'w') as f:
            config.write(f)
        return 0

    def check_pip(self):
        """make sure pip.version is above V22.3 if not install it"""
        if self.new_venv:
            return 0
        from importlib.metadata import version
        v = version('pip').split('.')
        if (int(v[0]) < 22) or (int(v[0] == 22 and int(v[1]) < 3)):
            message = '''django_clone_deploy package requires pip V22.3 minimum.
            it can be installed in the virtualenv {project.venv_path}
            to confirm installation type Yes, to exit command type enter'''
            returncode = h.valid_message(message)
            if returncode != 0:
                return
            returncode = h.update_pip(self.venv_path)
            return returncode
        return 0


class Matrix:
    """ describes the directory structure of the application or project as exists in the clone_deploy_django package
    that is all directories and files which must be created by the  django startproject or startapp command
"""

    def __init__(self, package_top_dir, project_or_app):
            self.type = project_or_app
            self.top_dir = Path(package_top_dir, project_or_app + '_template')
            self.tmp_top_dir_name = 'temporary_' + project_or_app + '_template_to_be_deleted'
            self.generated_suffixes = ['.py', '.cfg', '.txt']
            self.template_suffixes = ['.py-tpl', '.cfg-tpl', '.txt-tpl']
            if project_or_app == 'project':
                #this is the name of the directory in the clone_deploy
                #package and of the  attribute of the project instance
                self.tobe_renamed = ['configuration_directory']
            else:
                self.tobe_renamed = []

    def __str__(self):
        return f"""type : {self.type}
        top_dir: {self.top_dir}
        tmp_top_dir_name : {self.tmp_top_dir_name}"""


def mmain(namespace, cwd, stdout=None, test=False):
    """stdout may contain files where stdout are stored"""

    if stdout:
        project_stdout = stdout[0]
        app_stdout = stdout[1]
    else:
        project_stdout = app_stdout = stdout

    project = Project_to_build(namespace, cwd)
    pmatrix = Matrix(Path(__file__).parent, 'project')
    amatrix = Matrix(Path(__file__).parent, 'app')

    #check that base_dir exists
    returncode = project.check_directories()
    if returncode != 0:
        return

    #check right away that the project directory exists otherwise django will yell
    if project.base_dir.exists() and project.base_dir.is_file():
        print(f'project directory {project.dir} is a file; it must be a dir')
        return

    #if directory not empty, delete project_files
    returncode1 = project.clean(pmatrix, Path(project.base_dir, project.name), 'project')
    returncode2 = project.clean(amatrix, project.app_directory, 'application')
    if returncode1 != 0 or returncode2 != 0:
        return

    #insert temporary matrix in project to serve as source for startDjango command
    #rename if necessary
    returncode1 = project.insert_matrix(pmatrix)
    returncode2 = project.insert_matrix(amatrix)
    if returncode1 != 0 or returncode2 != 0:
        return

    # look for a venv or create one;
    venv = project.get_venv()
    if not venv:
        return
    project.new_venv, project.venv_path = venv
    project.venv_name = project.venv_path.stem
    project.extras.update({'venv_path': project.venv_path,
                           'venv_name': project.venv_name})

    #make sure pip is last version if not install it
    returncode = project.check_pip()
    if returncode != 0:
        return

    #check venv with sites packages
    returncode = project.check_venv_with_sites_packages()
    if returncode != 0:
        return

    #at this point all packages for the project are installed by the user and venv is OK
    # except pip upgrade nothing has been installed for clone_deploy
    #thus packages in venv are project requirements + pip
    returncode = project.create_project_requirements(test=test)
    if returncode != 0:
        return

    #call the django startproject command
    django_start_project(project, stdout=project_stdout)

    #call the django startapp command
    django_start_app(project, stdout=app_stdout)

    #temporary matrixes are left in the project delete them at this point
    sh.rmtree(project.sourceproj_tmp_matrix_top)
    sh.rmtree(project.sourceapp_tmp_matrix_top)

    #create symlink to env.cfg if venv is not under base_dir
    #if not project.venv_path.parent == project.base_dir:
        #project.create_cfg_lnk()

    project.create_secret_key()

    #adjust env.cfg
    returncode = project.adjust_config()
    if returncode != 0:
        return

    ##we do not install requirements for clone_deploy_django
    ##they will be automatically installed when clone-deploy_django will be a package

    print(f'successfully created your django project in {project.base_dir} as a development configuration')
    if project.dbtype == 'mysql':
        project.post_tasks.append(f'''you should now create the mysql database: type in {Path(__file__).parent} mysql_init -h ''')
    print(f''' a file containing your project requirements has been created in {Path(project.conf_dir, 'requirements', 'project.txt')}\n
    when duplicating your project either in another development environment or in a production environment, or a test environment
    this requirement file will be used to install all required packages''')
    if project.new_venv:
        print(f'''because a new virtualenv has been created this file is empty; you must add into this file all required packages for your project''')
    else:
        print(f'''please check the validity of this file and remove any unnecessary package.''')
    print(f'''be sure to update this file if you add new packages in the project virtual environment;
    to help you maintain this file uptodate you can use pip list; see pip documentation here https://pip.pypa.io/en/stable/cli/''')

    if project.post_tasks:
        print(f''' you have now {len(project.post_tasks)} task(s) to perform: you can open a new terminal window to perform these tasks
        while keeping this window open''')
        for index, m in enumerate(project.post_tasks):
            print(f'task {index} : {m}')


if __name__ == '__main__':
    """this code is used for test configuration pstartproject_in_SoftLudd21
    the IDE must be set with external terminal windows"""
    from pathlib import Path
    import argparse
    import sys
    dir = Path(__file__).parent.parent
    cwd = Path(dir, 'SoftLudd21_trial', 'Ludd21')
    parser = argparse.ArgumentParser()
    subparser_add_args(parser)
    if cwd.parent.exists():
        #remove directory and recreate it
        sh.rmtree(cwd, ignore_errors=True)
    else:
        parent = cwd.parent
        parent.mkdir()
    cwd.mkdir()
    sys.argv[2:] = ['.', 'trial_project']

    ns = parse_args(parser, sys.argv[2:])

    mmain(ns, cwd)

