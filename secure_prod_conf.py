#!/usr/bin/env python
# coding:utf-8
"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""
from pathlib import Path
import os
import inspect
import shutil

""" this file executes the tasks which are needed to secure the prod configuration when it is running"""

""" 
delete the repository directory?
delete the my manage.py file?
for mysql database run the utilisty to secure the database, chnge the password
create backup tools see tutorial for deploment
"""


def mmain(args, cwd):
    pass


if __name__ == '__main__':
    from pathlib import Path
    cwd = Path.cwd()
    mmain(sys.argv[1:], cwd)



