#!/usr/bin/env python
# coding:utf-8
"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""


import sys
import re
import shutil
from pathlib import Path

import create_django_env.helpers as h
from create_django_env.repo_clone import email as check_mail


def subparser_add_args(subparser):
    """ creates the parser"""

    #subparser.add_argument('out_mail_server',
                           #help=f"""the name of the outgoing mail server for example smtp.orange.fr; it is avisable to enter the
                           #the outgoing server of the email account you entered in the repo_colne command""",
                           #)

    subparser.add_argument('email_host_user',
                           help=f"""the user account registered in the outmailserver entered when you installed dma package; the outmailserver is the outgoing mail server for example smtp.orange.fr; 
                           it is advisable to enter the outgoing server of the email account you entered in the repo_clone command (email-admin)"""
                           )

    subparser.add_argument('security_conf',
                           help='the security parameters for the outgoing mail server STARTTLS or SSL/TLS; you can check into your mail server client if necessary',
                           choices=['STARTTLS', 'SSL/TLS']
                           )


def parse_args(subparser, arguments):
    return subparser.parse_args(arguments)

#def parse_args(subparser, arguments):
    #args = subparser.parse_args(arguments)
    #return args


def get_value_in_file(path, string):
    """ gets the value of a parameter in a config file ,
    value is the string following string in the first line containing string
    if string is '' return the content of the first line"""
    try:
        with open(path, 'r') as f:
            if not string:
                return f.readline().strip(' ').strip('\n')
            line = f.readline()
            while line:
                match = re.search(string, line)
                if line and match:
                    substring = slice(match.span()[1], -1)
                    value = line[substring].strip(' ').strip('\n')
                    return value
                line = f.readline()
            return None
    except FileNotFoundError:
        print(f'cannot open file {path}')
        return None


def replace_email(config, mailname):
    email = config.get('users', 'email_admin')
    print(f""" you provided {mailname} as the host name and you entered {email} as email for the django admin;
    this will not be accepted by you outgoing mail server;""")
    okemail = False
    while not okemail or mailname == email.split('@')[1]:
        string = input(f'please provide another email with a domain name that is not {mailname}:\n')
        try:
            email = check_mail(string)
            okemail = True
        except:
            continue
    return email


def create_dma_section(config, out_mail_server, security_conf, mailname, email_host_user):
    try:
        config.add_section('dma')
    except:
        """ is this the right choice?"""
        pass
    email = config.get('users', 'email_admin')
    domain = config.get('host', 'domain')
    dns = config.getboolean('host', 'dns')
    config.set('dma', 'email_host_user', email_host_user)

    #email with same domain name as provider cannot be recipient
    if mailname == email.split('@')[1]:
        newemail = replace_email(config, mailname)
        config.set('users', 'email_admin', newemail)

    if not dns:

        config.set('dma', 'default_from_email', email_host_user)
        config.set('dma', 'server_email', email_host_user)
    else:
        config.set('dma', 'default_from_email', f'noreply@{domain}')
        config.set('dma', 'server_email', f'noreply@{domain}')

    config.set('dma', 'email_host', out_mail_server)

    if security_conf == 'STARTTLS':
        port = str(587)
        email_use_TLS = str(True)
        email_use_SSL = str(False)
    else:
        port = str(465)
        email_use_TLS = str(False)
        email_use_SSL = str(True)

    config.set('dma', 'email_port', port)
    config.set('dma', 'email_use_tls', email_use_TLS)
    config.set('dma', 'email_use_ssl', email_use_SSL)
    return True


def create_config_file(path, string):
    with open(path, 'w') as f:
        f.write(string + '\n')


def update_conf_file(file, dic):
    """update a configuration file using dic content:
    - key starting with # : replace the line starting with key by a new line containing value
                            if value is '' just remove the #
    - key = "append" : append value at the end of the config file
    - key = "insert" : value is a tuple; value[1] is a line to insert in the file
                        after the last line containing value[0]
    - key starting with $ : replace the string $key in the line with value
    """

    text = file.read()
    lines = text.split('\n')
    for key, value in dic.items():
        if key[0] == "#":
            try:
                index, line = next((index, line) for (index, line) in enumerate(lines) if re.match(key, line))
                if value:
                    lines[index] = value
                else:
                    lines[index] = line[1:]
            except:
                continue
        elif key == 'append':
            lines.append(value)
        elif 'insert' in key and isinstance(value, tuple):
            tuples = [(index, line) for (index, line) in enumerate(lines) if re.search(value[0], line)]
            #there are several commented lines in the original conf file so take the last one
            index, text = tuples[-1]
            #insert new line after only if it is commented line
            if text[0:1] == '#':
                lines.insert(index + 1, value[1])
            else:
                #replace last line with new one
                lines[index] = value[1]
        elif key[0] == '$':
            clines = lines.copy()
            lines = list(map(lambda line: line.replace(key, value), clines))
        else:
            print(f""" coud not modify file {path} according to {key} : {value}""")
            return
    newtext = ('\n').join(lines)
    return newtext


def get_etc(parts, first=False):
    """dma files do not go under project name configuration directory but directly under /etc"""
    if first and not Path('/', 'etc', 'dma').exists() and not Path('/', 'etc', 'dma').is_dir():
        return None
    else:
        return Path('/', 'etc', *parts)


def get_settings_module_cfg_path_function(project_root_repo):
    """ to look for the cfg_path in the prod environment"""
    import importlib
    spec = importlib.util.spec_from_file_location('settings', Path(project_root_repo, 'settings.py'))
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    function = module.get_cfg_path
    return function


def masqerade(config):
    """ the masquerade included in the dma.conf file
    this masquerade is used by dma to create the envelope from field of the mail
    need a user name to replace the user root"""
    email = config.get('users', 'email_admin')
    return email.split('@')[0] + '@'


def handle_dma_conf_file(config):
    """modifies dma_conf file"""
    dma_conf_path = get_etc(['dma', 'dma.conf'])
    orig_conf_path = get_etc(['dma', 'orig_dma.conf'])
    if (not orig_conf_path.exists() or
                (orig_conf_path.exists() and dma_conf_path.exists() and (orig_conf_path.stat().st_mtime < dma_conf_path.stat().st_mtime))
        ):
        #a new version of dma was installed or reconfigured
        shutil.copyfile(dma_conf_path, orig_conf_path)
        shutil.copystat(dma_conf_path, orig_conf_path)

    dic = {"#PORT": f'PORT {config.get("dma","email_port")}',
           "#ALIASES": '',
           "#AUTHPATH": '',
           "#SECURETRANSFER": '',
           "insert": ('MASQUERADE', f'MASQUERADE {masqerade(config)}'),
           }
    email_use_TLS = config.get('dma', 'email_use_TLS')
    if email_use_TLS == 'True':
        dic.update({"#STARTTLS": ''})
    with open(orig_conf_path, 'r') as f:
        text = update_conf_file(f, dic)
    with open(dma_conf_path, 'w') as f:
        f.write(text)


def write_pwd_file(config, out_mail_server):
    pwd = h.get_pwd(f'type in the email_account password for {out_mail_server}:')
    pwd_path = Path(config.get('paths', 'conf'), 'emailpwd.txt')
    with open(pwd_path, 'w') as f:
        f.write(pwd)
    return pwd


def mmain(namespace, cwd):
    email_host_user, security_conf = vars(namespace).values()
    if not h.check_root():
        return
    #check Dma is installed and parameters are coherent with args input
    from shutil import which
    if not which('dma'):
            print(f"""it seems that dma (DragonFly Mail Agent) is not installed\
            to install it : type sudo apt install dma
            it will ask you 2 parameters.  it is advisable to enter the outgoing server of the email account you entered in the repo_clone command (email-admin)
            the 2 prameters required are :
            -the system mail name  that is, the domain name that dma will use to identify the host.
              it could be your domain name or the domain name of your mail outgoing server (i.e orange.fr)
            - the smarthost (also called relayhost) which can be the outgoing mail server of your internet access provider (such as smtp.orange.fr)
              or your domain name 
            for more information about dma package see in documentation about dma configuration
            """)
            return
    else:
        dma_conf_path = get_etc(['dma', 'dma.conf'], first=True)
        if not dma_conf_path:
            print(f"""dma directory does not exist or has been destroyed; remove dma software sudo apt --purge autoremove dma;
            then reinstall it : sudo apt install dma""")
        mailname_path = get_etc(['mailname'])
        #if not check_value_in_file(dma_conf_path, f'{out_mail_server}'):
            #raise ValueError(f'file {dma_conf_path} does not contain the out_mail_server {out_mail_server}')
        out_mail_server = get_value_in_file(dma_conf_path, 'SMARTHOST')

        if not out_mail_server:
            print(f"""dma software is installed but configuration file {dma_conf_path} which has been created by dma
            does not contain the outmail server (SMARTHOST) ; type in dpkg-reconfigure dma to change the initial values""")
            return
        if mailname_path.exists() and mailname_path.is_file():
            dma_mailname = get_value_in_file(mailname_path, '')
        else:
            print(f"""dma software is installed but configuration file {mailname_path} which should have been created
            by dma does not exist, you might need to reconfigure dma :type in dpkg-reconfigure dma to creconfigure dma""")
            return
        if not dma_mailname:
            print(f"""dma software is installed but configuration file {mailname_path} created by DMA is incorrect; 
            you might need to reconfigure dma : type in dpkg-reconfigure dma and re-enter the values""")
            return

    #in this command the files were dispatched into the different repo; in production environment it is different from the repo env.cfg
    #import the get_cfg_path function from the settings in the repo to find the real env.cfg file.
    base_dir = h.get_base_dir_repo(cwd)
    if not base_dir:
        return
    project_root = h.get_project_root_repo(base_dir)
    if not project_root:
        return
    function = get_settings_module_cfg_path_function(project_root)
    cfg_path, _ = function()
    config = h.PathConfigParser()
    try:
        with open(cfg_path, 'r') as f:
            config.read_file(f)
    except FileNotFoundError:
        print(f'cannot read config file : {cfg_path}')
        return

    #update config but do not write it
    res = create_dma_section(config, out_mail_server, security_conf, dma_mailname, email_host_user)
    if not res:
        return

    pwd = write_pwd_file(config, out_mail_server)
    handle_dma_conf_file(config)

    email = config.get('users', 'email_admin')
    dma_alias_path = get_etc(['aliases'])
    create_config_file(dma_alias_path, f'root: {email}')

    dma_auth_path = get_etc(['dma', 'auth.conf'])
    string = f'{email_host_user}|{out_mail_server}:{pwd}'
    create_config_file(dma_auth_path, string)

    openssl_path = get_etc(['ssl', 'openssl.cnf'])
    dic = {"insert": ('#.include', 'openssl_conf= default_conf')}

    with open(Path(Path(__file__).parent, 'prod_files', 'dma_files', 'openssl.cnf'), 'r') as f:
        to_append = f.read()
    with open(openssl_path, 'r') as f:
        text = update_conf_file(f, dic)
        newtext = text + to_append
    with open(openssl_path, 'w') as f:
        f.write(newtext)

    #here everything is OK so write env.cfg
    print(f'in confdma write config to {cfg_path}')
    print(f''' dns is {config.get('host', 'dns')}''')
    with open(cfg_path, 'w') as configfile:
        config.write(configfile)

    #check that email is correctly configured
    print(f"""check that dma is correctly configured : in terminal window type
    sendmail {email}
    From: {config.get('dma', 'email_host_user')}
    To: {email}
    Subject : some text
    type in a message and terminate with a full stop in a single line. check that email arrived
    in case of problem : sudo journalctl -r contains information as well as /var/log/mail.log  or var/log/mail.err
    
    to check that django correctly interfaces with dma : create an error in the django app(rename template or anything alike)
    reload django : sudo systemctl restart {config.get('pnames', 'project_name')}
    admin should receive an email""")


if __name__ == '__main__':

    cwd = Path.cwd()
    mmain(sys.argv[1:], cwd)



















