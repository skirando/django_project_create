#!/usr/bin/env python
# coding:utf-8

"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""


import shutil as sh
from pathlib import Path
import os
from argparse import Namespace

import unittest
from unittest.mock import patch, Mock


from tests.tests_super_class import TestSuperClass
import pstartproject as m
import create_django_env.helpers as h


"""This test_module must be run with an activated_venv which does not contain django that is venv_for_tests_myvenv_no_django
"""
"""it is not completely operational and therefore is skipped"""


@unittest.skip
class Test_Integration_venv_outside_no_django (TestSuperClass, unittest.TestCase):
    """test create project, insertmatrix and startcommands with an existing venv which is not under base_dir"""

    @ classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)
        cls.data = Path(cls.datadir, cls.__name__)
        cls.venv_path = Path(Path(cls.datadir).parent, 'tests_pstartproject', 'venv_for_tests', 'myvenv_no_django')

    def test(self):
        """check that env.cfg symlink is created """
        name = self.id().split('.')[-1]
        cwd = Path(self.data, name)

        dic = {'project_name': 'project_name',
               'directory': 'directory',
               'app_name': 'appname',
               'database_type': 'sqlite'
               }

        #remove leftovers from precedent test_run
        for tree in self.data.iterdir():
            if tree.exists() and tree.is_dir():
                sh.rmtree(tree, ignore_errors=True)
            if tree.is_file():
                tree.unlink()
        Path(self.venv_path, 'env.cfg').unlink(missing_ok=True)

        stdfile_project = open(str(Path(self.data, 'stdout_create_project')), 'w')
        stdfile_application = open(str(Path(self.data, 'stdout_create_app')), 'w')
        stdout = (stdfile_project, stdfile_application)
        cwd.mkdir()

        with patch('create_django_env.helpers.valid_message', return_value=0):
            m.mmain(Namespace(**dic), cwd, stdout=stdout)

        #check that requirements can be installed
        reqpath = Path(cwd, 'directory', 'project_name', 'requirements', 'dev.txt')
        pipargs = ['install', '-r', str(reqpath)]
        returncode = h.call_pip(self.venv_path, pipargs, wprint=False)
        self.assertTrue(returncode == 0)

        ##check that env.cfg symlink exists
        #self.assertTrue(Path(self.venv_path, 'env.cfg').exists())
        #self.assertTrue(Path(self.venv_path, 'env.cfg').is_symlink())

        #manually launch manage.py in test/directory using runserver


if __name__ == '__main__':
    import unittest
    import importlib
    current = Path(__file__)
    module = importlib.import_module((current.stem))

    # This orders the tests to be run in the order they were declared.
    # It uses the unittest load_tests protocol.
    #load_tests = load_ordered_tests
    loader = unittest.TestLoader()
    tests_suite = loader.loadTestsFromModule(module)
    testRunner = unittest.runner.TextTestRunner()
    testRunner.run(tests_suite)
