#!/usr/bin/env python
# coding:utf-8
"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL

Django's command-line utility for administrative tasks.
This file is used to temporarly launch django with manage.py in a prod configuration
it should be deleted to secure the prod configuration"""

import os
import sys
import pprint
from pathlib import Path
import configparser


def get_cfg_path():
    """returns the path of the env.cfg file"""
    virtual_env_path = os.environ.get('VIRTUAL_ENV')
    if not virtual_env_path:
        error_message = (f'you are not in a virtual environment ;\n'
                         f'to activate virtual env type;\n'
                         f'source <nameofyourvenv>/bin/activate \n'
                         )
        raise ValueError(error_message)
    #try prod configuration : symlink to env.cfg is under venv directory
    cfg_path = Path(virtual_env_path, 'env.cfg')
    if cfg_path.is_symlink():
        """ a symlink towards the env.cfg file has been created
			check that it is a production environment"""
        return cfg_path.resolve(), 'prod'
    else:
        parent = Path(virtual_env_path).parent
        if is_under(parent, 'env.cfg'):
            cfg_path = Path(parent, 'env.cfg')
            return cfg_path, 'dev'
        if is_under(parent, 'manage.py'):
            # normal configuration parent is base_dir, env.cfg in under project_root
            cfg_path_dir = get_parent(parent, 'env.cfg')
            if cfg_path_dir:
                return Path(cfg_path, 'env.cfg'), 'dev'
        else:
            #top_dir configuration
            base_dir = get_parent(parent, 'manage.py')
            if base_dir:
                cfg_dir = get_parent(base_dir, 'env.cfg')
                if cfg_dir:
                    return Path(cfg_dir, 'env.cfg'), 'dev'
        raise FileNotFoundError(f'could not find env.cfg under {parent}')


def get_parent(directory, filename):
    """ returns the subdirectory of directory which contains a file whose name is filename"""
    if is_under(directory, filename):
        return directory
    res = [child for child in directory.iterdir() if child.is_dir() and
           any(file.is_file() and file.match(filename) for file in child.iterdir())]
    if len(res) > 1:
        print(f'several directories under {directory} contain a  {filename}')
        print(f'check that you are in the project directory')
        return None
    if not res:
        print(f'could not find a parent directory for{filename} in {directory}. check that you are in the project directory ')
        return None
    if res[0].exists:
        return res[0]
    else:
        raise FileNotFoundError(f'cannot find {filename} in {directory}')


def is_under(directory, filename):
    """returns True if directory contains a file whose name is filename"""
    return any(file.is_file and file.match(filename) for file in directory.iterdir())


def main():
    """Run administrative tasks."""
    cfg_path, _ = get_cfg_path()
    config = configparser.ConfigParser()
    with open(cfg_path, 'r') as f:
        config.read_file(f)
    BASE_CONF = Path(config.get('paths', 'conf'))
    BASE_PROGS = Path(config.get('paths', 'progs'))
    sys.path.append(str(BASE_CONF))
    sys.path.append(str(BASE_PROGS))

    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
