#!/usr/bin/env python
# coding:utf-8

"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""


import shutil as sh
from pathlib import Path
import os
from argparse import Namespace

import unittest
from unittest.mock import patch, Mock


from tests.tests_super_class import TestSuperClass
import pstartproject as m
import create_django_env.helpers as h


"""This test_module must be run with no activated venv
first test creates one and install django in it"""

from tests.tests_super_class import load_ordered_tests
load_tests = load_ordered_tests

"""this text is not operational and therefore is skipped"""


@unittest.skip
class Test_Integration_no_venv (TestSuperClass, unittest.TestCase):
    """test create project, insertmatrix, create_venv and startcommands
    """

    @ classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)
        cls.data = Path(cls.datadir, cls.__name__)

    @patch('pstartproject.print')
    def test(self, mockprint):
        """check that project requirements allow installing of required packages for duplicating configuration """
        name = self.id().split('.')[-1]
        cwd = Path(self.data, name)

        dic = {'project_name': 'project_name',
               'directory': 'directory',
               'app_name': 'appname',
               'database_type': 'sqlite',
               'configuration_directory': 'confname',
               }

        #remove leftovers from precedent test_run
        for tree in self.data.iterdir():
            if tree.exists() and tree.is_dir():
                sh.rmtree(tree, ignore_errors=True)
            if tree.is_file():
                tree.unlink()
        #self.venv_path = Path(Path(self.datadir).parent, 'tests_pstartproject', 'venv_for_tests', 'myvenv')

        stdfile_project = open(str(Path(self.data, 'stdout_create_project')), 'w')
        stdfile_application = open(str(Path(self.data, 'stdout_create_app')), 'w')
        stdout = (stdfile_project, stdfile_application)
        cwd.mkdir()

        #because created venv cannot be activated within the test, requirements cannot be read
        #thus chek only that requirements can be installed
        with patch('create_django_env.helpers.valid_message', return_value=0):
            m.mmain(Namespace(**dic), cwd, stdout=stdout, test=True)

        reqpath = Path(cwd, 'directory', 'project_name', 'requirements', 'dev.txt')
        pipargs = ['install', '-r', str(reqpath)]
        venv_path = Path(cwd, 'myvenv')
        returncode = h.call_pip(venv_path, pipargs, wprint=False)
        self.assertTrue(returncode == 0)
        managepath = Path(cwd, 'directory', 'manage.py')
        #manually launch the generated manage.py in test/directory with runserver1 launch configuration
        #exec(f'python3 {str(managepath)} runserver')


if __name__ == '__main__':
    import importlib
    import unittest
    current = Path(__file__)
    module = importlib.import_module((current.stem))

    # This orders the tests to be run in the order they were declared.
    # It uses the unittest load_tests protocol.
    #load_tests = load_ordered_tests
    loader = unittest.TestLoader()
    tests_suite = loader.loadTestsFromModule(module)
    testRunner = unittest.runner.TextTestRunner()
    testRunner.run(tests_suite)

