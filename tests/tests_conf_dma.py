#!/usr/bin/env python
# coding:utf-8
"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""

import unittest
from unittest.mock import patch
import argparse
import shutil
from pathlib import Path

from tests.tests_super_class import TestSuperClass
import helpers as h
import conf_dma


""" this test module is partial coverage for the modules in django_create_project"""


class Test_files_function (TestSuperClass, unittest.TestCase):

    def setUp(self):
        self.dic = {'$Dragon': 'myDragon',
                    '$TOTO': 'this Value',
                    '# NOTE': 'NOTE: this was uncommented',
                    '#Please': '',
                    '#NOTPresent': '',
                    'insert0': ('comment', 'text to be inserted'),
                    'append': 'appended line \n another appended line \n',
                    'insert1': ('MASQUERADE', 'MASQUERADE line replaced'),
                    }
        self.get_dirs(__file__)
        self.conf_file = Path(self.datadir, 'test_functions', 'update_conf_file_input')
        self.result_file = Path(self.datadir, 'test_functions', 'shouldbe')

    def test_update_config_file(self):
        test_file = Path(self.datadir, 'test_functions', 'test-file')
        shutil.copyfile(self.conf_file, test_file)

        with open(test_file, 'r') as f:
            result_lines = conf_dma.update_conf_file(f, self.dic)

        with open(self.result_file, 'r') as f:
            shouldbe_lines = f.read()

        self.assertEqual(shouldbe_lines, result_lines)

    def test_get_value_in_file(self):

        value = conf_dma.get_value_in_file(self.conf_file, '#this')
        value2 = conf_dma.get_value_in_file(self.conf_file, '')

        self.assertEqual(value, 'line not to be removed there is a $DragonFly')
        self.assertEqual(value2, '$DragonFly: src  matthias Exp $TOTO')


class Test_dma_config (TestSuperClass, unittest.TestCase):

    @ classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)

    def test_dma_config(self):
        out_mail_server = 'smtp.orange.fr'
        security_conf = 'SSL/TLS'
        mailname = 'orange.fr'
        email_host_user = 'test@orange.fr'
        config = h.PathConfigParser()
        #uses env.cfg from tests_data/fake_repository do not delete
        with open(Path(self.project_root, 'env.cfg'), 'r') as f:
            config.read_file(f)
        config.remove_section('dma')

        with patch('conf_dma.replace_email', return_value='email@laposte.net'):
            res = conf_dma.create_dma_section(config, out_mail_server, security_conf, mailname, email_host_user)

        self.assertTrue(res)
        email = config.get('users', 'email_admin')
        dic = dict(config.items('dma'))
        shouldbe = {'host': '1.2.3.4',
                    'server_email': 'test@orange.fr',
                    'default_from_email': 'test@orange.fr',
                    'email_host_user': 'test@orange.fr',
                    'email_host': 'smtp.orange.fr',
                    'email_use_tls': str(False),
                    'email_use_ssl': str(True),
                    'email_port': str(465)
                    }
        for key, value in dic.items():
            with self.subTest(key=key):
                self.assertEqual(dic[key], shouldbe[key])

    @ classmethod
    def tearDownClass(cls):
        pass


if __name__ == '__main__':
    import unittest

    unittest.main()
