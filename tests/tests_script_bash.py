#!/usr/bin/env python
# coding:utf-8
"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL

this module tests bash scripts

"""

import unittest
from unittest.mock import patch
import os
from pathlib import Path
import getpass
import subprocess
import grp
import shutil as sh

from helpers import call_bash
from tests.tests_super_class import TestSuperClass
"""these test require an outside console
 in wing go to edit preferences in debug I/O choose external terminal window and consoles wait on exit
this second one is necessary to see the tests results
this test must be ran without any virtual env.
and ?in debug advanced uncheck use sys.stdin wrapper?
it is difficult to run all tests sequentially"""


class TestBashScript(TestSuperClass, unittest.TestCase):

     @classmethod
     def setUpClass(cls):
          cls.get_dirs(cls, __file__)
          cls.script_path = Path(__file__).parent.parent
          cls.dic = {}
          cls.script_name = ''
          cls.mess1 = f'message OK {cls.script_name}'
          cls.mess2 = f'message NOK {cls.script_name}'

          cls.okerror = None,
          cls.fail_action = None,
          cls.arguments = None
          cls.cwd = cls.datadir
          cls.user = getpass.getuser()

     def call_script_wrapper(cls):
          environs = cls.dic
          os.environ.update(environs)

          script = Path(cls.script_path, cls.script_name)
          return_code = call_bash(script, cls.mess1, cls.mess2)
          return return_code


#@unittest.skip
class Test_venv_related_bash_scripts (TestBashScript):

     @classmethod
     def setUpClass(cls):
          super().setUpClass()
          cls.venv_path_dev = Path(cls.cwd, 'myvenv_dev')

     def setUp(self):
          if self.venv_path_dev.exists() and self.venv_path_dev.is_dir():
               sh.rmtree(self.venv_path_dev, ignore_errors=True)

     def test0_ok_dev(self):
          self.dic = {#'DJANGO_USER': '',
                      #'ENV': 'dev',
                      'VENV_PATH': str(self.venv_path_dev),
                      #'REQUIREMENTS_PATH': str(self.first_req_dev)
          }
          self.script_name = 'script_create_venv.bash'

          return_code = self.call_script_wrapper()

          self.assertEqual(return_code, 0)
          self.assertTrue(self.venv_path_dev.exists())
          self.assertTrue(self.venv_path_dev.is_dir())
          self.assertEqual(self.venv_path_dev.owner(), self.user)


@unittest.skip
class Test_create_django_user_bash (TestBashScript):

     def setUp(self):
          self.djangouser = 'django_user'
          self.script_name = 'script_create_django_user.bash'
          self.home = Path(self.cwd, 'djangouser_data')
          self.dic = {'DJANGO_USER': self.djangouser,
                      #'ENV': 'dev',
                      #'HOMEUSER': str(self.home),
                      }
          _ = subprocess.run(['sudo', 'userdel', self.djangouser])
          if self.home.exists() and self.home.is_dir():
               sh.rmtree(self.home, ignore_errors=True)

     @ patch('builtins.print')
     def test0_ok(self, mockprint):

          return_code = self.call_script_wrapper()

          self.assertEqual(return_code, 0)
          # will raise an exception if user does not exist"
          grp.getgrnam(self.djangouser)
          _ = subprocess.run(['sudo', 'userdel', self.djangouser])
          if self.home.exists() and self.home.is_dir():
               sh.rmtree(self.home, ignore_errors=True)

     def test1_ok_user_exists(self):
          # create the user
          _ = self.call_script_wrapper()

          return_code = self.call_script_wrapper()

          self.assertEqual(return_code, 0)
          grp.getgrnam(self.djangouser)
          _ = subprocess.run(['sudo', 'userdel', self.djangouser])
          if self.home.exists() and self.home.is_dir():
               sh.rmtree(self.home, ignore_errors=True)


@unittest.skip
class Test_create_git_clone_bash (TestBashScript):
     """this test must be ran with an outside console so as to enter a password"""

     def setUp(self):
          self.djangouser = 'django_user'
          self.project_name = 'test_project'
          self.script_name = 'script_git_clone.bash'
          self.dic = {'URL': 'https://gitlab.com/skirando/family-recipes',
                      'PRONAME': self.project_name}
          os.chdir(self.cwd)
          self.result = Path(self.datadir, self.project_name)
          sh.rmtree(self.result, ignore_errors=True)

     def test0_ok(self):

          return_code = self.call_script_wrapper()

          self.assertTrue(self.result.exists())
          self.assertTrue(self.result.is_dir())

     def tearDown(self):
          sh.rmtree(self.result, ignore_errors=True)


if __name__ == '__main__':
     #import wingdbstub
     #import pprint
     #import sys
     #sys.path.append(Path(__file__).parent.parent)
     #pprint.pprint(sys.path)
     unittest.main()




