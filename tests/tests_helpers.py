#!/usr/bin/env python
# coding:utf-8

"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""

from pathlib import Path
import subprocess
import shutil

from unittest.mock import patch
import unittest

from tests.tests_super_class import TestSuperClass
import helpers as h

""" this module is to be ran with no virtual_env activated"""


class Test_PathConfigParser(TestSuperClass, unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)
        cls.data = Path(cls.datadir, cls.__name__)
        cls.file = Path(cls.data, 'env1.cfg')

    def setUp(self):
        """ create empty config"""
        self.sections_list = ['users', 'repo_paths']
        self.options_list = [('users', 'django_user'),
                             ('repo_paths', 'base_dir'),
                             ]
        self.myconfig = h.PathConfigParser(self.sections_list, self.options_list)

    def test_init_fromkwargs(self):
        kwargs = {'django_user': 'toto_user',
                  'base_dir': self.data}

        otherconfig = h.PathConfigParser(self.sections_list, self.options_list, **kwargs)

        self.assertEqual(otherconfig['users']['django_user'], 'toto_user')
        self.assertEqual(otherconfig['repo_paths']['base_dir'], str(self.data))
        self.assertEqual(otherconfig.getp('repo_paths', 'base_dir'), self.data)

    def test_init_from_kwargs_error(self):
        kwargs = {'django_users': 'toto_user'}

        with self.assertRaises(ValueError):
            otherconfig = h.PathConfigParser(self.sections_list, self.options_list, **kwargs)

    def test_init_from_kwargs_ambiguous(self):
        kwargs = {'django_user': 'toto_user',
                  'base_dir': self.data}
        sections_list = self.sections_list + ['paths']
        initvalues = self.options_list + [('paths', 'base_dir')]

        with self.assertRaises(ValueError):
            otherconfig = h.PathConfigParser(sections_list, initvalues, **kwargs)

    def test_init_default_values(self):
        self.options_list.append(('repo_paths', 'extra_name'))
        kwargs = {'django_user': 'toto_user',
                  'base_dir': self.data,
                  }
        default_values = {'extra_name': ''}
        otherconfig = h.PathConfigParser(self.sections_list, self.options_list, default_values=default_values, **kwargs)

        self.assertEqual(otherconfig.get('repo_paths', 'extra_name'), '')

    def test_init_with_default_values(self):
        self.options_list.append(('repo_paths', 'extra_name'))
        kwargs = {'django_user': 'toto_user',
                  'base_dir': self.data,
                  'extra_name': 'extra'
                  }
        default_values = {'extra_name': ''}
        otherconfig = h.PathConfigParser(self.sections_list, self.options_list, default_values=default_values, **kwargs)

        self.assertEqual(otherconfig.get('repo_paths', 'extra_name'), 'extra')

    def test_create_from_file(self):

        conf = self.myconfig.create_from_file(self.file, self.sections_list)

        self.assertIsInstance(conf, h.PathConfigParser)
        avalue = conf.get('database', 'env_type')
        self.assertEqual(avalue, 'dev')

    def test_getp_nopath(self):
        self.myconfig.read(self.file)

        avalue = self.myconfig.getp('database', 'env_type')
        self.assertEqual(avalue, 'dev')

        avalue = self.myconfig['database']['env_type']
        self.assertEqual(avalue, 'dev')

    def test_getp_path(self):
        self.myconfig.read(self.file)

        apath = self.myconfig.getp('repo_paths', 'base_dir')
        self.assertIsInstance(apath, Path)
        anotherpath = self.myconfig.getp('paths', 'progs')
        self.assertIsInstance(anotherpath, Path)

    def test_setp_path(self):
        self.myconfig.read(self.file)

        self.myconfig.setp('repo_paths', 'otherpath', Path(__file__))

        self.assertEqual(self.myconfig['repo_paths']['otherpath'], str(Path(__file__)))


class Test_call_bash(TestSuperClass, unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        #cls.get_dirs(cls, __file__)
        #cls.data = Path(cls.datadir, cls.__name__)
        cls.script = []
        cls.okmessage = 'message OK'
        cls.nokmessage = 'message NOK'

    #def fakeprint(self, message):
        #try:
            #self.messages.append(message)
        #except AttributeError:
            #self.messages = [message]

    def fail_action(self, *args):
        self.failaction = 'executed'
        self.failactionargs = args

    @patch('builtins.print')
    def test_ok_error(self, mock_print):
        with patch('subprocess.run', side_effect=subprocess.CalledProcessError(1, 'cmd')):

            returncode = h.call_bash(self.script,
                                     self.okmessage,
                                     self.nokmessage,
                                     okerror=1)

        self.assertEqual(returncode, 0)
        l = mock_print.mock_calls
        self.assertTrue(len(l) > 1)
        #call_args is the last call
        self.assertTrue(self.nokmessage in mock_print.call_args.args)

    @patch('builtins.print')
    def test_fail_action(self, mock_print):
        with patch('subprocess.run', side_effect=subprocess.CalledProcessError(1, 'cmd')):

            returncode = h.call_bash(self.script,
                                     self.okmessage,
                                     self.nokmessage,
                                     fail_action=self.fail_action,
                                     arguments=[1, 2, 3])

        self.assertEqual(returncode, 1)
        l = mock_print.mock_calls
        self.assertTrue(self.nokmessage in mock_print.call_args.args)
        self.assertEqual(self.failaction, 'executed')
        self.assertEqual(self.failactionargs, (1, 2, 3))


class Test_others(TestSuperClass, unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)
        cls.data = Path(cls.datadir, cls.__name__)

    def test_with_site_packages(self):
        venv = Path(self.datadir.parent, 'tests_pstartproject_django', 'venv_for_tests', 'myvenv')

        res = h.with_site_packages(venv)

        self.assertIsInstance(res, bool)

    def test_file_from_template(self):
        TestSuperClass.get_dirs(self, __file__)
        file = Path(self.data, 'fromtemplate')

        res = h.file_from_template_OK(file)

        self.assertFalse(res)

    def test_is_same_stat(self):
        path1 = self.data
        path2 = Path(self.datadir.parent, 'fake_repo_do_not_delete', 'configuration_directory', 'asgi.py')

        self.assertFalse(h.is_same_stat(path1, path2))


class Test_activate_venv(TestSuperClass, unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)
        cls.data = Path(cls.datadir, cls.__name__)

    @ patch('builtins.print')
    def test1_import_django(self, mockprint):
        h.activate_venv(Path(self.data, 'venvwithdjango'))

        try:
            import django
        except ImportError:
            pass

        #deactivate venv
        script = 'script_deactivate_venv.bash'
        returncode = h.call_bash(script,
                                 '',
                                 ''
                                 )


class Test_get_directories(TestSuperClass, unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)
        cls.data = Path(cls.datadir, cls.__name__)
        #copy the refakerepository
        cls.fakerepo_origin = Path(cls.datadir.parent, 'fake_repo_top_dir_do_not_delete_ludd21')
        cls.fakerepo = Path(cls.data, 'fake_repo')
        shutil.rmtree(cls.fakerepo, ignore_errors=True)
        shutil.copytree(cls.fakerepo_origin, cls.fakerepo)
        Path(cls.fakerepo, 'webLudd21', 'ambiguous_manage.py').touch()
        Path(cls.fakerepo, 'other programs', 'ambiguous_manage.py').touch()

    def test_is_under(self):
        self.assertTrue(h.is_under(Path(self.fakerepo, 'webLudd21'), 'manage.py'))
        self.assertFalse(h.is_under(self.fakerepo, 'toto.py'))

    @ patch('builtins.print')
    def test_get_parent_errors(self, mockprint):
        self.assertIsNone(h.get_parent(self.fakerepo, 'ambiguous_manage.py'))
        self.assertIsNone(h.get_parent(self.fakerepo, 'toto.py'))
        self.assertIsNone(h.get_parent(Path(self.fakerepo, 'other programs'), 'manage.py'))

    def test_get_parent_OK(self):
        self.assertEqual(h.get_parent(self.fakerepo, 'manage.py'),
                         Path(self.fakerepo, 'webLudd21'))
        self.assertEqual(h.get_parent(Path(self.fakerepo, 'webLudd21'), 'settings.py'),
                         Path(self.fakerepo, 'webLudd21', 'djangoLudd21')
                         )

    def test_get_project_root_repo(self):
        dir = h.get_project_root_repo(self.fakerepo)

        self.assertEqual(dir, Path(self.fakerepo, 'webLudd21', 'djngoLudd21'))

    def test_get_project_root_repo(self):
        dir = h.get_base_dir_repo(self.fakerepo)

        self.assertEqual(dir, Path(self.fakerepo, 'webLudd21'))


if __name__ == '__main__':
    import unittest

    unittest.main()
