#!/usr/bin/env python
# coding:utf-8

"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""


import shutil as sh
from pathlib import Path
import os
from argparse import Namespace

import unittest
from unittest.mock import patch, Mock


from tests.tests_super_class import TestSuperClass
import pstartproject as m
import create_django_env.helpers as h


"""This test_module must be run with an activated venv containing django use tests_data/tests_pstartproject/venv_for_tests/myvenv
first test creates one and install django in it"""

from tests.tests_super_class import load_ordered_tests
load_tests = load_ordered_tests

# this module uses venv in tests_pstartproject venv_for_tests which contains django


#@unittest.skip
class Test_django_start_proj_app (TestSuperClass, unittest.TestCase):
    """ test create project command and create app command calling django commands"""

    @classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)
        cls.data = Path(cls.datadir, cls.__name__)
        ns = Namespace(**{'project_name': 'testproj',
                          'directory': Path(cls.data, 'directory'),
                          'configuration_directory': 'confname',
                          'app_name': 'app_test',
                          'database_type': 'mysql',
                          })

        cls.project = m.Project_to_build(ns, cls.data)
        #add requirements to project because it will not be created
        cls.project.requirements = {'xmlschema': {'version': '3.4',
                                                  'python_version': None,
                                                  'requires': 'cairo',
                                                  'location': None,
                                                  'installer': None,
                                                  'others': None}
                                    }
        cls.project.django_requirements = '3.4'
        cls.project.extras.update({'project_requirements': cls.project.requirements,
                                   'django_requirements': cls.project.django_requirements})

        #add attributes to project because matrix willnot be inserted nor renamed
        cls.project.sourceproj_tmp_matrix_top = Path(Path(__file__).parent.parent, 'project_template')
        cls.project.sourceproj_matrix = Mock()
        cls.project.sourceproj_matrix.generated_suffixes = ['.py', '.cfg', '.txt']
        cls.project.sourceproj_matrix.template_suffixes = ['.py-tpl', '.cfg-tpl', '.txt-tpl']
        cls.project.sourceapp_tmp_matrix_top = Path(Path(__file__).parent.parent, 'app_template')
        cls.project.sourceapp_matrix = Mock()
        cls.project.sourceapp_matrix.generated_suffixes = ['.py', '.cfg', '.txt']
        cls.project.sourceapp_matrix.template_suffixes = ['.py-tpl', '.cfg-tpl', '.txt-tpl']
        cls.project.conf_dir = Path(cls.project.base_dir, 'configuration_directory')

        cls.project.venv_name = 'myvenv'
        cls.project.venv_path = Path(Path(cls.datadir).parent, 'tests_pstartproject', 'venv_for_tests', 'myvenv')

        #remove leftovers from precedent test_run
        if cls.project.directory.exists():
            sh.rmtree(cls.project.directory, ignore_errors=True)
            cls.project.directory.mkdir()
        Path(cls.project.venv_path, 'env.cfg').unlink(missing_ok=True)

        cls.stdfile_project = Path(cls.project.directory, 'stdout_create_project')
        cls.stdfile_application = Path(cls.project.directory, 'stdout_create_app')

    #@patch('builtins.print')

    def test_django_start_project(self):
        """ generate django project check that all templates correctly generated"""
        stdout = open(str(self.stdfile_project), 'w')

        m.django_start_project(self.project, stdout=stdout)

        for file in self.project.base_dir.iterdir():
            if file.is_file() and file.suffix in self.project.sourceproj_matrix.generated_suffixes:
                self.assertTrue(h.file_from_template_OK(file))
        for file in self.project.conf_dir.iterdir():
            if file.is_file() and file.suffix in self.project.sourceproj_matrix.generated_suffixes:
                self.assertTrue(h.file_from_template_OK(file))
        self.assertTrue(self.stdfile_project.exists())
        self.assertTrue(self.stdfile_project.stat().st_size != 0)

    #@ unittest.skip
    #@patch('builtins.print')
    def test_django_start_app(self):
        """ generate django app check that all templates correctly generated"""
        stdout = open(str(self.stdfile_application), 'w')

        m.django_start_app(self.project, stdout=stdout)

        for file in self.project.base_dir.iterdir():
            if file.is_file() and file.suffix in self.project.sourceproj_matrix.generated_suffixes:
                self.assertTrue(h.file_from_template_OK(file))
        self.assertTrue(self.stdfile_application.exists())
        self.assertTrue(self.stdfile_application.stat().st_size != 0)

    #@classmethod
    #def tearDownClass(cls):
        #super().tearDownClass()


if __name__ == '__main__':
    import importlib
    import unittest
    current = Path(__file__)
    module = importlib.import_module((current.stem))

    # This orders the tests to be run in the order they were declared.
    # It uses the unittest load_tests protocol.
    #load_tests = load_ordered_tests
    loader = unittest.TestLoader()
    tests_suite = loader.loadTestsFromModule(module)
    testRunner = unittest.runner.TextTestRunner()
    testRunner.run(tests_suite)

    #unittest.main()
