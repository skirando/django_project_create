#!/usr/bin/env python
# coding:utf-8

"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""
import unittest
from pathlib import Path


def ordered_test_cases(test_class):
    return sorted(test_class, key=lambda t: t.id().split('.')[-1])


def load_ordered_tests(loader, standard_tests, pattern):
    """
    Test loader function that keeps the tests in the order they were declared in the class.
    """
    import inspect

    def get_decl_line_no(cls):
        """returns the line number for the declaration of cls
        cls must be an object not a test_suite"""

        if cls and isinstance(cls, list):
            #it is a list of tests, take the class name of the first'
            return inspect.getsourcelines(cls[0].__class__)[1]
        elif cls:
            #it is a test
            return inspect.getsourcelines(cls.__class__)[1]

    test_classes = [list(test for test in test_class.__iter__()) for test_class in standard_tests.__iter__()]
    test_classes_cleaned = [l for l in test_classes if len(l) > 0]
    #sort the tests within a class
    ordered_tests = [sorted(l, key=get_decl_line_no) for l in test_classes_cleaned]
    ordered_classes = sorted(ordered_tests, key=get_decl_line_no)
    ordered_Suites = [unittest.TestSuite(tests=liste) for liste in ordered_classes]

    return unittest.TestSuite(tests=ordered_Suites)


class TestSuperClass (unittest.TestCase):

    def get_dirs(self, calling_file):

        self.datadir = Path(Path(calling_file).parent, 'tests_data', Path(calling_file).stem)
        self.repo_path = Path(Path(calling_file).parent, 'tests_data', 'fake_repo_do_not_delete')
        self.project_name = 'project_name'
        self.project_root = Path(self.repo_path, 'configuration_directory')
        #self.cfg_path = Path(self.project_root, 'env.cfg')
        self.myapp = Path(self.repo_path, 'myapp')











