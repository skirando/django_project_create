#!/usr/bin/env python
# coding:utf-8

"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""

import argparse
import shutil as sh
from pathlib import Path
import os
import sys
from argparse import Namespace

import unittest
from unittest.mock import patch, Mock

from tests.tests_super_class import TestSuperClass
import pstartproject as m
import helpers

"""This test_module must be ran with no virtual env activated 
test order is imposed because one test uses the virtual env created by preceding test
"""


from tests.tests_super_class import load_ordered_tests
load_tests = load_ordered_tests


#@unittest.skip('temporaire')
class TestProject_to_build(TestSuperClass, unittest.TestCase):
    """ does not care about virtualenv"""

    @classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)
        cls.data = Path(cls.datadir, cls.__name__)
        sh.rmtree(cls.data, ignore_errors=True)
        cls.data.mkdir()

    def test1_no_directory(self):
        """because directory is build without argparse, all keys must be present"""
        ns = Namespace(**{'project_name': 'testproj',
                          'directory': '.',
                          'app_name': 'app_name',
                          'database_type': 'mysql',
                          'configuration_directory': 'confname',
                          })

        project = m.Project_to_build(ns, self.data)

        self.assertEqual(project.directory, '.')
        self.assertEqual(project.base_dir, self.data)
        self.assertEqual(project.configuration_directory, Path(self.data, 'confname'))
        self.assertEqual(project.app_directory, Path(self.data, 'app_name'))
        self.assertEqual(project.matrixinsert, Path(self.data))

    def test2_directory(self):
        pathdir = Path(self.data, 'directory')
        pathdir.mkdir(exist_ok=True)
        ns = Namespace(**{'project_name': 'testproj',
                          'directory': pathdir,
                          'app_name': 'app_name',
                          'database_type': 'mysql',
                          'configuration_directory': 'DEFAULT',
                          })

        project = m.Project_to_build(ns, self.data)

        self.assertTrue(pathdir.exists())
        self.assertEqual(project.cwd, self.data)
        self.assertEqual(project.base_dir, pathdir)
        self.assertEqual(project.app_directory, Path(pathdir, 'app_name'))
        self.assertEqual(project.matrixinsert, pathdir)


class Test_Requirements(TestSuperClass, unittest.TestCase):
    """test functions related to requirements,
    requires a virtual env with django and xmlschema as project.venv_path but does not require this venv to be activated"""

    @ classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)
        cls.data = Path(cls.datadir, cls.__name__)
        cls.dic = {'project_name': 'project_name',
                   'directory': '.',
                   'app_name': 'appname',
                   'database_type': 'sqlite',
                   'configuration_directory': 'DEFAULT'
                   }
        ns = Namespace(**cls.dic)
        cls.project = m.Project_to_build(ns, cls.datadir)
        cls.project.venv_path = Path(cls.datadir, 'venv_for_tests', 'myvenv')

    @ patch('builtins.print')
    def test_create_project_requirements(self, mock_print):

        with patch.object(m.Project_to_build, 'install_django', return_value=0):
            self.project.create_project_requirements(test=True)

        self.assertEqual(len(self.project.requirements), 1)
        self.assertTrue('xmlschema' in self.project.requirements.keys())
        self.assertTrue('Django' in self.project.django_requirements.keys())


class Test_get_venv_no_venv(TestSuperClass, unittest.TestCase):
    """tests get_venv_funtion with no_venv_activated
    first 2 tests require no virtual environment
    last test creates one and activates it"""

    @classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)
        cls.data = Path(cls.datadir, cls.__name__)

        ns = Namespace(**{'project_name': 'testproj',
                          'directory': '.',
                          'app_name': 'app_name',
                          'database_type': 'mysql',
                          'configuration_directory': 'DEFAULT'
                          })
        sh.rmtree(cls.data, ignore_errors=True)
        cls.data.mkdir()

        cls.project = m.Project_to_build(ns, cls.data)

    @ patch('builtins.print')
    def test1_create_no(self, mock_print):
        """ user refuses to create new one"""
        with patch('create_django_env.helpers.valid_message', return_value=1):
            res = self.project.get_venv()

        self.assertIsNone(res)

    @ patch('builtins.print')
    def test2_create_no(self, mock_print):
        """refuse creation and check no_venv is created"""
        name = self.id().split('.')[-1]
        other_venv = Path(self.datadir, name)
        sh.rmtree(other_venv, ignore_errors=True)

        with patch('create_django_env.helpers.valid_message', return_value=1):
            res = self.project.get_venv()

        self.assertIsNone(res)

    @ patch('builtins.print')
    def test3_create_yes(self, mock_print):
        """check another venv is created
        cannot test venv is activated"""
        sh.rmtree(self.data, ignore_errors=True)
        self.data.mkdir()

        ##pretend there is no venv activate
        #with patch('pstartproject.h.get_virtual_env', return_value=None):
            #return yes to create one
        with patch('create_django_env.helpers.valid_message', return_value=0):
            res = self.project.get_venv()
            #self.assertEqual(os.environ['VIRTUAL_ENV'], str(Path(self.data, h.DEFAULT_VENV_NAME)))

        self.assertEqual(res[1], Path(self.data, helpers.DEFAULT_VENV_NAME))
        self.assertEqual(res[0], True)


#@unittest.skip
class Test_Matrix(TestSuperClass, unittest.TestCase):
    """test the clean and insert functions on existing directories
    does not care about virtual environment"""

    @ classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)
        cls.data = Path(cls.datadir, cls.__name__)
        cls.top = Path(Path(__file__).parent.parent)
        cls.matrix = m.Matrix(cls.top, 'project')

    #@ patch('builtins.print')
    def test_clean(self):
        name = self.id().split('.')[-1]
        dic = {'project_name': 'testproj',
               'directory': '.',
               'app_name': 'app_name',
               'database_type': 'mysql',
               'configuration_directory': 'DEFAULT'
               }
        cwd = Path(self.data, name)
        #clean scories from preceding tests
        sh.rmtree(cwd, ignore_errors=True)
        cwd.mkdir()
        ns = Namespace(**dic)
        project = m.Project_to_build(ns, cwd)
        #copy matrix project into dir to clean it
        source = self.matrix.top_dir
        dest = Path(cwd, self.matrix.tmp_top_dir_name)
        sh.copytree(source, dest, dirs_exist_ok=True)
        pathtoremain = Path(cwd, 'to_remain_after_clean')
        pathtoremain.mkdir(exist_ok=True)

        #second patch to prevent print
        with patch('create_django_env.helpers.valid_message', return_value=0):
            with patch('builtins.print'):
                project.clean(self.matrix, project.base_dir, 'project')

        #app directory remains after clean this is normal
        self.assertTrue(not Path(project.base_dir, project.conf_dir).exists())
        self.assertTrue(pathtoremain.exists())

    @ patch('builtins.print')
    def test_insert_matrix(self, mock_print):

        name = self.id().split('.')[-1]
        cwd = Path(self.data, name)
        #erase content of preceding tests
        sh.rmtree(cwd, ignore_errors=True)
        cwd.mkdir()
        dic = {'project_name': 'testproj',
               'directory': Path(cwd, 'testproj_dir'),
               'app_name': 'app_name',
               'database_type': 'mysql',
               'configuration_directory': 'DEFAULT'
               }

        ns = Namespace(**dic)
        top = self.matrix.top_dir
        project = m.Project_to_build(ns, cwd)

        with patch.object(project, 'matrixinsert', cwd):
            project.insert_matrix(self.matrix)

        #check configuration directory exists and was renamed
        self.assertTrue(Path(cwd, self.matrix.tmp_top_dir_name, project.conf_dir.stem).exists())
        self.assertEqual(project.sourceproj_matrix, self.matrix)


if __name__ == '__main__':

    import unittest
    from pathlib import Path
    import importlib

    from tests.tests_super_class import load_ordered_tests

    current = Path(__file__)
    module = importlib.import_module((current.stem))

    # This orders the tests to be run in the order they were declared.
    # It uses the unittest load_tests protocol.
    #load_tests = load_ordered_tests
    loader = unittest.TestLoader()
    tests_suite = loader.loadTestsFromModule(module)
    testRunner = unittest.runner.TextTestRunner()
    testRunner.run(tests_suite)

    unittest.main()
