#!/usr/bin/env python
# coding:utf-8

"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""

import argparse
import shutil as sh
import subprocess
from pathlib import Path
from argparse import Namespace
from configparser import NoOptionError, NoSectionError

import unittest
from unittest.mock import patch

from tests.tests_super_class import TestSuperClass
import helpers as h
import repo_clone


class Test_clone_git(TestSuperClass):
    """clone_git sets kwargs values for the creation of cfg
    mock the creation of the project directory 
    and set cwd to the tests/data directory
    use fake_repo_do_not_delete as if it was the copied repo """

    @classmethod
    def setUpClass(cls):
        # this is to simulate that script is called as root
        cls.bash_patcher = patch('helpers.call_bash', return_value=0)
        cls.bash_patcher.start()
        cls.get_dirs(cls, __file__)

    def test_no_top_dir(self):

        cwd = self.datadir.parent
        args_dict = {'project_name_directory': 'fake_repo_do_not_delete',
                     'url': 'fake'}

        args_dict = repo_clone.clone_git(cwd, **args_dict)

        self.assertEqual(args_dict['base_dir'], Path(self.datadir.parent, 'fake_repo_do_not_delete'))
        self.assertEqual(args_dict['top_dir'], Path(self.datadir.parent, 'fake_repo_do_not_delete'))

    def test_top_dir(self):

        cwd = self.datadir.parent
        args_dict = {'project_name_directory': 'fake_repo_top_dir_do_not_delete_ludd21',
                     'url': 'fake'}

        args_dict = repo_clone.clone_git(cwd, **args_dict)

        self.assertEqual(args_dict['base_dir'], Path(self.datadir.parent, 'fake_repo_top_dir_do_not_delete_ludd21', 'webLudd21'))
        self.assertEqual(args_dict['top_dir'], Path(self.datadir.parent, 'fake_repo_top_dir_do_not_delete_ludd21'))

    @ classmethod
    def tearDownClass(cls):
        cls.bash_patcher.stop()


class SuperSetUpClass(TestSuperClass, unittest.TestCase):
    @classmethod
    def setUpClass(cls, args_dict):
        # this is to simulate that script is called as root
        cls.user_patcher = patch('helpers.check_root', return_value=True)
        cls.user_patcher.start()

        def mock_script_bash(script, m1,
                             m2,
                             **kwargs):
            if script.parts[-1] == 'script_git_clone.bash':
                cwd, project_name_directory = kwargs.get('arguments', None)
                dest = Path(cwd, project_name_directory)

                sh.copytree(args_dict['url'], dest)

            #else:
            return 0
        cls.script_bash_mock = patch('helpers.call_bash', new=mock_script_bash)
        cls.script_bash_mock.start()


class Test_repo_clone(SuperSetUpClass):
    """repo clone create the env.cfg this is what needs to be tested"""

    @classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)
        cls.args_dict = {'project_name_directory': 'test_project',
                         'url': str(Path(cls.datadir.parent, 'fake_repo_do_not_delete')),
                         'env_type': 'dev',
                         'email_admin': 'test@orange.fr',
                         'user_admin': 'test',
                         }
        super().setUpClass(cls.args_dict)

    @ patch('builtins.print')
    def test1_clone_git_correct_repo(self, mockprint):

        namespace = Namespace(**self.args_dict)

        project_name = self.args_dict['project_name_directory']
        destination = Path(self.datadir, self.id().split('.')[-1])
        if destination.exists:
            sh.rmtree(destination, ignore_errors=True)
        base_dir = Path(destination, project_name)

        with patch('repo_clone.h.call_pip', return_value=0) as m1:
            repo_clone.mmain(namespace, destination)

        config = h.PathConfigParser.create_from_file(Path(destination, project_name, 'configuration_directory', 'env.cfg'))
        self.assertEqual(config.get('repo_paths', 'base_dir'), str(base_dir))
        self.assertTrue(config.get('repo_paths', 'base_dir'), config.get('repo_paths', 'top_dir'))
        self.assertEqual(config.get('configuration', 'env_type'), 'dev')
        self.assertEqual(config.get('paths', 'venv'), str(Path(destination, project_name, 'myvenv')))
        self.assertEqual(config.get('paths', 'conf'), str(Path(destination, project_name, 'configuration_directory')))
        self.assertTrue(config.get('pnames', 'project_name'), project_name)
        self.assertTrue(config.get('pnames', 'venvname'), 'myvenv')
        self.assertTrue(config.get('pnames', 'confname'), 'configuration_directory')
        self.assertTrue(config.get('users', 'email_admin'), self.args_dict['email_admin'])
        self.assertTrue(config.get('users', 'user_admin'), self.args_dict['user_admin'])
        self.assertTrue(config.get('database', 'dbtype'), 'sqlite')
        self.assertTrue(Path(destination, project_name, 'my_manage.py').exists())
        self.assertTrue(Path(destination, project_name, 'configuration_directory', 'installed_requirements').exists())
        with self.assertRaises(NoOptionError) as ex:
            self.assertTrue(config.get('users', 'django_user'), '')
        with self.assertRaises(NoOptionError) as ex:
            self.assertTrue(config.get('host', 'domain'), '')

    @ classmethod
    def tearDownClass(cls):
        cls.user_patcher.stop()
        cls.script_bash_mock.stop()


class Test_repo_clone_top_dir(SuperSetUpClass):
    """repo clone create the env.cfg this is what needs to be tested"""

    @classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)
        cls.args_dict = {'project_name_directory': 'test_project',
                         'url': str(Path(cls.datadir.parent, 'fake_repo_top_dir_do_not_delete_ludd21')),
                         'env_type': 'dev',
                         'email_admin': 'test@orange.fr',
                         'user_admin': 'test',
                         }
        super().setUpClass(cls.args_dict)

    @ patch('builtins.print')
    def test2_clone_git_repo_with_top_dir(self, mockprint):
        namespace = Namespace(**self.args_dict)
        project_name = self.args_dict['project_name_directory']
        destination = Path(self.datadir, self.id().split('.')[-1])
        if destination.exists:
            sh.rmtree(destination, ignore_errors=True)
        top_dir = Path(destination, project_name)

        with patch('repo_clone.h.call_pip', return_value=0) as m1:
            repo_clone.mmain(namespace, destination)

        config = h.PathConfigParser.create_from_file(Path(destination, project_name, 'webLudd21', 'djangoLudd21', 'env.cfg'))
        self.assertEqual(config.get('repo_paths', 'top_dir'), str(top_dir))
        self.assertEqual(config.get('repo_paths', 'base_dir'), str(Path(top_dir, 'webLudd21')))
        self.assertEqual(config.get('configuration', 'env_type'), 'dev')
        self.assertEqual(config.get('paths', 'venv'), str(Path(destination, project_name, 'myvenv')))
        self.assertEqual(config.get('paths', 'conf'), str(Path(destination, project_name, 'webLudd21', 'djangoLudd21')))
        self.assertTrue(config.get('pnames', 'project_name'), project_name)
        self.assertTrue(config.get('pnames', 'venvname'), 'myvenv')
        self.assertTrue(config.get('pnames', 'confname'), 'djangoLudd21')
        self.assertTrue(config.get('users', 'email_admin'), self.args_dict['email_admin'])
        self.assertTrue(config.get('users', 'user_admin'), self.args_dict['user_admin'])
        self.assertTrue(config.get('database', 'dbtype'), 'sqlite')
        self.assertTrue(Path(destination, project_name, 'webLudd21', 'my_manage.py').exists())
        self.assertTrue(Path(destination, project_name, 'webLudd21', 'djangoLudd21', 'installed_requirements').exists())
        with self.assertRaises(NoOptionError) as ex:
            self.assertTrue(config.get('users', 'django_user'), '')
        with self.assertRaises(NoOptionError) as ex:
            self.assertTrue(config.get('host', 'domain'), '')

    @ classmethod
    def tearDownClass(cls):
        cls.user_patcher.stop()
        cls.script_bash_mock.stop()


#class Test_repo_clone_extra_dir(SuperSetUpClass):
    #"""repo clone create the env.cfg this is what needs to be tested"""

    #@classmethod
    #def setUpClass(cls):
        #cls.get_dirs(cls, __file__)
        #cls.args_dict = {'project_name_directory': 'test_project',
                         #'url': str(Path(cls.datadir.parent, 'fake_repo_top_dir_do_not_delete_ludd21')),
                         #'env_type': 'prod',
                         #'email_admin': 'test@orange.fr',
                         #'user_admin': 'test',
                         #'extra_url': str(Path(cls.datadir.parent, 'fake_extra_repo_do_not_delete'))
                         #}
        #super().setUpClass(cls.args_dict)

    #@ patch('builtins.print')
    #def test3_clone_git_repo_with_extra_dir(self, mockprint):
        #"""extra_dir is handled only in prod configuration cannot do complete repoclone test with prod configuration"""
        #namespace = Namespace(**self.args_dict)
        #project_name = self.args_dict['project_name_directory']
        #destination = Path(self.datadir, self.id().split('.')[-1])
        #if destination.exists:
            #sh.rmtree(destination, ignore_errors=True)
        #top_dir = Path(destination, project_name)

        #with patch('repo_clone.h.call_pip', return_value=0) as m1:
            #args = repo_clone.clone_extra(destination, **self.args_dict)

        #self.assertEqual(args['extra_name'], 'fake_extra_repo_do_not_delete')

    #@ classmethod
    #def tearDownClass(cls):
        #cls.user_patcher.stop()
        #cls.script_bash_mock.stop()


if __name__ == '__main__':
    import unittest

    unittest.main()
