#!/usr/bin/env python
# coding:utf-8

"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""
import unittest

from pathlib import Path
from unittest.mock import patch
import shutil
import copy

import helpers as h
from dispatch_files import *
from tests.tests_super_class import TestSuperClass


class Test_create_paths_section(TestSuperClass):
    """test creation of paths from dict
    dict is produced by repo_clone and contains top_dir and base_dir"""

    @classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)

        cls.data = Path(cls.datadir, cls.__name__)
        cls.fakeserver = Path(cls.data, 'fake_server')
        cls.base_dir_top = Path(cls.datadir.parent, 'fake_repo_top_dir_do_not_delete_ludd21', 'webLudd21')
        cls.top_dir_top = Path(cls.datadir.parent, 'fake_repo_top_dir_do_not_delete_ludd21')
        cls.base_dir_no_top = Path(cls.datadir.parent, 'fake_repo_do_not_delete')
        cls.top_dir_no_top = cls.base_dir_no_top

    def test_init_no_top_dir(self):
        """test that default values are overwritten"""

        dic = {'confname': 'Recettes',
               'project_name': 'recettes_2022',
               'base_dir': self.base_dir_no_top,
               'top_dir': self.top_dir_no_top,
               'venvname': 'myvenvenv'
               }
        config = ConfigParserMixin(**dic)

        self.assertEqual(config.base_dir, self.base_dir_no_top)
        self.assertEqual(config.top_dir, self.top_dir_no_top)
        self.assertEqual(config.project_name, 'recettes_2022')
        self.assertEqual(config.venvname, 'myvenvenv')
        self.assertEqual(config.base_dir_name, '')

    def test_init_top_dir(self):
        """test that default values are included"""
        dic = {'project_name': 'ludd21',
               'base_dir': self.base_dir_top,
               'top_dir': self.top_dir_top,
               'confname': 'djangoLudd21'
               }
        config = ConfigParserMixin(**dic)

        self.assertEqual(config.base_dir, self.base_dir_top)
        self.assertEqual(config.top_dir, self.top_dir_top)
        self.assertEqual(config.project_name, 'ludd21')
        self.assertEqual(config.venvname, 'myvenv')
        self.assertEqual(config.base_dir_name, 'webLudd21')
        self.assertEqual(config.confname, 'djangoLudd21')

    def test_dev_top_dir(self):
        dic = {'project_name': 'ludd21',
               'base_dir': self.base_dir_top,
               'top_dir': self.top_dir_top,
               'confname': 'djangoLudd21'
               }

        self.config = DevConfigParser(**dic)
        self.config.create_paths_section()

        self.assertEqual(self.config.getp('paths', 'venv'), Path(self.top_dir_top, 'myvenv'))
        self.assertEqual(self.config.getp('paths', 'progs'), Path(self.base_dir_top))
        self.assertEqual(self.config.getp('paths', 'conf'), Path(self.base_dir_top, 'djangoLudd21'))

    def test_dev_no_top_dir(self):
        dic = {'project_name': 'recettes_2022',
               'base_dir': self.base_dir_no_top,
               'top_dir': self.top_dir_no_top,
               'confname': 'Recettes'
               }

        self.config = DevConfigParser(**dic)
        self.config.create_paths_section()

        self.assertEqual(self.config.getp('paths', 'venv'), Path(self.base_dir_no_top, 'myvenv'))
        self.assertEqual(self.config.getp('paths', 'progs'), Path(self.base_dir_no_top))
        self.assertEqual(self.config.getp('paths', 'conf'), Path(self.base_dir_no_top, 'Recettes'))

    def test_prod_no_top_dir(self):
        dic = {'project_name': 'recettes_2022',
               'base_dir': self.base_dir_no_top,
               'top_dir': self.top_dir_no_top,
               'confname': 'Recettes'
               }

        with patch.object(ProdConfigParser, 'BASE_PROD', self.fakeserver):
            self.config = ProdConfigParser(**dic)
            self.config.create_paths_section()

        self.assertEqual(self.config.getp('paths', 'progs'), Path(self.fakeserver, 'opt', 'recettes_2022',))
        self.assertEqual(self.config.getp('paths', 'venv'), Path(self.fakeserver, 'opt', 'recettes_2022', 'myvenv'))
        self.assertEqual(self.config.getp('paths', 'media'), Path(self.fakeserver, 'var', 'opt', 'recettes_2022', 'media'))
        self.assertEqual(self.config.getp('paths', 'statics'), Path(self.fakeserver, 'var', 'cache', 'recettes_2022'))
        self.assertEqual(self.config.getp('paths', 'logs'), Path(self.fakeserver, 'var', 'log', 'recettes_2022'))
        self.assertEqual(self.config.getp('paths', 'conf'), Path(self.fakeserver, 'etc', 'opt', 'recettes_2022'))
        self.assertEqual(self.config.getp('paths', 'nginx'), Path(self.fakeserver, 'etc', 'nginx'))
        self.assertEqual(self.config.getp('paths', 'www'), Path(self.fakeserver, 'var', 'www'))

    def test_prod_top_dir(self):
        dic = {'confname': 'djangoLudd21',
               'project_name': 'ludd21',
               'base_dir': self.base_dir_top,
               'top_dir': self.top_dir_top, }

        with patch.object(ProdConfigParser, 'BASE_PROD', self.fakeserver):
            self.config = ProdConfigParser(**dic)
            self.config.create_paths_section()

        self.assertEqual(self.config.getp('repo_paths', 'extra_name'), '')
        self.assertEqual(self.config.getp('paths', 'progs'), Path(self.fakeserver, 'opt', 'ludd21', self.config.base_dir_name))
        self.assertEqual(self.config.getp('paths', 'venv'), Path(self.fakeserver, 'opt', 'ludd21', 'myvenv'))
        self.assertEqual(self.config.getp('paths', 'media'), Path(self.fakeserver, 'var', 'opt', 'ludd21', 'media'))
        self.assertEqual(self.config.getp('paths', 'statics'), Path(self.fakeserver, 'var', 'cache', 'ludd21'))
        self.assertEqual(self.config.getp('paths', 'logs'), Path(self.fakeserver, 'var', 'log', 'ludd21'))
        self.assertEqual(self.config.getp('paths', 'conf'), Path(self.fakeserver, 'etc', 'opt', 'ludd21', self.config.base_dir_name))
        self.assertEqual(self.config.getp('paths', 'nginx'), Path(self.fakeserver, 'etc', 'nginx'))
        self.assertEqual(self.config.getp('paths', 'www'), Path(self.fakeserver, 'var', 'www'))

    def test_prod_extra_dir(self):

        extra_dir_dic = {'confname': 'djangoLudd21',
                         'project_name': 'ludd21',
                         'base_dir': self.base_dir_top,
                         'top_dir': self.top_dir_top,
                         'extra_name': 'SoftKnit21'}

        with patch.object(ProdConfigParser, 'BASE_PROD', self.fakeserver):
            self.config = ProdConfigParser(**extra_dir_dic)
            self.config.create_paths_section()

        self.assertEqual(self.config.getp('paths', 'extra_dir'), Path(self.fakeserver, 'opt', 'SoftKnit21'))


#@unittest.skip('temporaire')
class FilesTestSuperClass(TestSuperClass):

    @ classmethod
    def setUpClass(cls, dic):
        """prepare the test data directory for the dispatching of files taking into account dic"""
        #remove from previous tests
        shutil.rmtree(cls.fakeserver, ignore_errors=True)
        cls.fakeserver.mkdir()
        #copy the source directory (equivalent to repo directory)
        shutil.rmtree(cls.source_copy, ignore_errors=True)
        shutil.copytree(Path(cls.datadir.parent, cls.source_repo), cls.source_copy)

        with patch.object(ProdConfigParser, 'BASE_PROD', cls.fakeserver):
            cls.config = ProdConfigParser(**dic)
            cls.config.create_paths_section()
            #we need to create env.cfg corresponding to the config

            configdir = Path(cls.config.getp('repo_paths', 'base_dir'), cls.config.get('pnames', 'confname'), 'env.cfg')
            with open(configdir, 'w') as configfile:
                cls.config.write(configfile)
        #need to create venv which is normally created by repoclone
        cls.myvenv = cls.config.getp('paths', 'venv')
        cls.myvenv.mkdir(parents=True, exist_ok=True)
        cls.project_name = dic['project_name']
        cls.project_root_name = dic['confname']

        """cannot tests rights. would require to launch the copy_repo as root"""
        cls.chmod_patcher = patch('helpers.Path.chmod')
        cls.chmod_patcher.start()
        cls.sh_chown_patcher = patch('shutil.chown')
        cls.sh_chown_patcher.start()


#@unittest.skip('temporaire')
class Test_ProdFilesDistribution(FilesTestSuperClass):
    """test distribution of files for prod configuration"""
    @ classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)
        cls.data = Path(cls.datadir, cls.__name__)
        #this is destination of dispatched files
        cls.fakeserver = Path(cls.data, 'fake_server')
        #this is source of dispatched files
        cls.source_repo = 'fake_repo_do_not_delete'
        cls.source_copy = Path(cls.data, 'source_fake_repo_do_not_delete_with_cfg')
        dic = {'django_user': 'toto_user',
               'domain': '1.2.3.4',
               #'host_ip': '1.2.3.4',
               'project_name': 'test_project',
               'env_type': 'prod',
               'base_dir': cls.source_copy,
               'top_dir': cls.source_copy,
               'confname': 'configuration_directory',
               'venvname': 'myvenv',
               'dns': 'False',
               }
        super().setUpClass(dic)

    def test_Program_files(self):
        cls = Program_files

        cls.files_dirs(self.config, install='new')

        progs = Path(self.fakeserver, 'opt', self.project_name)
        self.assertTrue(progs.exists())
        self.assertTrue(progs.is_dir())
        self.assertTrue(Path(progs, 'manage.py').exists())

        #check excluded dirs
        for string in cls.dir_ignore:
            self.assertFalse(Path(*progs.parts, string).exists())

        #check configuration files
        self.assertTrue(Path(progs, self.project_root_name).exists())
        self.assertTrue(Path(progs, self.project_root_name).is_dir())
        self.assertTrue(Path(progs, self.project_root_name, 'wsgi.py').exists())
        self.assertTrue(Path(progs, self.project_root_name, 'urls.py').exists())
        self.assertTrue(Path(progs, self.project_root_name, 'asgi.py').exists())

    def test_Configuration(self):
        cls = Configuration
        conf_files = Path(self.fakeserver, 'etc', 'opt', self.project_name)
        #common_file = Path(self.repo_path, self.project_root_name, 'installed_requirements', 'common.txt')
        #to_compare = [
            #('st_mode', True),
            #('st_ino', False),
            #('st_dev', False),
            #('st_nlink', False),
            #('st_uid', True),
            #('st_gid', True),
            #('st_size', True),
            #('st_atime', False),
            #('st_mtime', True),
            #('st_ctime', False)
        #]

        cls.files_dirs(self.config, install='new')

        self.assertTrue(conf_files.exists())
        self.assertTrue(conf_files.is_dir())
        self.assertFalse(Path(conf_files, 'wsgi.py').exists())
        self.assertTrue(Path(conf_files, 'settings.py').exists())

    def test_Virtual_env(self):
        cls = Virtual_env

        cls.files_dirs(self.config, install='new')

        #venv created by repoclone do not check content but check symlink
        self.assertTrue(Path(self.myvenv, 'env.cfg').is_symlink())

    def test_Media_files(self):
        cls = Media_files

        cls.files_dirs(self.config, install='new')

        self.assertTrue(Path(self.fakeserver, 'var', 'opt', self.project_name, 'media').exists())
        self.assertTrue(Path(self.fakeserver, 'var', 'opt', self.project_name, 'media').is_dir())
        repo_paths = self.config.getp('repo_paths', 'base_dir')
        if any(Path(repo_paths, 'media').iterdir()):
            self.assertTrue(any(Path(self.fakeserver, 'var', 'opt', self.project_name, 'media').iterdir()))
        self.assertTrue(Path(self.fakeserver, 'var', 'opt', self.project_name, 'media', 'not_to_be_copied_in_prod_update').exists())

    def test_Log_files(self):
        cls = Log_files

        cls.files_dirs(self.config, install='new')

        self.assertTrue(Path(self.fakeserver, 'var', 'log', self.project_name).exists())
        self.assertTrue(Path(self.fakeserver, 'var', 'log', self.project_name).is_dir())

    def test_Static_files(self):
        cls = Static_files

        cls.files_dirs(self.config, install='new')

        #statics do not verify content because it is not filled by copy_repo
        self.assertTrue(Path(self.fakeserver, 'var', 'cache', self.project_name).exists())
        self.assertTrue(Path(self.fakeserver, 'var', 'cache', self.project_name).is_dir())

    def test_Nginx(self):
        cls = Nginx

        cls.files_dirs(self.config, install='new')

        #nginx
        domain = self.config.get('host', 'domain')
        self.assertTrue(Path(self.fakeserver, 'etc', 'nginx', 'sites-enabled', domain,).exists())
        self.assertTrue(Path(self.fakeserver, 'etc', 'nginx', 'sites-enabled', domain,).is_symlink())
        self.assertTrue(Path(self.fakeserver, 'etc', 'nginx', 'sites-available', domain).exists())
        self.assertTrue(Path(self.fakeserver, 'etc', 'nginx', 'sites-available', domain).is_file())
        self.assertTrue(Path(self.fakeserver, 'etc', 'nginx', 'sites-enabled', 'default').exists())
        self.assertTrue(Path(self.fakeserver, 'etc', 'nginx', 'sites-enabled', 'default').is_file())

        #www
        self.assertTrue(Path(self.fakeserver, 'var', 'www', 'index.html').exists())
        self.assertTrue(Path(self.fakeserver, 'var', 'www', 'index.html').is_file())

    @ classmethod
    def tearDownClass(cls):
        #shutil.rmtree(cls.fakeserver, ignore_errors=True)
        cls.sh_chown_patcher.stop()
        cls.chmod_patcher.stop()


#@unittest.skip('temporaire')
class Test_ProdFilesDistribution_install_update(FilesTestSuperClass):
    """ test prod files for update'"""

    @ classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)
        cls.data = Path(cls.datadir, cls.__name__)
        cls.fakeserver = Path(cls.data, 'fake_server')
        #this is source of dispatched files
        cls.source_repo = 'fake_repo_do_not_delete'
        cls.source_copy = Path(cls.data, 'source_fake_repo_do_not_delete_with_cfg')
        dic = {'django_user': 'toto_user',
               'domain': '1.2.3.4',
               'project_name': 'test_project',
               'env_type': 'prod',
               'base_dir': cls.source_copy,
               'top_dir': cls.source_copy,
               'confname': 'configuration_directory',
               'venvname': 'myvenv',
               'dns': 'False'
               }
        cls.project_name = dic['project_name']
        cls.project_root_name = dic['confname']
        super().setUpClass(dic)

    def test_copy_repo_to_prod(self):
        copy_repo_to_prod(self.config, install='update')

        #only progs_media, nothing else in media
        media = self.config.getp('paths', 'media')
        self.assertFalse(Path(media, 'not_to_be_copied_in_prod_update').exists())
        #not Nginx
        conf = self.config.getp('paths', 'conf')
        self.assertFalse(Path(conf, 'nginx').exists())
        #not env.cfg in confdir
        self.assertFalse(Path(conf, 'env.cfg').exists())
        #not symlink to env.cfg in virtual_env
        venv = self.config.getp('paths', 'venv')
        self.assertFalse(Path(venv, 'env.cfg').exists())
        # not logs
        logs = self.config.getp('paths', 'logs')
        self.assertFalse(logs.exists())


#@unittest.skip('temporaire')
class Test_ProdFilesDistribution_top_dir(FilesTestSuperClass):
    """ test copy_repo with a top_dir'"""

    @ classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)
        cls.data = Path(cls.datadir, cls.__name__)
        #this is destination of dispatched files
        cls.fakeserver = Path(cls.data, 'fake_server')
        #this is source of dispatched files
        cls.source_repo = 'fake_repo_top_dir_do_not_delete_ludd21'
        cls.source_copy = Path(cls.data, 'source_fake_repo_top_dir_do_not_delete_with_cfg')
        dic = {'django_user': 'toto_user',
               'domain': '1.2.3.4',
               #'host_ip': '1.2.3.4',
               'project_name': 'ludd21',
               'env_type': 'prod',
               'base_dir': Path(cls.source_copy, 'webLudd21'),
               'confname': 'djangoLudd21',
               'top_dir': cls.source_copy,
               'venvname': 'myvenv',
               'dns': 'True'
               }
        super().setUpClass(dic)

    def test_copy_repo_to_prod(self):
        copy_repo_to_prod(self.config, install='new')

        progs = self.config.getp('paths', 'progs')
        confdir = self.config.getp('paths', 'conf')

        self.assertTrue(progs.exists())
        self.assertTrue(progs.is_dir())
        self.assertTrue(Path(progs, 'manage.py').exists())
        self.assertTrue(Path(progs, 'myapp').exists())
        self.assertTrue(Path(progs, 'djangoLudd21').exists())
        self.assertTrue(Path(progs, 'djangoLudd21', 'wsgi.py').exists())
        self.assertTrue(Path(progs.parent, 'other programs').exists())

        self.assertTrue(confdir.exists())
        self.assertTrue(confdir.is_dir())
        self.assertTrue(Path(confdir, 'settings.py').exists())
        self.assertTrue(Path(confdir, 'pwd.txt').exists())
        """ check for media_progs"""
        self.assertTrue(Path(self.fakeserver, 'var', 'opt', self.project_name, 'media').exists())
        self.assertTrue(Path(self.fakeserver, 'var', 'opt', self.project_name, 'media').is_dir())
        self.assertTrue(Path(self.fakeserver, 'var', 'opt', self.project_name, 'media').exists())
        self.assertTrue(Path(self.fakeserver, 'var', 'opt', self.project_name, 'media', 'progs_media').exists())
        self.assertTrue(Path(self.fakeserver, 'var', 'opt', self.project_name, 'media', 'progs_media').is_dir())
        """check for static"""
        self.assertTrue(Path(self.fakeserver, 'var', 'cache', self.project_name, 'cache').exists())

    @ classmethod
    def tearDownClass(cls):
        #shutil.rmtree(cls.fakeserver, ignore_errors=True)
        cls.sh_chown_patcher.stop()
        cls.chmod_patcher.stop()


#@unittest.skip('temporaire')
class Test_ProdFilesDistribution_extra_dir(FilesTestSuperClass):
    """ test copy_repo with a top_dir'"""

    @ classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)
        cls.data = Path(cls.datadir, cls.__name__)
        #this is destination of dispatched files
        cls.fakeserver = Path(cls.data, 'fake_server')
        #this is source of dispatched files
        cls.source_repo = 'fake_repo_top_dir_do_not_delete_ludd21'
        cls.source_copy = Path(cls.data, 'source_fake_repo_top_dir_do_not_delete_with_cfg')
        dic = {'django_user': 'toto_user',
               'domain': '1.2.3.4',
               #'host_ip': '1.2.3.4',
               'project_name': 'ludd21',
               'env_type': 'prod',
               'base_dir': Path(cls.source_copy, 'webLudd21'),
               'confname': 'djangoLudd21',
               'top_dir': cls.source_copy,
               'venvname': 'myvenv',
               'dns': 'True',
               'extra_name': 'source_fake_extra_repo_do_not_delete'
               }
        cls.extra = Path(cls.datadir.parent, 'fake_extra_repo_do_not_delete')
        #remove from previous test
        destination = Path(cls.data, 'source_fake_extra_repo_do_not_delete')
        shutil.rmtree(destination, ignore_errors=True)
        shutil.copytree(cls.extra, destination)

        super().setUpClass(dic)

    def test_copy_repo_to_prod(self):
        copy_repo_to_prod(self.config, install='new')

        progs = self.config.getp('paths', 'progs')
        confdir = self.config.getp('paths', 'conf')

        self.assertTrue(progs.exists())
        self.assertTrue(progs.is_dir())
        self.assertTrue(Path(progs.parent.parent, 'source_fake_extra_repo_do_not_delete').exists())
        self.assertTrue(Path(progs.parent.parent, 'source_fake_extra_repo_do_not_delete').is_dir())
        self.assertTrue(Path(progs.parent.parent, 'source_fake_extra_repo_do_not_delete', 'extra_code').exists())
        self.assertTrue(Path(progs.parent.parent, 'source_fake_extra_repo_do_not_delete', 'extra_code').is_dir())

    @ classmethod
    def tearDownClass(cls):
        #shutil.rmtree(cls.fakeserver, ignore_errors=True)
        cls.sh_chown_patcher.stop()
        cls.chmod_patcher.stop()


if __name__ == '__main__':
    import unittest

    unittest.main()
