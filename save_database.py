#!/usr/bin/env python
# coding:utf-8
"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL

these command save data of the database either as a form of mysqldump or as a form of .yaml under a fixture directory
only save as yaml is tested"""


from pathlib import Path
import sys
import os
import subprocess
import datetime
from django.core import management
from django.conf import settings


ADMIN_EXCLUDE_TABLES = ['django_admin_log', 'django_content_type', 'django_session', 'django_migrations']


def save_as_mysql_dumpdb(user, pwd, dbname, dumps_path, exclude_admin=False):
    """ dump a database with or without the tables for admin
    not tested
     """
    command = ['mysqldump', f'-u{user}', f'-p{pwd}', f'{dbname}', f'--extended-insert', f'--no-tablespaces']

    exclude_command = [f'--ignore-table={dbname}.{table_name}' for table_name in ADMIN_EXCLUDE_TABLES]
    dump_file = Path(dumps_path, datetime.date.today().__str__().replace('-', '_', 2) + '_dump.sql')
    if exclude_admin:
        total_command = command + exclude_command
    else:
        total_command = command
    try:
        with open(dump_file, 'w') as f:
            completedProcess = subprocess.run(total_command,
                                              stdout=f,
                                              check=True,
                                              encoding='utf-8'
                                              )
    except subprocess.CalledProcessError as err:
        print(f'dump of database failed')
        print(f'error : {err.stderr}')
        print(f'command ; {err.cmd}')
        print(f'arguments : {err.args}')
        exit()

    if not completedProcess.stdout:
        print('dump terminated')
    else:
        print(completedProcess.stdout)


def save_app_all_data(appname, filepath):
    management.call_command('dumpdata', appname, '--all', '--exclude', 'auth',
                            '--format', 'yaml', '--indent', '4', '--o', filepath)
    sys.stdout.write(f'dumped {appname} to {filepath}')
    try:
        authapp = settings.AUTH_USER_MODEL.split('.')[0]
    except AttributeError:
        authapp = None
    if authapp and authapp == appname:
        fixture, prefix = get_prefix_suffix()
        authfilepath = Path(*filepath.parts[0:-1], prefix + '_auth')
        management.call_command('dumpdata', 'auth', '--format', 'yaml', '--indent', '4', '--o', authfilepath)
        sys.stdout.write(f'dumped auth database to {authfilepath}')


def save_data(appname, datadict, filepath):
    import contextlib
    with open(filepath, 'w') as f:
        with contextlib.redirect_stdout(f):
            for key, value in datadict.items():
                app_model = appname + '.' + str(key)
                pks = ','.join(map(str, value))
                management.call_command('dumpdata', app_model, '--pks', pks,
                                        '--format', 'yaml', '--indent', '4')
    sys.stdout.write(f'dumped database extract to {filepath}')


def get_prefix_suffix():
    """ returns file names with date prefix + test """

    if settings.DATABASES['default']['NAME'].startswith('test_'):
            fixture_path = Path('tests', 'tests_data', 'fixtures')
            prefix = 'test_' + datetime.date.today().__str__().replace('-', '_', 2)
    else:
        fixture_path = Path('fixtures')
        prefix = datetime.date.today().__str__().replace('-', '_', 2)
    return fixture_path, prefix


def save_as_yaml(app, data=None, ):
    """if data is none save all database else save data"""
    from django.conf import settings
    app_path = next((p for p in settings.BASE_PROGS.iterdir() if p.is_dir() and p.parts[-1] == app))
    fixture_path, prefix = get_prefix_suffix()
    if data:
        filepath = Path(app_path).joinpath(fixture_path, prefix + '_database_extrait')
        save_data(app, data, filepath)
    else:
        filepath = Path(app_path).joinpath(fixture_path, prefix + '_database')
        save_app_all_data(app, filepath)





