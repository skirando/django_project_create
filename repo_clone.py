#!/usr/bin/env python
# coding:utf-8
"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

Module purpose
==============
repo_clone copy the repository, checks that directory structure is OK, create vitual env and create env.cfg

Implements
==========


Documentation
=============


Usage
=====

@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""
from pathlib import Path
import os
import argparse
import shutil
import sys

import helpers as h
import dispatch_files


def email(string):
    """ check that email is correct"""
    import re
    # this is a copy of the regex from django
    regex = re.compile(
        r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*"  # dot-atom
        r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-011\013\014\016-\177])*"'  # quoted-string
        r')@(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?$', re.IGNORECASE)  # domain
    if not re.fullmatch(regex, string):
        raise argparse.ArgumentTypeError(f'{string} is not a correct email')
    return string


def subparser_add_args(subparser):
    """ creates the parser"""

    subparser.add_argument('project_name_directory',
                           help='the name of the directory which will be created under the current dir to copy the repo',
                           )

    subparser.add_argument('url',
                           help='the url of the remote repository',
                           )

    subparser.add_argument('email_admin',
                           help="""in production environment, will be used by django to warn you in case of errors""",
                           type=email)

    subparser.add_argument('env_type',
                           choices=['prod', 'dev', 'test'],
                           help='type of environment to be created ',
                           )
    subparser.add_argument('user_admin',
                           help=f'''login of the user who will be administrator of the mysql database and a super user of the django application;
                           this is not the django_user who will be executing the django application''')

    subparser.add_argument('domain', type=domain,
                           help='''domain name for your application, that is DNS name; if no DNS (on a local network) enter any name;
                           you will need to add this name to every client's etc/hosts''')


def domain(string):
    if string == 'DEFAULT':
        raise argparse.ArgumentTypeError(f'{string} is not accepted; enter either domain name if you have a dns or anystring if you do not have dns')
    else:
        return string


def parse_args(subparser, args):
    """ here are additional arguments for production environment"""

    namespace, list_unknowns = subparser.parse_known_args(args)

    if vars(namespace)['env_type'] == 'prod':
        additional_parser = argparse.ArgumentParser(description='for prod environment, additional parameters are required:')

        django_user_help = 'login of the system user who will run the django programm, required for prod configuration; this is not the useradmin'
        additional_parser.add_argument('--django_user', help=django_user_help, required=True)

        dns_help = ''' if your are on a local network with no DNS enter False otherwise enter True'''
        additional_parser.add_argument('--dns', help=dns_help, choices=['false', 'true', 'False', 'True'], required=True)

        #extra_help = ''' if another repository is required add a second url here'''
        #additional_parser.add_argument('--extra_url', help=extra_help, required=False)

        namespace = additional_parser.parse_args(list_unknowns, namespace)

    return namespace


def create_cfg(**kwargs):
    """writes the first elements of the config file immediately after copying the repo"""

    if kwargs['env_type'] == 'prod':
        try:
            config = dispatch_files.ProdConfigParser(**kwargs)
        except ValueError as e:
            print(e.args)
            return 1
    else:
        try:
            config = dispatch_files.DevConfigParser(**kwargs)
        except ValueError as e:
            print(e.args)
            return 1

    return config


def undo_clone_git(cwd, project_name):
    """ config might no yet be created so use project_name rather than config"""
    print('cloned repository erased')
    repo_paths = Path(cwd, project_name)
    if repo_paths.exists() and repo_paths.is_dir():
        shutil.rmtree(repo_paths, ignore_errors=True)
    return


def get_base_dir(base_repo):
    """returns 1 if there is no manage.py file, 0 if manage.py is under base_repo otherwise returns the path for basedir where is manage.py"""
    if h.is_under(base_repo, 'manage.py'):
        return 0
    else:
        base_dir_repo = h.get_base_dir_repo(base_repo)
    if base_dir_repo:
        return base_dir_repo
    else:
        return 1


def clone_git(cwd, **args):
    """ call bash for git clone because requires password etc...."""
    url = args.pop('url')
    project_name_directory = args['project_name_directory']
    project_name = str(project_name_directory)
    environs = {'URL': url,
                'PRONAME': project_name}

    os.environ.update(environs)
    #do not erase directory if it is not empty as it could be just a typing error
    script = Path(Path(__file__).parent, 'script_git_clone.bash')
    returncode = h.call_bash(script, f'git repository {url} cloned',
                             f'could not clone git repository {url} in {project_name}',
                             okerror=None,
                             fail_action=None,
                             arguments=[cwd, project_name_directory])

    if returncode != 0:
        return 1
    #check directories
    #base_repo = args['base_repo']
    base_repo = Path(cwd, project_name_directory)
    res = get_base_dir(base_repo)
    if res == 1:
        undo_clone_git(cwd, project_name)
        return 1
    if res == 0:
        args.update({'base_dir': base_repo})
        args.update({'top_dir': base_repo})
    else:
        #this is a configuration with topdir
        #print(f"""top_dir configuration detected top_dir is {base_repo}
        #base_dir is {res}
        #make sure all necessary modules and software required for you django project are installed in the {base_repo}""")
        args.update({'top_dir': base_repo})
        args.update({'base_dir': res})
    return args


def handle_dango_user(config):
    django_user = config.get('users', 'django_user')
    environs = {'DJANGO_USER': django_user,
                }
    os.environ.update(environs)
    import grp
    try:
        user_exists = grp.getgrnam(django_user)
    except KeyError:
        user_exists = None
    if not user_exists:
        script = Path(Path(__file__).parent, 'script_create_django_user.bash')
        return_code = h.call_bash(script,
                                  f'user {django_user} created',
                                  f'could not create {django_user} ',
                                  okerror=[9])
        return return_code
    else:
        print(f'django_user {django_user} already exists')
        return 0


def create_venv(config, req_path):
    """ create the venv and activate venv"""
    env_type = config.get('configuration', 'env_type')
    if sys.prefix != sys.base_prefix:
        #we are running into a existing virtualenv
        #this might be usefull for dev configuration on machines whose system python is not as recent
        #as what is required for the project
        #as the repoclone command must be ran as sudo in prod
        #an existing environment can only be detected if it is created as sudo
        venv_path = os.environ.get('VIRTUAL_ENV')
        config['paths']['venv'] = venv_path
        print(f''' virtual environment {venv_path} is activated''')
        message = f'do you want to use {venv_path} for your django project?'
        returncode = h.valid_message(message)
        if returncode != 0:
            return returncode
        else:
            print(f'requirements will be installed here {venv_path}')
    else:
        venv_path = config.get('paths', 'venv')
        os.environ['VENV_PATH'] = venv_path
        script = Path(Path(__file__).parent, 'script_create_venv.bash')
        returncode = h.call_bash(script, f'virtual env {venv_path} created',
                                 f'''could not create the virtual env
                             make sure /usr/bin/virtualenv exists
                             if virtualenv is not installed do :sudo apt install python3-virtualenv
                             we use virtualenv rather than standard builtin python venv
                             because is has additional functionalities ''')
        if returncode != 0:
            return returncode

    print('starting installation of requirements')
    conf_reqs = Path(req_path, env_type + '.txt')
    if h.with_site_packages(venv_path):
        #this might be useful for an environment whose python is not recent enough for the project
        print('virtual env authorizes access to site_packages')
        pipargs = ['install', '--user', '-r', str(conf_reqs)]
    else:
        pipargs = ['install', '-r', str(conf_reqs)]
    returncode = h.call_pip(venv_path, pipargs)
    if returncode != 0:
        if env_type == 'prod':
            sudo = 'sudo'
        else:
            sudo = ''
        print('''installation of requirements failed ; look into the output for detailed error messages. hints to help solve the issues:\n''')
        if config.get('database', 'dbtype') == 'mysql':
            print('''check into package documentation mysql_and_other_dependencies for all LINUX packages required for using mysql''')
        print(f''' if a specific package was not installed, check compatibility constraints between the python version and your requirements file''')
        print(f''' if necessary change your requirements file on your repository''')
        print(f'''it might also be caused by the system pythion of the machine which would not meet the requirements of somme packages
        in this case you can create the virtual env with --system-site-packages option before launching repo-clone''')
        print(f'you can launch again the repo-clone command')
    return returncode


def handle_database_type(cwd, req_path, config):
    try:
        with open(Path(req_path, 'database_requirements.txt'), 'r') as f:
            #read only the first line
            comment_line = f.readline()
            dbtype = f.readline().strip()
            print(f'database type is {dbtype}')

    except FileNotFoundError:
        print('no database requirements found; use sqlite')
        dbtype = 'sqlite'
    env_type = config.get('configuration', 'env_type')

    from shutil import which
    if dbtype == 'mysql' and env_type != 'prod':
        # check that mysql server is installed
        # apt install does not work correctly when called from python script so keep the bash script outside
        #all packgages to install are listed in script_install_bash
        # ideal would be to use a bash script which reads requirements for LINUX packages one for prod and one for dev
        # see here : https://askubuntu.com/questions/519/how-do-i-write-a-shell-script-to-install-a-list-of-applications

        if not which('mysql') and env_type != 'prod':
            print("""it seems that mysql-server and mysql-client are not installed while you intend to use mysqldatabase;\n
            see in package documentation mysql_and_other_dependencies the list of needed packages""")
            undo_clone_git(cwd, config.get('pnames', 'project_name'))
            return 1
    if dbtype == 'mysql' and env_type == 'prod' and (not which('nginx') or not which('mysql')):
        print('it seems that mysql_server and mysql_client are not installed while you intend to use mysqldatabase.')
        print(' for a production environment, you need mysql-server and mysql-client plus nginx-light')
        print('see in documentation mysql_and_other_dependencies the list of needed packages')
        undo_clone_git(cwd, config.get('pnames', 'project_name'))
        return 1
    config.set('database', 'dbtype', dbtype)
    return 0


def handle_IP_dns(config, dns):
    """ set dns"""
    #IPaddress = h.get_IPaddress()
    #config.set('host', 'host_ip', IPaddress)
    if dns and dns.title() == 'True':
        config.set('host', 'dns', 'True')
    else:
        config.set('host', 'dns', 'False')


def mmain(namespace, cwd):

    args = vars(namespace)
    if args['env_type'] == 'prod' and not h.check_root():
        return

    args = clone_git(cwd, **args)
    if isinstance(args, int) and args == 1:
        ##it is an error code instead of args
        return
    venv_name = h.DEFAULT_VENV_NAME
    args['venvname'] = venv_name

    try:
        project_root = h.get_project_root_repo(args['base_dir'])
    except FileNotFoundError as e:
        print(f'check you are in the project directory')
        return
    confname = project_root.stem
    args['confname'] = confname
    req_path = Path(project_root, 'requirements')

    args['project_name'] = args['project_name_directory']
    args.pop('project_name_directory')

    #args = clone_extra(cwd, **args)
    #if isinstance(args, int) and args == 1:
        ##it is an error code instead of args
        #return

    config = create_cfg(**args)
    if config == 1:
        return

    returncode = handle_database_type(cwd, req_path, config)
    if returncode != 0:
        undo_clone_git(cwd, args['project_name'])
        return
    dns = args.get('dns', None)
    handle_IP_dns(config, dns)

    config.create_paths_section()

    log_dir = config.getp('paths', 'logs')
    log_dir.mkdir(exist_ok=True, parents=True)
    env_type = config.get('configuration', 'env_type')
    if env_type == 'prod':
        return_code = handle_dango_user(config)
        if return_code != 0:
            return return_code
        else:
            print('django user created')
    return_code = create_venv(config, req_path)
    if return_code != 0:
        undo_clone_git(cwd, args['project_name'])
        return
    cfg_path = h.get_cfg_path_repo(project_root)
    if cfg_path.exists():
        print(f"""it appears that {cfg_path} already exists and therefore is stored in git configuration.
        env.cfg file should not be in configuration as it is different for production, development and test configurations.
        add env.cfg to the gitignore file
        existing env.cfg is going to be deleted
        """)
        cfg_path.unlink()
    with open(cfg_path, 'w') as configfile:
        config.write(configfile)

    # add some utilities to the project
    import shutil
    origin = Path(__file__).parent
    dest = config.get('repo_paths', 'base_dir')
    shutil.copyfile(Path(origin, 'my_manage.py'), Path(dest, 'my_manage.py'))
    #keep track of installed requirements for future updates
    conf_path = config.getp('paths', 'conf')
    dest = Path(conf_path, 'installed_requirements')
    shutil.copytree(req_path, dest, dirs_exist_ok=True)
    print(f"""repo_clone completed""")
    base_dir = config.get('repo_paths', 'base_dir')
    top_dir = config.get('repo_paths', 'top_dir')
    if base_dir != top_dir:
        #it is a top_dir configuration
        print(f"""top_dir configuration detected top_dir is {top_dir}
        base_dir is {base_dir}
        make sure all necessary modules and software required for you django project are installed in the {top_dir}
        after this command. if necessary do git_clone to install any software from a repo
        
        """)
    if config.get('database', 'dbtype') == 'mysql':
        print(""" next step is mysql_init""")
        h.prepare_for_virtual_env(config, 'mysql_init')
    else:
        print("""next step is  install_env""")
        h.prepare_for_virtual_env(config, 'install_env')


if __name__ == '__main__':
    import os
    from pathlib import Path
    import argparse
    import sys
    cwd = Path.cwd()

    parser = argparse.ArgumentParser()
    subparser_add_args(parser)
    ns = parse_args(parser, sys.argv[2:])
    mmain(ns, cwd)




















