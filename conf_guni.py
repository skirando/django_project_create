#!/usr/bin/env python
# coding:utf-8
"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""

import shutil
from pathlib import Path

import create_django_env.conf_dma
import create_django_env.helpers as h


def subparser_add_args(subparser):
    """ creates the parser"""
    pass


def parse_args(subparser, arguments):
    return subparser.parse_args(arguments)


def mmain(namespace, cwd):
    if not h.check_root():
        return

    #in this command the files were dispatched into the different repo; in production environment its is different from the repo env.cfg
    #import the get_cfg_path function from the settings in the repo to find the real env.cfg file.
    base_dir = h.get_base_dir_repo(cwd)
    if not base_dir:
        return
    project_root = h.get_project_root_repo(base_dir)
    if not project_root:
        return
    function = conf_dma.get_settings_module_cfg_path_function(project_root)
    cfg_path, _ = function()
    config = h.PathConfigParser()
    try:
        with open(cfg_path, 'r') as f:
            config.read_file(f)
    except FileNotFoundError:
        print(f'cannot read config file : {cfg_path}')
        return
    project_name = config.get('pnames', 'project_name')
    source = Path(Path(__file__).parent, 'prod_files', 'gunicorn_files', 'service.txt')
    parts = ['systemd', 'system', project_name + '.service']
    dest = service_file = conf_dma.get_etc(parts)
    shutil.copyfile(source, dest)

    project_name = config.get('pnames', 'project_name')

    django_user = config.get('users', 'django_user')
    wsgi = project_root.stem
    dic = {'$DJANGO_USER': django_user,
           '$DJANGO_GROUP': django_user,
           '$DJANGO_PROJECT': project_name,
           '$WSGI': wsgi,
           '$BASELOG': config.get('paths', 'logs'),
           '$VENV': config.get('paths', 'venv'),
           '$BASEPROGS': config.get('paths', 'progs'),
           '$BASECONF': config.get('paths', 'conf'),
           '$BASE_VENV': config.get('paths', 'venv'),
           }

    with open(service_file, 'r') as f:
        texte = conf_dma.update_conf_file(f, dic)
    with open(service_file, 'w') as f:
        f.write(texte)

    #check that gunicorn is correctly configured
    package = Path(__file__).parent
    print(f"""to check that gunicorn is configured :
    type in : service {project_name} start; you django app  should be available
    to automatically start the django app at boot type in :
    systemctl enable {project_name}
    
    its now time to configure dma; Type in:
    python3 {package} conf_dma -h    
    """)


if __name__ == '__main__':

    print('in conf_dma')
    # print(sys.path)
    # import wingdbstub
    cwd = Path.cwd()
    mmain(sys.argv[1:], cwd)



















