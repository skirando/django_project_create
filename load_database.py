#!/usr/bin/env python
# coding:utf-8
"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""


from pathlib import Path

from django.core import management
import subprocess


import sys


def get_appPathsNames(settings, base_dir):
    """ returns a list of tuples (app_path, app_name) all applications which have a corresponding path in the base_dir directory
    returns also the name of the application which includes the custom usermodel"""
    applications = settings.INSTALLED_APPS
    app_paths_names = [(path, app) for path in base_dir.iterdir() for app in applications
                       if path.is_dir() and path.parts[-1] == app]
    try:
        authapp = settings.AUTH_USER_MODEL.split('.')[0]
    except AttributeError:
        authapp = None
    return app_paths_names, authapp


def get_dbname(settings):
    databases = settings.DATABASES.keys()
    if len(databases) > 1:
        raise ValueError(f'this command can load database only if there is a single database')
    else:
        database_settings = settings.DATABASES[list(databases)[0]]
    return database_settings['NAME']


def get_most_recent(fixture_dir, suffix='.yaml', withauth=False):
    """ returns the most recent fixture sorted with the date prefix on 10 characters
    suffix can be replaced by another prefix such as '.sql'"""

    fixtures = [f for f in fixture_dir.iterdir() if f.is_file()
                and f.suffix == suffix
                and f.match('*database*')
                and f.name[0].isdigit()]
    s = slice(0, 10)

    if fixtures:
        fixture = max(fixtures, key=lambda f: f.stem[s])
    else:
        raise (f'cannot find most_recent fixture in {fixture_dir} ')
    if withauth:
        withauth = next((f for f in fixture_dir.iterdir() if f.stem[0: 10] == fixture.stem[0: 10] and f != fixture))
        if not withauth:
            print(f'could not load authfixture corresponding to {fixture}')

    return fixture, withauth


def load_fixture(filename, appname, dbname, withauth=None):
    """ load a yaml fixture into database dbname """

    try:
        management.call_command('loaddata', filename, '--app', appname)
        sys.stdout.write(f'loaded file {filename} in {dbname}')
    except Exception as e:
        sys.stderr.write(e.args[0])
        sys.stderr.write(f'could not load file {filename} into database {dbname}\n')
        raise e
    if withauth:
        # loads the data corresponding to the authentication app
        try:
            management.call_command('loaddata', withauth, '--app', 'auth')
            sys.stdout.write(f'loaded file {withauth} in {dbname} \n')
        except Exception as e:
            sys.stderr.write(e.args[0])
            sys.stderr.write(f'could not load file {withauth} into database {dbname}\n')
            raise e


def load_database_yaml_fixture(settings, base_dir, loadauth=True):
    """loads the database with most recent fixture for all apps"""
    dbname = get_dbname(settings)
    appPathsNames, authapp = get_appPathsNames(settings, base_dir)
    for path, app in appPathsNames:
        fixture_path = Path(path, 'fixtures')
        filename, authfilename = get_most_recent(fixture_path, withauth=authapp)
        if loadauth:
            withauth = authfilename
        else:
            withauth = None
        load_fixture(filename, app, dbname, withauth=withauth)
    return


    
    
def load_database_mysql_dump(config, dump_file, settings):
    """this function is not tested"""
    """loads the database with most recent dump
    commands for loading a dump do not exist in mysqlclient (MySQLdb module), after connecting to the database
    chosen solution run mysql command in a subprocess
    because dumps are global for the database and not by apps, the dumps directory should be under the project root ( with settings)s"""

    dbname = get_dbname(settings)
    confpath = config.getp('paths', 'conf')
    dbuser = config.get('users', 'user_admin')
    with open(Path(confpath, 'pwd.txt'), 'r') as f:
        pwd = f.read()

    command = ['mysql', f'-u{dbuser}', f'-p{pwd}', dbname]
    try:
        with open(dump_file, 'r') as f:
            completedProcess = subprocess.run(command,
                                              stdin=f,
                                              check=True,
                                              capture_output=True,
                                              encoding='utf-8'
                                              )
    except subprocess.CalledProcessError as err:
        print(f'loading of database failed')
        print(f'error : {err.stderr}')
        print(f'command ; {err.cmd}')
        print(f'arguments : {err.args}')
        print(f'{err.stdout}')
        exit()

    if completedProcess.stdout:
        print(completedProcess.stdout)
    else:
        print(f'database loaded with {dump_file}')

    return

if __name__ == '__main__':
    from create_django_env import helpers as h
    from django.conf import settings
    cwd = Path('/home/christian/Documents/Dev_logiciel/projet_recettes/test_load_dump/recettes_load_dump')
    base_dir = cwd
    project_root = h.get_project_root_repo(base_dir)
    dump_file=Path(project_root,'dumps')
    cfg_path = Path(project_root, 'env.cfg')
    config = h.PathConfigParser.create_from_file(cfg_path)   
    env_type = config.get('configuration', 'env_type')
    dbname = config.getp('database', 'dbname')
    load_database_mysql_dump(config, dump_file, settings)
















