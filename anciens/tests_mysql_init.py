#!/usr/bin/env python
# coding:utf-8
"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""
from pathlib import Path
import unittest
from unittest.mock import patch
import argparse
import MySQLdb as mysql


import mysql_init as mysql_init
import helpers as h
from tests.tests_super_class import TestSuperClass


#unittest.skip
class TestMySql_Init_mock_database (TestSuperClass):

    def setUp(self):
        self.get_dirs(__file__)
        self.pwd_patcher = patch('getpass.getpass', return_value='pwd')
        self.pwd_patcher.start()
        self.args_dict = {'dbname': 'dbname',
                          }

    def test_main_OK(self):
        self.prepare_cfg('database')

        argsnamespace = argparse.Namespace(**self.args_dict)
        #cwd is not used
        cwd = Path(Path(__file__).parent, 'tests_data', 'tests_mysql_init')

        return_code = 0

        with patch('mysql_init.h.check_root', return_value=True) as f:
            with patch('mysql_init.h.get_project_root_repo', return_value=self.project_root) as p0:
                with patch('mysql_init.h.get_cfg_path_repo', return_value=self.cfg_path) as p1:
                    with patch('mysql_init.create_mysql_user_database', return_value='pwd') as p2:
                        mysql_init.mmain(argsnamespace, cwd)

        self.assertTrue(Path(self.project_root, 'pwd.txt').is_file())
        config = h.PathConfigParser()
        with open(self.cfg_path, 'r') as f:
            config.read_file(f)
        self.assertEqual(config.get('database', 'dbname'), self.args_dict['dbname'])

    def tearDown(self):
        self.pwd_patcher.stop()


if __name__ == '__main__':
    import unittest

    unittest.main()
