"""
Django settings for Recettes project."""
from pathlib import Path
# import environ
import os
import configparser


""" the 2 functions beto 
in prod configuration, a symlink is in the myvenv path linking to the env.cfg file
this module needs to be in conf for each project"""


def get_project_root_repo(base_dir_repo):
	# PROJECT_ROOT is where settings.py is
	try:
		project_root = next((child for child in base_dir_repo.iterdir() if child.is_dir() and
		                     any(file.is_file() and file.match('settings.py') for file in child.iterdir())))
		return project_root
	except StopIteration:
		return None


def get_cfg_path():
	"""returns the path of the env.cfg file"""
	virtual_env_path = os.environ.get('VIRTUAL_ENV')
	if not virtual_env_path:
		error_message = (f'you are not in a virtual environment ;\n'
		                 f'to activate virtual env type;\n'
		                 f'source <nameofyourvenv>/bin/activate \n'
		                 )
		raise ValueError(error_message)
	cfg_path = Path(virtual_env_path, 'env.cfg')
	if cfg_path.is_symlink():
		""" it is a production environment; a symlink towards the env.cfg file has been created"""
		return cfg_path.resolve()
	else:
		"""it is a dev or test configuration; the cfg file is under the project_root"""
		base_dir_repo = Path(virtual_env_path).parent
		project_root = Path(get_project_root_repo(base_dir_repo))
		return Path(project_root, 'env.cfg')


cfg_path = get_cfg_path()
config = configparser.ConfigParser()
with open(cfg_path, 'r') as f:
	config.read_file(f)
try:
	env_type = config.get('configuration', 'env_type')
	dbname = config.get('database', 'dbname')
	dbuser = config.get('database', 'dbuser')
	email = config.get('users', 'email')
	IPAdress = config.get('host', 'host_ip')
	BASE_CONF = Path(config.get('paths', 'conf'))
	BASE_MEDIA = Path(config.get('paths', 'media'))
	BASE_LOGS = Path(config.get('paths', 'logs'))
	BASE_PROGS = Path(config.get('paths', 'progs'))
	BASE_REPO = Path(config.get('repo_paths', 'base_dir'))
	BASE_STATICS = Path(config.get('paths', 'statics'))
except (configparser.NoOptionError, configparser.NoSectionError):
	raise ValueError(f'{cfg_path} is not properly configured')
try:
	server_email = config.get('dma', 'server_email')
	default_from_email = config.get('dma', 'default_from_email')
	email_host = config.get('dma', 'email_host')
	email_host_user = config.get('dma', 'email_host_user')
	email_host_password = config.get('dma', 'email_host_password')
	email_port = config.get('dma', 'email_port')
	email_use_tls = bool(config.get('dma', 'email_use_tls'))
	email_use_ssl = bool(config.get('dma', 'email_use_ssl'))
	mail = True
except configparser.NoSectionError:
	mail = False
except configparser.NoOptionError:
	raise ValueError(f'{cfg_path} mail section is not properly configured')


with open(Path(BASE_CONF, 'pwd.txt'), 'r') as f:
	pwd = f.read()
with open(Path(BASE_CONF, 'secret_key'), "r")as f:
	SECRET_KEY = f.read()

# SECURITY WARNING: don't run with debug turned on in production!
# DEBUG = True

if env_type == 'prod':
	DEBUG = False
if env_type == 'test' or env_type == 'dev':
	DEBUG = True

# the IP adresses on which the django application is acessible.
# the 1rst 2 IP adresses are used for development and the last one is the IP address on local network

ALLOWED_HOSTS = ['localhost', '127.0.0.1', IPAdress]

# Application definition

INSTALLED_APPS = [
    'dal',
    'dal_select2',
    # 'bootstrap4',
    'widget_tweaks',
    # 'bootstrap4', créé des tags et des templates de type bootstrap-form,...
    # auth créé les tables de permissions du modèle, les tables de groupes et d'utilisateurs
    'django.contrib.auth',
    # 'accounts',
    'django.contrib.admin',
    # suit l'ensemble des tables contenues dans le modèle avec un framework associé
    'django.contrib.contenttypes',
    # contient l'historique des sessions
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'mesrecettes',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'Recettes.urls'

AUTH_USER_MODEL = 'mesrecettes.CustomUser'

"""peut_on mettre debug = TRUE en prod ?"""
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_PROGS / 'templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'debug': True,
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'Recettes.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases


DATABASES = {
    'default': {
	'ENGINE': 'django.db.backends.mysql',
	'NAME': dbname,
	'USER': dbuser,
	'PASSWORD': pwd,
	'HOST': 'localhost',
	'PORT': '',
	'DEFAULT-CHARACTER-SET': 'utf8',
    }
}

# this is the configuration of the database for Django tests (where database is created by django)
# if Migrate = True django create test database with mysql and not sqlite3
DATABASES['default']['TEST'] = {
	# 'NAME': 'test_' + dbname + '_sqlite3',
    'USER': dbuser,
    'PASSWORD': pwd,
    # 'ENGINE': 'django.db.backends.sqlite3',
    'MIGRATE': True}


# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LOGIN_URL = 'login'
LOGIN_REDIRECT_URL = 'rechercher_une_recette'
# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = 'fr'
TIME_ZONE = 'Europe/Paris'
# used for display , must be expressed with template syntax
TIME_FORMAT = "H:i"
# used for input must be expressed in python syntax, can be overridden in form
TIME_INPUT_FORMATS = ['%H:%M']

USE_I18N = True

USE_L10N = True

USE_TZ = True


""" Static files (CSS, JavaScript, Images)
 https://docs.djangoproject.com/en/3.2/howto/static-files/
 STATIC_URL gives the path for static files in development. they are automatically served in development
 with the django.contrib.static app, files are looked into apps directory
 STATIC_ROOT is the location from which Django application will serve static files in production (DEBUG = False);
 this should be an empty directory
 for production use collectstatic to get all staticfiles into thestatic_ROOTdirectory
 STATICFILES_DIRS is a list of locations the static file app will use to look for files if the FileSystemFinder is enabled
 STATICFILES_FINDER by default contains 2 finders : the FilsystemFinder and APPDirectoriesFinder which looks into each app satic de chque app
 """
# this variable is used by collectstatic to generate the list of directories to scan
# could be a list or a list of tuples. in the latter case, first element of the tuple is a prefix, the second is a directory
# collectstatic looks for files in STATIC_ROOT/prefix
# in the template the static file can be named with static prefix file in the directory
#note : if the repo dir does not exists on the production server, when the django server
#is lauched with manage.py, that is as a development server, Django raises a warning
# do not care+

STATICFILES_DIRS = [
    ("static", Path(BASE_REPO, 'mesrecettes', 'static')),
 ]
# Url to be used when referring to static files located in STATIC_ROOT
# also used by the application looking for static files
STATIC_URL = '/static/'

MEDIA_ROOT = str(BASE_MEDIA)
MEDIA_URL = '/' + BASE_MEDIA.stem + '/'

# STATIC_ROOT is used only by collectstatic:
STATIC_ROOT = BASE_STATICS

#STATIC_ROOT = '/var/cache/recettes_for_git2/static'

# config/settings.py

if env_type == 'prod' and mail == True:
	EMAIL_BACKEND = "django_sendmail_backend.backends.EmailBackend"
	SERVER_EMAIL = server_email
	DEFAULT_FROM_EMAIL = default_from_email
	ADMINS = [(dbuser, email)]
	MANAGERS = ADMINS
	EMAIL_HOST = email_host
	EMAIL_HOST_User = email_host_user
	EMAIL_HOST_PASSWORD = email_host_password
	EMAIL_PORT = email_port
	EMAIL_USE_TLS = email_use_tls
	EMAIL_USE_SSL = email_use_ssl
if env_type == 'test' or env_type == 'dev' or mail == False:
	EMAIL_BACKEND = "django.core.mail.backends.filebased.EmailBackend"
	EMAIL_FILE_PATH = Path(BASE_LOGS, 'sent_emails')


LOGGING = {
	'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '[%(asctime)s] %(levelname)s: '
                      '%(message)s',
        }
    },
    'handlers': {
        'file': {
            'class': 'logging.handlers.'
                     'TimedRotatingFileHandler',
            'filename': str(Path(BASE_LOGS, str(BASE_LOGS).split("/")[-1])) + '.log',
            'when': 'midnight',
            'backupCount': 60,
            'formatter': 'default',
        },
    },
    'root': {
        'handlers': ['file'],
        'level': 'INFO',
    },
}
CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.filebased.'
            'FileBasedCache',
            'LOCATION': str(Path(BASE_STATICS, 'cache')),
        }
}
# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

if env_type == 'prod':
	SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

