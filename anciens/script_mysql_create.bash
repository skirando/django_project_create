#!/bin/bash
# the  authentication plugin caching_sha2_password is more secure than native_pass_word
# it is the default user authentication plugin in mysql 8 with some not up-to-date mysqlclient, the automatic plugin recognition protocol does not work
# as a consequence the created user cannot create new database which is a problem for test and dev
# sqlcommands_prod = """CREATE USER IF NOT EXISTS %s@'localhost'IDENTIFIED BY %s;"""

# the following CREATE USER creates the user with authentication plugin mysql_native_password
# this is less secure but allows test
# sqlcommands_dev = """CREATE USER IF NOT EXISTS %s@'localhost'IDENTIFIED WITH mysql_native_password BY %s;"""

if [ $ENV != "prod" ];
then
#watchout for environment variables whose names are common
sudo mysql -u root <<MY_QUERY
CREATE USER IF NOT EXISTS '$DUSER'@'localhost' IDENTIFIED WITH mysql_native_password by '$PASSINWORD';
CREATE DATABASE IF NOT EXISTS $DNAME ;
GRANT ALL ON *.* TO '$DUSER'@'localhost';
FLUSH PRIVILEGES;
MY_QUERY

else
sudo mysql -u root <<MY_QUERY 
CREATE USER IF NOT EXISTS '$DUSER'@'localhost' IDENTIFIED BY '$PASSINWORD';
CREATE DATABASE IF NOT EXISTS $DNAME ;
GRANT ALL ON *.* TO '$DUSER'@'localhost';
FLUSH PRIVILEGES;
MY_QUERY
fi

#sudo mysql_secure_installation

