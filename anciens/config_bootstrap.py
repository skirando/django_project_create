import os
from pathlib import Path

""" this module allows to get the env.cfg file which gives all paths required for the settings
the path of the cfg file is found from the virtual env environment variable
for dev and test configurations the cfg file is under project_root
in prod configuration, a symlink is in the myvenv path linking to the env.cfg file
this module needs to be in conf for each project"""


def get_project_root_repo(base_dir_repo):
    # PROJECT_ROOT is where settings.py is
    try:
        project_root = next((child for child in base_dir_repo.iterdir() if child.is_dir() and
                             any(file.is_file() and file.match('settings.py') for file in child.iterdir())))
        return project_root
    except StopIteration:
        return None


def get_cfg_path():
    """returns the path of the config file"""
    virtual_env_path = os.environ.get('VIRTUAL_ENV')
    if not virtual_env_path:
        error_message = (f'you are not in a virtual environment ;\n'
                         f'to activate virtual env type;\n'
                         f'source <nameofyourvenv>/bin/activate \n'
                         )
        raise ValueError(error_message)
    cfg_path = Path(virtual_env_path, 'env.cfg')
    if cfg_path.is_symlink():
        return cfg_path.resolve()
    else:
        base_dir_repo = Path(virtual_env_path).parent
        project_root = Path(get_project_root_repo(base_dir_repo))
        return Path(project_root, 'env.cfg')


# def get_settings(cfg_path):
    #""" we need settings module not settings path"""
    #module = Path(cfg_path).parts[-1] + '.settings'
    # return module
