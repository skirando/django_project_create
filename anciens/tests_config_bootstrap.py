#!/usr/bin/env python
# coding:utf-8
import unittest
from unittest.mock import patch
import create_django_env.config_bootstrap as conf_bootstrap
from pathlib import Path
import os


""" this test module is partial coverage for the modules in django_create_project
test are numbered within a class as they must be ran in this order

data needed for the tests :
- a copy of the repository to be cloned must be copied at the following spot
    Path(Path('__file__').parent, 'tests_data', 'tests_repo_clone', 'tobe_cloned_dir')

- a complete and correct cfg file under tests_repo_clone"""


class TestConfig_Bootstrap (unittest.TestCase):

    def setUp(self):
        self.cwd = Path(Path(__file__).parent, 'tests_data', 'tests_config_bootstrap')
        self.cfg_path = Path(self.cwd, 'base_dir', 'project_root', 'env.cfg')
        self.myvenv_repo = Path(self.cwd, 'base_dir', 'myvenv')
        self.myvenv_prod = Path(self.cwd, 'myvenv')

    def test1_get_cfg_path(self):

        with patch.dict(os.environ, {'VIRTUAL_ENV': str(self.myvenv_repo)}):
            cfg_path = conf_bootstrap.get_cfg_path()
        self.assertEqual(cfg_path, self.cfg_path)

        Path(self.myvenv_prod, 'env.cfg').symlink_to(self.cfg_path)
        with patch.dict(os.environ, {'VIRTUAL_ENV': str(self.myvenv_prod)}):
            cfg_path = conf_bootstrap.get_cfg_path()
        self.assertEqual(cfg_path, self.cfg_path)

    def tearDown(self):
        Path(self.myvenv_prod, 'env.cfg').unlink(missing_ok=True)


if __name__ == '__main__':
    import unittest

    unittest.main()
