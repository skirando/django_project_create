""" this command creates the environment for a django recettes application
the environement includes :
the packages included in the requirements file which are installed
the env.cfg file corresponding to a given environmenet : test or dev or prod
in the virtual env, the file containing the password and the name of the database
it runs migrations and populates the database with the content of the latest mysql dump_file in the dumps directory, or a fixture
or it runs inspectdb
finally it creates the superuser if it does not exist
"""


from pathlib import Path
import sys
import os
import argparse
import subprocess
import configparser
from django_management.mysql_init import base_dir


def email(string):
    """ check that email is correct"""
    import re
    # this ia a copy of the regex from django
    regex = re.compile(
        r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*"  # dot-atom
        r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-011\013\014\016-\177])*"'  # quoted-string
        r')@(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?$', re.IGNORECASE)  # domain
    if not re.fullmatch(regex, string):
        raise argparse.ArgumentTypeError(f'{string} is not a correct email')
    return string


def create_parser():
    """ creates the parser"""

    parser = argparse.ArgumentParser(description='''install packages required , create the .cfg file for django configuration ,
                                     loads the database and create the django amdmin superuser and makemigrations''',
                                     # prog=f'{Path(__file__).stem}',
                                     # usage=f'python3 -m management.{Path(__file__).stem} email'
                                     )

    parser.add_argument('email',
                        help='your user mail',
                        type=email,)
    parser.add_argument('--load',
                        choices=['fix', 'dump', 'inspect'],
                        help='omit to get an empty database, fix to load the most recent fixture, dump to load the most recent dump, inspect to run an inspectdb',
                        default='')
    parser.add_argument('--project_name_directory',
                        help='base directory if it is not the current directory',
                        type=base_dir,
                        )

    return parser


def read_arguments(arguments):
    parser = create_parser()
    args = parser.parse_args(arguments)
    return args.__dict__.values()


def install_packages(sysexec, req_path):
    """ call subprocess to install the packages in requirements"""
    try:
        # cannot use here sysexecutable because if ran in wing debug, sys.executable is wingdb

        completedProcess = subprocess.run([sysexec, '-m', 'pip', 'install', '-v', '-r', req_path],
                                          check=True,
                                          capture_output=True,
                                          encoding='utf-8'
                                          )
    except subprocess.CalledProcessError as err:
        print(f'installing of packages failed')
        print(f'error : {err.stderr}')
        print(f'command ; {err.cmd}')
        print(f'arguments : {err.args}')
        print(f'{err.stdout}')
        exit()
    print(completedProcess.stdout)


# def create_env_file(user, pwd):
    # the django-environment2 package allows to import environment variables which have been setup previoulsy in a script
    # here we use a python configuration file which has been created at the database creation (mysql_init)

    # import configparser

    # config = configparser.ConfigParser(dict_type=dict)
    # try:
        # config.read(env_file_path)
    # except FileExistsError as e:
        # print(f'fichier de configuration inexistant : {e.args}')
    # dbname = config.get('database', 'dbname')
    # env_type = config.get('database', 'env_type')

    # config.set('database', 'email', email)
    # config.set('database', 'user', user)
    # write now the other options of the config to make sure they are correctly read
    # with open(env_file_path, 'w') as configfile:
        # config.write(configfile)
    # return dbname, env_type


def inspect_db():
    """ create generic draft models.py"""
    # try:
        # completedProcess = subprocess.run([sysexec, '-m', 'manage.py', 'inspectdb'],
                                          # check=True,
                                          # capture_output=True,
                                          # encoding='utf-8'
                                          # )
    # except subprocess.CalledProcessError as err:
        # print(f'could not launch inspectdb')
        # print(f'error : {err.stderr}')
        # print(f'command ; {err.cmd}')
        # print(f'arguments : {err.args}')
        # print(f'{err.stdout}')
        # exit()
    from django.core import management
    with open('models.py', 'w') as file:
        management.call_command('inspectdb', stdout=file)
    print("""inspect_db created the model if the database had tables;
           check the model out, dont forget to put managed = True, move the model into your app
           then make migrations and migrate """)


def create_super_user(env_type, pwd, user, email):
    # user is the login who ran this script
    from django.core import management
    if env_type != 'prod':
        os.environ['DJANGO_SUPERUSER_PASSWORD'] = pwd
        os.environ['DJANGO_SUPERUSER_USERNAME'] = user
        os.environ['DJANGO_SUPERUSER_EMAIL'] = email
        try:
            management.call_command('createsuperuser', '--noinput')
            print(f'superuser {user} created')
        except management.base.CommandError as e:
            if not 'already taken' in e.args[0]:
                raise e
            print(f'no super user created; kept existing')
    else:
        print(f'you should create super user for django admin')


def create_secret_key():
    from django.core.management import utils
    secret_key = utils.get_random_secret_key()
    key_file = Path(project_root, 'secret_key')
    with open(key_file, 'w') as f:
        f.write(secret_key)


def create_django_env(user, dbname, pwd, email, env_type, load):
    """ create django environment"""
    import django
    create_secret_key()
    django.setup()
    print('django setup OK')
    if load and load == 'inspect':
        inspect_db()
        create_super_user(env_type, pwd, user, email)
        return

    from django.core import management
    management.call_command('makemigrations')
    management.call_command('migrate')
    print('migrations OK')


def load_db(user, dbname, pwd, email, env_type, load):
    if load and load == 'dump' and dumps_path.exists():
        from django_management import mysql_loaddb
        try:
            loaded = mysql_loaddb.mysql_loaddb(user, pwd, dbname, dumps_path)
        except:
            print(f'error while loading database with dump {dumps_path}')
            return

    elif load and load == 'fix' and fixtures_path.exists():
        from django_management import load_db_yaml
        try:
            loaded = load_db_yaml.load_db_yaml(fixture_dir=fixtures_path)
        except Exception as e:
            print(f'error while loading database with fixture {fixtures_path}')
            return
    try:
        print(f'database loaded with {loaded}')
    except UnboundLocalError:
        print(f'nothing loaded in the database')


def check_we_are_in_the_right_place():
    # get virtualenv path
    virtual_env_path = Path(os.environ.get('VIRTUAL_ENV'))
    if not virtual_env_path:
        error_message = (f'you are not in a virtual environment ;\n'
                         f'if a virtual env was created activate it by typing ;\n'
                         f'source <nameofyourvenv>/bin/activate \n'
                         f'if no virtual env was created, create one byt typing :\n'
                         f'python3 -m venv <nameofyourvenv> to create one;')
        raise ValueError(error_message)

    # BASE-DIR is where your manage.py is,
    base_dir = virtual_env_path.resolve().parent
    try:
        manage_file = next((file for file in base_dir.iterdir() if file.is_file()
                           and file.match('manage.py')))
    except StopIteration:
        raise ValueError(f''' imporperly configured project : base_dir is {base_dir}, virtual_env is {virtual_env_path}\n
    may be your virtual env is not properly in the right prod''')

    # PROJECT_ROOT is where settings.py is
    try:
        project_root = next((child for child in base_dir.iterdir() if child.is_dir()
                             and any(file.is_file() and file.match('settings.py') for file in child.iterdir())))
    except StopIteration:
        raise ValueError(f'''improperly configured project: base_dir : {BASE_DIR}, virtual_env :{virtual_env_path}
        no settings;py found in base_dir ''')
    cfg_path = Path(project_root, 'env.cfg')
    return project_root, base_dir, cfg_path


def update_config_file(project_root, base_dir, cfg_path):
    import socket
    config = configparser.ConfigParser()
    config.read(cfg_path)
    hostname = socket.gethostname()
    IPaddress = socket.gethostbyname(hostname)
    config['paths'] = {'project_root': project_root,
                       'base_dir': base_dir,
                       'virtual_env': virtual_env_path.stem
                       }
    config['host'] = {'host_IP': IPaddress}
    with open(cfg_path, 'w') as configfile:
        config.write(configfile)
    return (config.get('database', 'env_type'),
            config.get('database', 'dbname'),
            config.get('database', 'dbuser'),
            config
            )


if __name__ == '__main__':
    project_root, base_dir, cfg_path = check_we_are_in_the_right_place()
    virtual_env_path = Path(os.environ.get('VIRTUAL_ENV'))
    env_type, dbname, dbuser, config = update_config_file(project_root, base_dir, cfg_path)
    email, load, django_user = read_arguments(sys.argv[1:])
    if env_type == 'prod':
        import getpass
        user = getpass.getuser()
        if user != 'root':
            raise argparse.ArgumentError('for a production environment, you must run this python script as root')
        if not django_user:
            raise argparse.ArgumentError('for a production environment, you must supply a django_user')

    req_path = Path(project_root, 'requirements', env_type + '.txt')
    sysexec = Path(virtual_env_path, 'bin', 'python3')
    install_packages(sysexec, req_path)

    # create django environment
    pwd_file = Path(project_root, 'pwd.txt')
    with open(pwd_file, 'r') as f:
        pwd = f.read()
    # we need the module name in the environment variable
    os.environ['DJANGO_SETTINGS_MODULE'] = str(Path(project_root.parts[-1] + '.settings'))
    create_django_env(dbuser, dbname, pwd, email, env_type, load)

    # load database with dump or fixture
    fixtures_path = Path(project_root, 'fixtures')
    dumps_path = Path(project_root, 'dumps')
    load_db(dbuser, dbname, pwd, email, env_type, load)

    # create super user
    create_super_user(env_type, pwd, dbuser, email)
    del os.environ['DJANGO_SETTINGS_MODULE']

    if env_type == 'prod':
        from django_management.conf_prod import Prod_configuration, copy_repo_to_prod
        prod_conf = Prod_configuration()
        copy_repo_to_prod(config, prod_conf, django_user)
        # to be done : add the type of database in cfg file
        # to be done : if database is mysql run the script sudo mysql_secure_installation with the appropriate options




