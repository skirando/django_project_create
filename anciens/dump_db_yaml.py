""" this command dumps data for application mesrecettes et pour l'application auth
usage: can be called from admin or as a standalone command
if called from admin can dump a queryset"""

from django.core import management
from django.conf import settings
from django.apps import apps
from django.core.exceptions import AppRegistryNotReady
import datetime

from pathlib import Path
import os
import sys


def dump_data(data=None, from_admin=False):

    applications = settings.INSTALLED_APPS
    app_path = next(child for child in settings.BASE_DIR.iterdir() if child.is_dir() and child.parts[-1] in applications)
    app_name = app_path.parts[-1]

    if from_admin:
        # in dev or prod context
        fixture_path = Path(app_path, 'fixtures')
        path_prefix = datetime.date.today().__str__().replace('-', '_', 2)

    elif settings.DATABASES['default']['NAME'].startswith('test_'):
            fixture_path = Path(app_path, 'tests', 'tests_data', 'fixtures')
            path_prefix = 'test_' + datetime.date.today().__str__().replace('-', '_', 2) 
    else:
        raise 'cannot find where to save fixture'

    if 'django.contrib.auth' in applications:
        management.call_command('dumpdata', 'auth', '--format', 'yaml', '--indent', '4', '--o', str(Path(fixture_path, path_prefix + 'auth.yaml')))
        sys.stdout.write(f'dumped auth database to {str(Path(fixture_path, path_prefix + "auth.yaml"))}')
    # import ipdb
    # ipdb.set_trace()
    if not data or not isinstance(data, dict):
        management.call_command('dumpdata', app_name, '--all', '--exclude', 'auth',
                                '--format', 'yaml', '--indent', '4', '--o', str(Path(fixture_path, path_prefix + 'database.yaml')))
        sys.stdout.write(f'dumped {app_name} to {str(Path(fixture_path, path_prefix + "database.yaml"))}')
    else:
        filepaths = []
        for i, (key, value) in enumerate(data.items()):
            filepath = Path(fixture_path, path_prefix + 'database' + str(i) + '.yaml')
            filepaths.append(filepath)
            #to_dump = [app_name + '.' + str(key) + ', --pks  ' + ','.join(map(str, value)) for (key, value) in data.items()]
            management.call_command('dumpdata', app_name + '.' + str(key), '--pks', ','.join(map(str, value)),
                                    '--format', 'yaml', '--indent', '4', '--o', str(filepath))
        if Path(fixture_path, path_prefix + 'database.yaml').exists():
            raise FileExistsError(f'file {Path(fixture_path, path_prefix + "database.yaml")} already exists')
        with open(Path(fixture_path, path_prefix + 'database.yaml'), 'w') as f:
            for filepath in filepaths:
                with open(filepath, 'r') as infile:
                    content = infile.read()
                    f.write(content)
                filepath.unlink()
        sys.stdout.write(f'dumped {app_name} to {str(Path(fixture_path, path_prefix + "database.yaml"))}')
    
    

if __name__ == '__main__':
    import django
    django.setup()
    # get virtualenv path
    virtual_env_path = os.environ.get('VIRTUAL_ENV')
    if not virtual_env_path:
        error_message = (f'you are not in a virtual environment ;\n'
                         f'if a virtual env was created activate it by typing ;\n'
                         f'source <nameofyourvenv>/bin/activate \n'
                         f'if no virtual env was created, create one by typing :\n'
                         f'python3 -m venv <nameofyourvenv> to create one;')
        raise ValueError(error_message)

    # BASE-DIR is where your manage.py is,
    BASE_DIR = Path(__file__).resolve().parent.parent
    parent = Path(__file__).resolve().parent

    # PROJECT_ROOT is where settings.py is .
    PROJECT_ROOT = next(child for child in BASE_DIR.iterdir() if child.is_dir()
                        and any(file.is_file() and file.match ('settings.py') for file in child.iterdir()))
    if not settings.configured:
        DJANGO_SETTINGS_MODULE = Path(PROJECT_ROOT, 'settings')
    parent.parts[-1] == 'management'
    fixture_path = Path(parent, 'tests', 'tests_data')
    path_prefix = ''    
    data = {
        'Categorie': [6, 7],
        'Source': [17, 10],
        'Recette': [192, 10],
        'Ingredient': [63, 64, 65, 66, 67, 68, 69],
        'IngredientsDeLaRecette': [63, 64, 65, 66, 67, 68, 69, 1298, 1299, 1300, 1301, 1302],
    }

    dump_data(data=data)


