#!/usr/bin/env python
# coding:utf-8

"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""
import unittest

from pathlib import Path
from tests.tests_super_class import TestSuperClass
from unittest.mock import patch
from helpers import copy_repo_to_prod, MyConfigParser


#class TestMyConfigParser(TestSuperClass):

    #def setUp(self):
        #self.data_dirs = Path(Path(__file__).parent, 'tests_data', 'tests_conf_prod', 'tests_configparser')
        ##self.addTypeEqualityFunc(MyConfigParser, self.MyConfigParserTest.config_compare)

    #def test2_create(self):
        #config = MyConfigParser.create(Path(self.data_dirs, 'env1.cfg'))

        #self.assertIsInstance(config, MyConfigParser)
        #self.assertEqual(config.get('database', 'env_type'), 'dev')


class Test_get_test_path(TestSuperClass):

    def setUp(self):
        self.get_dirs(__file__)
        self.cfg_path = Path(self.project_root, 'env.cfg')
        self.config = MyConfigParser.create(self.cfg_path)
        self.config.__setattr__('getp', TestSuperClass.get_test_path((self.config, self.datadir)))

        #self.venvname = self.config.get('pnames', 'venvname')

    def test_getp_basedir(self):
        progs_dir = self.config.getp('paths', 'progs')

        self.assertEqual(progs_dir, Path(self.datadir, 'opt', self.project_name))

    def test_getp_other_path(self):
        logs = self.config.getp('paths', 'logs')

        self.assertEqual(logs, Path(self.datadir, 'var', 'log', self.project_name))

    # def test_getp_source(self):
        #project_root = self.config.getp('repo_paths', 'project_root')

        #self.assertEqual(project_root, Path(self.repo_path, self.project_name, 'Recettes'))

    # def test_getp_source(self):
        #base_dir = self.config.getp('repo_paths', 'base_dir')

        #self.assertEqual(base_dir, Path(self.repo_path, self.project_name))

    def test_getp_myvenv(self):
        myvenv = self.config.getp('paths', 'venv')

        self.assertEqual(myvenv, Path(self.datadir, 'opt', self.project_name, self.venvname))


# @unittest.skip
class Test_copy_repo(TestSuperClass):

    def setUp(self):
        self.get_dirs(__file__)
        self.cfg_path = Path(self.project_root, 'env.cfg')
        self.fakeserver = Path(self.datadir, 'tests_copy_repo', 'fake_server')
        self.clean_dir(self.fakeserver)
        self.IPaddress = '1.2.3.4'

        #self.config = create_env_cfg_prod()
        self.config = MyConfigParser()
        with open(self.cfg_path, 'r') as f:
            self.config.read_file(f)

        # create myvenv
        self.myvenv = Path(self.fakeserver, *Path(self.config.get('paths', 'venv')).parts[1:])
        self.myvenv.mkdir(parents=True, exist_ok=True)

        # modifies the config so that the getp method return a path relative to the test_data_directory
        self.config.__setattr__('getp', TestSuperClass.get_test_path((self.config, self.fakeserver)))

        self.chmod_patcher = patch('helpers.Path.chmod')
        self.chmod_patcher.start()
        self.sh_chown_patcher = patch('helpers.shutil.chown')
        self.sh_chown_patcher.start()
        self.venv_name = self.config.get('pnames', 'venvname')
        self.django_user = self.config.get('users', 'django_user')

    # @unittest.skip
    def test_copy_repo(self):
        """cannot tests rights. would require to lauch the copy_repo as root"""

        copy_repo_to_prod(self.config, self.repo_path)

        #progs
        progs = Path(self.fakeserver, 'opt', self.project_name)
        self.assertTrue(progs.exists())
        self.assertTrue(progs.is_dir())
        self.assertTrue(Path(progs, 'manage.py').exists())
        for string in ['tests', 'media', 'static', '.git', 'fixtures']:
            self.assertFalse(Path(*progs.parts, string).exists())
        self.assertTrue(Path(*progs.parts, self.project_root.stem).exists())
        self.assertTrue(Path(*progs.parts, self.project_root.stem).is_dir())
        self.assertTrue(Path(*progs.parts, self.project_root.stem, 'wsgi.py').exists())
        self.assertTrue(Path(*progs.parts, self.project_root.stem, 'wsgi.py').is_file())
        self.assertFalse(Path(*progs.parts, self.project_root.stem, 'settings.py').exists())

        #venv created by repoprod do not check content but check symlink
        self.assertTrue(Path(self.myvenv, 'env.cfg').is_symlink())

        #statics do not verify content because it is filled by collectstatic called later
        self.assertTrue(Path(self.fakeserver, 'var', 'cache', self.project_name).exists())
        self.assertTrue(Path(self.fakeserver, 'var', 'cache', self.project_name).is_dir())

        #logs
        self.assertTrue(Path(self.fakeserver, 'var', 'log', self.project_name).exists())
        self.assertTrue(Path(self.fakeserver, 'var', 'log', self.project_name).is_dir())

        #conf
        self.assertTrue(Path(self.fakeserver, 'etc', 'opt', self.project_name).exists())
        self.assertTrue(Path(self.fakeserver, 'etc', 'opt', self.project_name).is_dir())
        self.assertTrue(Path(self.fakeserver, 'etc', 'opt', self.project_name, 'settings.py').exists())

        #media
        self.assertTrue(Path(self.fakeserver, 'var', 'opt', self.project_name, 'media').exists())
        self.assertTrue(Path(self.fakeserver, 'var', 'opt', self.project_name, 'media').is_dir())
        if any(Path(self.repo_path).iterdir()):
            self.assertTrue(any(Path(self.fakeserver, 'var', 'opt', self.project_name, 'media').iterdir()))

        #nginx
        self.assertTrue(Path(self.fakeserver, 'etc', 'nginx', 'sites-enabled', self.IPaddress,).exists())
        self.assertTrue(Path(self.fakeserver, 'etc', 'nginx', 'sites-enabled', self.IPaddress,).is_symlink())
        self.assertTrue(Path(self.fakeserver, 'etc', 'nginx', 'sites-available', self.IPaddress).exists())
        self.assertTrue(Path(self.fakeserver, 'etc', 'nginx', 'sites-available', self.IPaddress).is_file())
        self.assertTrue(Path(self.fakeserver, 'etc', 'nginx', 'sites-enabled', 'default').exists())
        self.assertTrue(Path(self.fakeserver, 'etc', 'nginx', 'sites-enabled', 'default').is_file())

        #www
        self.assertTrue(Path(self.fakeserver, 'var', 'www', 'index.html').exists())
        self.assertTrue(Path(self.fakeserver, 'var', 'www', 'index.html').is_file())

    def tearDown(self):
        self.clean_dir(self.fakeserver)
        self.sh_chown_patcher.stop()
        self.chmod_patcher.stop()


if __name__ == '__main__':
    import unittest

    unittest.main()
