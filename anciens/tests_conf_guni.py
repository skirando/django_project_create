#!/usr/bin/env python
# coding:utf-8

"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""
import unittest
from unittest.mock import patch
from pathlib import Path

from tests.tests_super_class import TestSuperClass
import helpers as h
import conf_guni


"""Conf_guni does not modify the cfg file. So there is no need to worry about te env.cfg"""


class Test_global (TestSuperClass, unittest.TestCase):

    @ classmethod
    def setUpClass(cls):
        cls.get_dirs(cls, __file__)

    def test_global_command(self):
        self.fakeserver = Path(self.datadir)

        def mock_get_etc(parts):
            """ mailname should not exist """
            return Path(self.fakeserver, *parts)

        def mock_get_cfg_path():
            return Path(self.fakeserver, 'env.cfg')

        #no arguments
        namespace = {}

        created = [Path(self.datadir, 'systemd', 'system', self.project_name + '.service'),
                   ]
        #no need to erase file it is copied in main and then rewritten
        #for parts in created:
            #Path(*parts).unlink(missing_ok=True)

        with patch('conf_dma.get_etc', return_value=self.fakeserver.parts) as m2:
            with patch('conf_guni.h.check_root', return_value=True) as m3:
                with patch('conf_dma.get_settings_module_cfg_path_function', return_value=mock_get_cfg_path) as m3:
                        conf_guni.mmain(namespace, Path(self.datadir, 'fake_repo_do_not_delete'))

        for file in created:
            with self.subTest(file=file):
                self.assertTrue(file.exists() and file.is_file())
                filecmp.cmp(file, Path(self.datadir, 'shouldbe', file.relative_to(self.fakeserver)))


if __name__ == '__main__':
    import unittest

    unittest.main()
