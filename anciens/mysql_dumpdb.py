"""this command dumps the database content into a file which is created in the dumps directory"""


from pathlib import Path
import sys
import os
import subprocess
import datetime

ADMIN_EXCLUDE_TABLES = ['auth_group', 'auth_group_permissions', 'auth_permission', 'auth_user', 'auth_user_groups', 'auth_user_user_permissions',
                        'django_admin_log', 'django_content_type', 'django_session', 'django_migrations']


def mysql_dumpdb(user, pwd, dbname, dumps_path, exclude_admin=False):
    """ dump a database with or without the tables for admin
     """
    command = ['mysqldump', f'-u{user}', f'-p{pwd}', f'{dbname}', f'--extended-insert', f'--no-tablespaces']

    exclude_command = [f'--ignore-table={dbname}.{table_name}' for table_name in ADMIN_EXCLUDE_TABLES]
    dump_file = Path(dumps_path, datetime.date.today().__str__().replace('-', '_', 2) + '_dump.sql')
    if exclude_admin:
        total_command = command + exclude_command
    else:
        total_command = command
    try:
        with open(dump_file, 'w') as f:
            completedProcess = subprocess.run(total_command,
                                              stdout=f,
                                              check=True,
                                              encoding='utf-8'
                                              )
    except subprocess.CalledProcessError as err:
        print(f'dump of database failed')
        print(f'error : {err.stderr}')
        print(f'command ; {err.cmd}')
        print(f'arguments : {err.args}')
        exit()

    if not completedProcess.stdout:
        print('dump terminated')
    else:
        print(completedProcess.stdout)


if __name__ == '__main__':
    # get virtualenv path
    virtual_env_path = os.environ.get('VIRTUAL_ENV')
    if not virtual_env_path:
        error_message = (f'you are not in a virtual environment ;\n'
                         f'to activate virtual env type;\n'
                         f'source <nameofyourvenv>/bin/activate \n'
                         )
        raise ValueError(error_message)

    # BASE-DIR is where your manage.py is,
    BASE_DIR = Path(__file__).resolve().parent.parent

    # PROJECT_ROOT is where settings.py is .
    PROJECT_ROOT = next(child for child in BASE_DIR.iterdir() if child.is_dir() and
                        any(file.is_file() and file.match('settings.py') for file in child.iterdir()))

    pwd_file = Path(virtual_env_path, 'my_db_password.txt')
    try:
        with open(pwd_file, 'r') as f:
            pwd = f.read()
    except OSError:
        print(f'password file{pwd_file} does not exist')




