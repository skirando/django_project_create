#!/usr/bin/env python
# coding:utf-8

"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""
import unittest
import load_database as load_db
from pathlib import Path
import shutil as sh
from tests.tests_super_class import TestSuperClass
from unittest.mock import patch
import importlib
import sys


class TestFunctions(TestSuperClass):

    def setUp(self):
        self.get_dirs(__file__)
        #module_name = 'settings'
        #spec = importlib.util.spec_from_file_location(module_name, Path(self.project_root, 'settings.py'))
        #self.settings = importlib.util.module_from_spec(spec)
        #sys.modules[module_name] = self.settings
        #spec.loader.exec_module(self.settings)
        self.myapp = Path(self.datadir, 'fixtures')

    def test1_gest_most_recent_fixture_with_auth(self):

        fixture, auth = load_db.get_most_recent(self.myapp, withauth=True)

        self.assertEqual(fixture,
                         Path(self.myapp, '2022_01_04database.yaml'))
        self.assertEqual(auth,
                         Path(self.myapp, '2022_01_04auth.yaml'))

    #def test2_get_appPathsNames(self):

        #with patch.object(self.settings, 'BASE_PROGS', new=Path(self.repo_path)):
            #app_paths_names, authapp = load_db.get_appPathsNames(self.settings, self.repo_path)

        #self.assertEqual(app_paths_names,
                         #[(self.myapp, 'myapp')])
        #self.assertEqual(authapp, 'myapp')

    def tearDown(self):
        pass


if __name__ == '__main__':
    import unittest

    unittest.main()
