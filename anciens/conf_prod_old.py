""" this command configures the environment of the production site
"""
from pathlib import Path
import os
import inspect
import shutil
import configparser
import compileall

"""this is the dictionnary which determines where files are stored
each list is followed by the project name"""

PRODCONFDICT = {
    "Program_files": ['opt',  # PROJECT_NAME
                      ],
    "Virtual_env": ['opt',  # PROJECT_NAME
                    ],
    "Data_files": ['var', 'opt',  # PROJECT_NAME
                   ],
    "Static_files": ['var', 'cache',  # PROJECT_NAME
                     ],
    "Log_files": ['var', 'log',  # PROJECT_NAME
                  ],
    "Configuration_files": ['etc', 'opt',  # PROJECT_NAME
                            ],
    "BASE_PROD": '/',
    "VENV_NAME": 'myvenv',
}


class ProdConfig:
    """defines how files are distributed on production server"""

    def __init__(self, dic, project_name):

        self.program_files_path = Path(dic['BASE_PROD'], *dic["Program_files"], project_name)
        self.virtual_env_path = Path(dic['BASE_PROD'], *dic["Virtual_env"], project_name, dic['VENV_NAME'])
        self.data_files_path = Path(dic['BASE_PROD'], *dic["Data_files"], project_name)
        self.static_files_path = Path(dic['BASE_PROD'], *dic["Static_files"], project_name)
        self.log_files_path = Path(dic['BASE_PROD'], *dic["Log_files"], project_name)
        self.configuration_files_path = Path(dic['BASE_PROD'], *dic["Configuration_files"], project_name)
        self.venv_name = dic['VENV_NAME']
        self.base_prod = dic['BASE_PROD']
        self.project_name = project_name


class MyConfigParser (configparser.ConfigParser):
    """ this class is used only in django_management modules
    in django_settings we use standard configparser class
    so as not to import it"""

    @staticmethod
    def create(cfg_path):
        config = MyConfigParser()
        try:
            with open(cfg_path, 'r') as f:
                config.read_file(f)
        except FileNotFoundError:
            raise ValueError(f'could not find cfg file at {cfg_path}; repo clone is the command to create env.cfg')
        return config

    def getp(self, section, option, **kwargs):
        value = super().get(section, option, **kwargs)
        if section == 'paths' or section == 'repo_paths':
            if not option == 'venvname':
                return Path(value)
        else:
            return value

    def setp(self, section, option, value, **kwargs):
        if section == 'paths' or section == 'repo_paths':
            svalue = str(value)
            super().set(section, option, svalue, **kwargs)

    def add_paths_section_for_prod(self, prodcfg):
        """ create the section for the CongiParser object for the production"""

        self.add_section('paths')
        self.add_section('pnames')

        self.setp('paths', 'progs', prodcfg.program_files_path)
        self.setp('paths', 'venv', prodcfg.virtual_env_path)
        self.setp('paths', 'data', prodcfg.data_files_path)
        self.setp('paths', 'statics', prodcfg.static_files_path)
        self.setp('paths', 'logs', prodcfg.log_files_path)
        self.setp('paths', 'conf', prodcfg.configuration_files_path)
        self.set('pnames', 'venvname', prodcfg.venv_name)
        self.setp('pnames', 'baseprod', prodcfg.base_prod)
        self.set('pnames', 'project_name', prodcfg.project_name)
        self.setp('paths', 'media', Path(prodcfg.data_files_path, 'media'))
        self.setp('paths', 'fixtures', Path(prodcfg.data_files_path, 'fixtures'))

    def add_paths_section_for_dev(self):
        """transforms the django settings parameters into file paths so as to have a single settings"""
        base_dir = Path(self.get('repo_paths', 'base_dir'))
        project_root = Path(self.get('repo_paths', 'project_root'))
        venvname = self.get('repo_paths', 'venvname')

        self.add_section('paths')
        self.add_section('pnames')

        self.setp('paths', 'progs', base_dir)
        self.setp('paths', 'venv', Path(base_dir, venvname))
        self.setp('paths', 'media', Path(base_dir, 'media'))
        self.setp('paths', 'logs', Path(project_root, 'logs'))
        self.setp('paths', 'conf', Path(project_root))
        self.setp('paths', 'data', Path(project_root, 'media'))
        self.set('pnames', 'venvname', venvname)
        self.setp('paths', 'baseprod', base_dir)
        self.set('pnames', 'project_name', base_dir.name)

    def add_section(self, section):
        if self.has_section(section):
            self.remove_section(section)
        super().add_section(section)

    def compareValue(self, section, option, other):
        try:
            other_value = other.get(section, option)
        except (configparser.NoSectionError, configparser.NoOptionError):
            return False
        return self.get(section, option) == other_value

    def compareSection(self, section, other):
        try:
            other_sec = other.options(section)
        except (configparser.NoSectionError, configparser.NoOptionError):
            return False
        return all(self.compareValue(section, option, other) for option in self.options(section))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return all(self.compareSection(section, other) for section in self.sections())
        else:
            return False


class Prod_dirs_files():
    """ cette classe est vide; c'est la classe mère des sous-classes suivante - utile dans la boucle for"""
    pass


class Program_files(Prod_dirs_files):

    @staticmethod
    def files_dirs(prod_conf):
        """ copy sous la racine  manage.py
    copy a set of directory from source to destination à l'exclusion de media, virtalenv, static ,.git, fixtures
    Exclure Project_root et ne transférer que urls, asgy,wsgy,init
    erreur de compilation settings et modèle
    """
        virtual_env = prod_conf.get('pnames', 'venvname')
        project_root = prod_conf.getp('repo_paths', 'project_root')
        base_dir = prod_conf.getp('repo_paths', 'base_dir')
        dir_ignore = ('media', virtual_env, 'static', '.git', 'fixtures', 'ancien*', 'test*', project_root.stem)
        fn_ignore = shutil.ignore_patterns(*dir_ignore)
        des_progs = prod_conf.getp('paths', 'progs')

        # copy all program-files of apps
        shutil.copytree(base_dir, des_progs, ignore=fn_ignore, dirs_exist_ok=True)

        #des_path=Path(des_path, project_root.stem)
        # copy files which are under project root.
        file_names = ('asgi.py', 'wsgi.py', 'urls.py', '__init__.py')
        progs_conf = prod_conf.getp('paths', 'conf')
        progs_conf.mkdir(parents=False, exist_ok=True)
        for file in file_names:
            shutil.copy(Path(project_root, file),
                        progs_conf)
        import re
        rexcl = re.compile(prod_conf.get('pnames', 'venvname'))
        compileall.compile_dir(des_progs, rx=rexcl
                               )


class Virtual_env (Prod_dirs_files):

    @ staticmethod
    def files_dirs(prod_conf):
        """ create a symlink to env.cfg"""
        pass


class Data_files (Prod_dirs_files):

    @ staticmethod
    def files_dirs(prod_conf):
        """ copier répertoire media vers la destination"""
        base_dir = Path(prod_conf.getp('repo_paths', 'base_dir'), 'media')
        dest = Path(prod_conf.getp('paths', 'media'))

        shutil.copytree(base_dir, dest, dirs_exist_ok=True)
        """ne fonctionne pas avec utilisateur """
        import pwd
        print(f'qui est l\'utilisateur? {pwd.getpwuid(os.getuid())}')
        #shutil.chown(dest, prod_conf.get('host','django_user'))


class Static_files(Prod_dirs_files):

    @ staticmethod
    def files_dirs(prod_conf):
        """ appeler le collectstatic sur l'ensemble des applications à faire après le lancement de django"""
        pass


class Log_files(Prod_dirs_files):

    @ staticmethod
    def files_dirs(prod_conf):
        # sudo mkdir -p $DJANGO_LOG
    # sudo chown $DJANGO_USER $DJANGO_LOG
        pass


class Configuration(Prod_dirs_files):

    @ staticmethod
    def files_dirs(prod_conf):
        """copy of settings.py, password.txt et env.cfg"""
        files_cfg = ['env.cfg', 'pwd.txt', 'secret_key', 'settings.py']
        p = prod_conf.getp('paths', 'conf')
        p.mkdir(mode=750, exist_ok=True, parents=True)
        for f in files_cfg:
            shutil.copy(Path(prod_conf.getp('repo_paths', 'project_root'), f),
                        Path(p, f))
        """ copy of directory requirements"""
        preq = Path(prod_conf.getp('paths', 'conf'), 'requirements')
        preq.mkdir(mode=750, exist_ok=True, parents=True)
        srcrec = Path(prod_conf.getp('repo_paths', 'project_root'), 'requirements')
        for f in srcrec.iterdir():
            shutil.copy(f,
                        Path(preq, f.name))


def get_key_classes(module):
    classes = inspect.getmembers(module, inspect.isclass)
    subclasses_names = [p.__name__ for p in Prod_dirs_files.__subclasses__()]
    childclasses = [c[1] for c in classes if c[1].__name__ in subclasses_names]
    return childclasses


def copy_repo_to_prod(prod_conf):
    """ copy all files from base repo to prod
        base_repo = Path(BASE_DIR)
        base_prod = /"""
    module = __import__(Path(__file__).stem)
    keyclasses = get_key_classes(module)

    # """create data and log directory"""
    for aclass in keyclasses:
        aclass.files_dirs(prod_conf)

    # environs = {
        # 'HOMEUSER': str(conf_prod.BASE_PROD.joinpath(*conf_prod.prod_conf_dic['Data_files'])),
        # 'DJANGO_USER': user,
        # 'VENV_PATH': str(conf_prod.BASE_PROD.joinpath(*conf_prod.prod_conf_dic['Virtual_env']).joinpath(conf_prod.venv_name)),
        # 'REQUIREMENTS_PATH': str(conf_prod.BASE_PROD.joinpath(*conf_prod.prod_conf_dic['Program_files']).joinpath('requirements', 'prod.txt')),
        # 'VENV': source_cfg.get('paths', 'virtual_env'),
        # 'DJANGO_PROGRAMS': str(conf_prod.BASE_PROD.joinpath(*conf_prod.prod_conf_dic['Program_files'])),
        # 'DJANGO_DATA': str(conf_prod.BASE_PROD.joinpath(*conf_prod.prod_conf_dic['Data_files'])),
        # 'DJANGO_LOG': str(conf_prod.BASE_PROD.joinpath(*conf_prod.prod_conf_dic['Log'])),
        # 'DJANGO_CONF': str(conf_prod.BASE_PROD.joinpath(*conf_prod.prod_conf_dic['Configuration'])),
    # }
    # os.environ.update(environs)
    script = Path(Path(__file__).parent, 'script_create_prod_env.bash')
    call_bash(script, 'création environnement de production', 'erreur dans la création de l\'environnement de production')

    # sudo mkdir -p $DJANGO_DATA chown $DJANGO_USER $DJANGO_DATA
    # sudo chown $DJANGO_USER $DJANGO_DATA
    # sudo mkdir -p $DJANGO_LOG
    # sudo chown $DJANGO_USER $DJANGO_LOG
    # sudo mkdir $DJANGO_CONF
    # su $DJANGO_USER
    # source $VENV_PATH/bin/activate
    # export PYTHONPATH=$DJANGO_CONF:$DJANGO_PROGRAMS
    # export DJANGO_SETTINGS_MODULE=settings
    # python $DJANGO_PROGRAMS/manage.py migrate
    # python $DJANGO_PROGRAMS/manage.py runserver 0.0.0.0:8000
if __name__ == '__main__':
    prod_conf = MyConfigParser()
    cfg_path = Path(Path(__file__).parent.parent, 'recettes_for_git', 'Recettes', 'env.cfg')
    with open(cfg_path, 'r') as f:
        prod_conf.read_file(f)
    mydict = PRODCONFDICT
    mydict['BASE_PROD'] = Path(Path(__file__).parent, 'tests', 'tests_data', 'tests_conf_prod')
    prod_config = ProdConfig(mydict, 'family-recipes')
    prod_conf.add_paths_section_for_prod(prod_config)

    with open(cfg_path, 'w') as f:
        prod_conf.write(f)
    copy_repo_to_prod(prod_conf)
    """ suppression du répertoire de travail"""
    shutil.rmtree(prod_conf.getp('rep_paths', 'base_dir'), ignore_errors=True)
