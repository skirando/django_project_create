#!/usr/bin/env python
# coding:utf-8
"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""


from pathlib import Path
import sys
import os


import create_django_env.helpers as h
from configparser import DuplicateSectionError


def subparser_add_args(subparser):
    """ creates the parser"""

    subparser.add_argument('dbname',
                           help='name of the mysql database')


def parse_args(subparser, arguments):
    args = subparser.parse_args(arguments)
    return args


def mmain(namespace, cwd):
    args = vars(namespace)
    dbname = args['dbname']
    """we suppose to be in the directory of the cloned repository or the directory of the created project"""
    base_dir = h.get_base_dir_repo(cwd)
    if not base_dir:
        return
    project_root = h.get_project_root_repo(base_dir)
    if not project_root:
        return
    if not h.check_root():
        return
    #get config
    cfg_path = h.get_cfg_path_repo(project_root)
    config = h.PathConfigParser.create_from_file(cfg_path)
    env_type = config.get('configuration', 'env_type')
    dbuser = config.get('users', 'user_admin')
    venvpath = config.get('paths', 'venv')
    pwd_file = Path(project_root, 'pwd.txt')

    dbpwd = create_mysql_user_database(env_type, dbname, dbuser, venvpath)
    if not dbpwd:
        return

    pwd_file = Path(project_root, 'pwd.txt')
    with open(pwd_file, 'w') as f:
        f.write(dbpwd)
    # write config
    try:
        config.add_section('database')
    except DuplicateSectionError:
        pass
    config['database'] = {'dbname': dbname, 'dbtype': 'mysql'}
    with open(cfg_path, 'w') as file:
        config.write(file)

    print('mysql database created, next step is install_env')
    h.prepare_for_virtual_env(config, 'install_env')
    print(f'''command for install_env is :
    python3 {Path(__file__).parent} install_env -h''')


def create_mysql_user_database(env_type, dbname, dbuser, venvpath):
    try:
        import MySQLdb as mysql
    except ModuleNotFoundError:
        message = f'''could not import MySQLdb; install mysqlclient in the virtual environment with pip install mysqlclient
        other hint you must run this command as sudo and from the virtual environment so type in:
        sudo -E {venvpath}/bin/python3 {Path(__file__).parent} mysql_init <database name>'''
        print(message)
        return

    if env_type == 'prod':
        auth = 'IDENTIFIED WITH caching_sha2_password BY '
    else:
        auth = 'IDENTIFIED WITH mysql_native_password BY '
    privileges = f'{dbname}.*' if env_type == 'prod' else '*.*'

    #open mysql connection
    try:
        connection = mysql.connect('localhost', 'root')
    except Exception as e:
        print(f'cannot open mysql connection')
        print(e)
        print(f'in case of authentication error make sure socket_auth plugin is installed')
        print(f'to check installed plugins, open mysql with sudo mysql then type show plugins')
        print(f'to install new plugin check into mysql documentation')
        print('root should be authentified with socket_auth plugin')
        print(f'check by typing in : SELECT User, Host, Plugin FROM mysql.user')
        print(f'if necessary change root authentication plugin by typing in ')
        print(f"ALTER USER 'root'@'localhost' IDENTIFIED WITH auth_socket;")
        return
    #find out if user already exists
    cursor = connection.cursor()
    sql = f'select user, host from mysql.user;'
    cursor.execute(sql)
    tup = next((tup for tup in cursor
                if tup[0] == dbuser
                and tup[1] == 'localhost'
                ), None)
    if tup:
        message = f' mysql user : {dbuser}@localhost already exists enter its password:\n'
        # get database password
        dbpwd = h.get_pwd(message)
        existing_user = True
    else:
        message = f'enter password for new mysql user {dbuser}:\n'
        dbpwd = h.get_pwd(message)
        #touchy mysql requires the password to be in quotes
        sql = 'create user ' + dbuser + '@localhost ' + auth + " '" + dbpwd + "' ;"
        cursor.execute(sql)
        existing_user = False
        print(f'created user {dbuser}@localhost {auth} entered password')
    try:
        sql = f'CREATE DATABASE {dbname} ;'
        cursor.execute(sql)
        message = f'database : {dbname} created'
        db_exist = False
    except mysql.MySQLError as e:
        message = f'could not create database {dbname} ; suppose it already exists'
        db_exist = True
    print(message)
    sql = f'use {dbname}'
    cursor.execute(sql)
    sql = f'grant all privileges on {privileges} to {dbuser}' + '@localhost ;'
    cursor.execute(sql)
    sql = 'flush privileges'
    cursor.execute(sql)
    print(f'granted privileges :{privileges} to {dbuser}@localhost')

    try:
        db_connection = mysql.connect('localhost', dbuser, dbpwd, dbname)
    except:
        print(f'user {dbuser}@localhost cannot access database {dbname} using {dbpwd} as password')
        if existing_user:
            print(f'did you enter the correct mysql user password for user {dbuser}')
        if db_exist:
            print(f'also check sql privileges on existing {dbname} database ')
        print(f'after correcting the password, run again the mysql_init command')
        return

    return dbpwd


if __name__ == '__main__':
    import os
    from pathlib import Path
    import argparse
    namespace = argparse.Namespace(dbname=sys.argv[1:])
    mmain(namespace, Path.cwd())














