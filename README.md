#django create environment

This package is an implementation in python scripts of the tutorial https://djangodeployment.readthedocs.io/en/latest/01-getting-started.html
All thanks go to the author of this brilliant tutorial. It does not not handle web sites which include several databases. 
it allows to create a duplicable or clonable django project. The project can later on be duplicated in a production or development configuration.
This package uses only one settings file for all environment types (dev, test or prod). The settings is coupled with an env.cfg file containing
parameters to be used by the settings. This env.cfg file is created by the create django environment commands. In test and dev configuration the env.cfg file is located 
under the project_root. In production environment, a symlink towards the env.cfg file is located into the virtualenv directory.
At the end of each command of the package, prints give step by step description of the command lines  required to test the results of the command.

#Usage1 :
to create a django project from scratch with a settings.py that will allow its easy duplication with the create django environment package commands
1. create a directory for the django_project;
2. if a virtual env already exists for your global project and you want to use it for the djangoproject, activate it with source myvenv/bin/activate
  otherwise the package will create a virtualenv for the djangoproject and activate it
3. call the package startproject command : python absolute_path_to_create_django_env pstartproject -h to see the required parameters

#Usage2 : to duplicate an existing project in a repo using the django create environment commands:
Six commands are available and must be called in this order:
1. repo_clone to clone the existing django project repository.
2. mysql_init to create in the new environment an empty mysql database; 
3. install_env to create the env.cfg files specific to the type of environment; this file is read by django settings.
    it can load the database from dump or yaml files existing in the project repository .
    for prod environment the files are copied in the appropriate directorys so as to create a secure environment as recommended by the tutorial.
    This distribution of the files can be modified in the PRODCONFDICT in module helpers.
    finally it configures Nginx as suggested in the tutorial.
5. conf_guni to configure gunicorn for production environment. 
4. conf_dma to configure DragonFly Mail Agent for production environment. The command ensures cohrence between the dma parameters and 
   the Django settings.
6. secure_conf_prod (to be developped) to secure the database, install the backup software and the crons as suggested by the tutorial


If not created with the startproject command of this package, the existing project must conform to the following criteria:
1. it is contained in a distant repository which can be cloned; if media files are in the repository, unused media files shoud be manually removed before cloning
2. the distant repository contains a settings.py which includes the code required to read the env.cfg file. 
   See the template for the settings.py file
3. 4 requirements files called dev.txt, prod.txt , test.txt and database_requirements are in the repository and contain the python packages required for each type of environment.
see requirements templates in project template configuration_directory
4. the django project uses only one database. a command of the package allows to create an empty mysql database and update the env.cfg file accordingly.

# Usage 3 : to update code and static files of production configuration from a new version of your repository.
this usage requires that requirements file are manually updated each time a new version of a package  is installed in the virtual environment of the project
1. as a safe measure backup the database content
2. git pull the new code version from your repository   into the production server 
3. install_env command with load = DEFAULT as parameter so as not to load a database and keep the existing database and with new_machine = update
 



#installation 
the clone_deploy_django package is not yet set up for distribution so clone the repo.

#dependencies : see documentation mysql_and_other_dependencies 

#backup : this package provide an "almost automatic" method to recreate a configuration after a crash. However the database and the media 
# files which are uploaded in the django application must be manually restored.
# for a production environment, you must make regular backups of the database and the media files. 
# if using mysql database, you can use mysqldump with --no-tablespaces option as the mysql_init command does not grant Process privilege to admin of the data base
