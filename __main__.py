#!/usr/bin/env python
# coding:utf-8
"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""


if __name__ == '__main__':

    import sys
    from pathlib import Path
    from importlib import import_module

    parent = Path(__file__).parent.parent
    cwd = Path.cwd()
    sys.path.append(str(parent))

    from create_django_env.package_parser import PackageParser

    commands_list = ['repo_clone', 'mysql_init', 'install_env', 'conf_dma', 'conf_guni', 'pstartproject']

    parser = PackageParser(commands_list, help)

    try:
        command = sys.argv[1]
        if command in commands_list:
            module = import_module(f'create_django_env.{command}')
            subparser = parser.get_module_parser(command)
            namespace = module.parse_args(subparser, sys.argv[2:])
            module.mmain(namespace, cwd)
        else:
            parser.parser.print_help()

    except IndexError:
        parser.parser.print_help()







