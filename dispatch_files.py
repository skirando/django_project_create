#!/usr/bin/env python
# coding:utf-8
"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""
from pathlib import Path
import inspect
import shutil
import compileall
import stat

from create_django_env import helpers as h


SECTIONS = ['repo_paths', 'configuration', 'paths', 'pnames', 'users', 'host', 'database']
OPTIONS = [('repo_paths', 'base_dir'),
           ('repo_paths', 'top_dir'),
           #('repo_paths', 'extra_name'),
           ('configuration', 'env_type'),
           ('users', 'email_admin'),
           ('users', 'user_admin'),
           ('users', 'django_user'),
           ('pnames', 'project_name'),
           ('pnames', 'venvname'),
           ('pnames', 'confname'),
           #('host', 'host_ip'),
           ('host', 'domain'),
           ('host', 'dns'),
           ('database', 'dbtype')
           ]

class ConfigParserMixin(h.PathConfigParser):
    VENVNAME = h.DEFAULT_VENV_NAME

    def __init__(self, **kwargs):
        """create the configparser from scratch;  needs at list base_dir and project_name and topdir in kwargs
        al missing arguments are to be handled here"""
        required_kwargs = ['project_name', 'base_dir']
        missing_kwargs = [k for k in required_kwargs if k not in kwargs]
        if len(missing_kwargs) != 0:
            raise ValueError(f' {missing_kwargs} are required to create a config')
        default_values = {'venvname': self.VENVNAME,
                          'confname': kwargs['project_name'],
                          #'extra_name': ''
                          }
        #create sections and options for each value in kwargs and default values
        super().__init__(sections_list=SECTIONS, options_list=OPTIONS, default_values=default_values, **kwargs)

        self.base_dir = self.getp('repo_paths', 'base_dir')
        self.top_dir = self.getp('repo_paths', 'top_dir')
        self.project_name = self.get('pnames', 'project_name')
        if self.top_dir != self.base_dir:
            #configuration top_dir need to add base_dir_name  to the path
            self.base_dir_name = self.getp('repo_paths', 'base_dir').parts[-1]
        else:
            self.base_dir_name = ''
        self.confname = self.get('pnames', 'confname')
        self.venvname = self.get('pnames', 'venvname')


class ProdConfigParser (ConfigParserMixin):
    """defines how files are distributed on production server"""

    """this is the dictionnary which determines where files are stored
    __init__ will add the information as described in the comment"""
    BASE_PROD = '/'

    PRODCONFDICT = {
        "Program_files": ['opt',  # PROJECT_NAME
                          ],
        "Virtual_env": ['opt',  # PROJECT_NAME
                        ],
        "Media_files": ['var', 'opt',  # PROJECT_NAME #'media'
                        ],
        "Static_files": ['var', 'cache',  # PROJECT_NAME
                         ],
        "Log_files": ['var', 'log',  # PROJECT_NAME
                      ],
        "Configuration_files": ['etc', 'opt',  # PROJECT_NAME
                                ],
        "Nginx_files": ['etc', 'nginx'],
        "www_default": ['var', 'www'], }

    def create_paths_section(self):
        """ create the section for the files config
        for media, statics, logs do not add the base_dir name as it is not necessary to have 2 levels"""

        self.setp('paths', 'progs', Path(self.BASE_PROD,
                                         *self.PRODCONFDICT["Program_files"],
                                         self.project_name,
                                         self.base_dir_name))
        self.setp('paths', 'venv', Path(self.BASE_PROD,
                                        *self.PRODCONFDICT["Virtual_env"],
                                        self.project_name,
                                        self.VENVNAME))
        self.setp('paths', 'statics', Path(self.BASE_PROD,
                                           *self.PRODCONFDICT["Static_files"],
                                           self.project_name,
                                           #self.base_dir_name
                                           ))
        self.setp('paths', 'logs', Path(self.BASE_PROD,
                                        *self.PRODCONFDICT["Log_files"],
                                        self.project_name,
                                        #self.base_dir_name
                                        ))
        self.setp('paths', 'conf', Path(self.BASE_PROD,
                                        *self.PRODCONFDICT["Configuration_files"],
                                        self.project_name,
                                        self.base_dir_name
                                        ))
        self.setp('paths', 'media', Path(self.BASE_PROD,
                                         *self.PRODCONFDICT["Media_files"],
                                         self.project_name,
                                         #self.base_dir_name,
                                         'media'))
        self.setp('paths', 'nginx', Path(self.BASE_PROD,
                                         *self.PRODCONFDICT["Nginx_files"]))
        self.setp('paths', 'www', Path(self.BASE_PROD,
                                       *self.PRODCONFDICT['www_default']))

        #extra = self.getp('repo_paths', 'extra_name')
        #if extra != '':
            #self.setp('paths', 'extra_dir', Path(self.BASE_PROD,
                                                 #*self.PRODCONFDICT["Program_files"],
                                                 #extra))


class DevConfigParser (ConfigParserMixin):
    """defines how files are distributed in dev configuration"""

    def create_paths_section(self):
        """transforms the config parameters into file paths"""
        project_root = Path(self.base_dir, self.confname)

        self.setp('paths', 'progs', self.base_dir)

        self.setp('paths', 'venv', Path(self.top_dir, self.venvname))
        self.setp('paths', 'media', Path(self.base_dir, 'media'))
        self.setp('paths', 'logs', Path(project_root, 'logs'))
        self.setp('paths', 'conf', project_root)
        self.setp('paths', 'media', Path(self.base_dir, 'media'))


class Prod_dirs_files():
    """ this class is empty and is the parent class of all classes to create directories.
    rights is the list of rights to be given the the django_user
    read, right, execute, read group, execute group none for others"""
    rights = stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR | stat.S_IRGRP | stat.S_IXGRP
    other_rights = stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR | stat.S_IRGRP | stat.S_IXGRP | stat.S_IROTH | stat.S_IXOTH
    log_rights = stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP | stat.S_IWGRP | stat.S_IROTH | stat.S_IWOTH


class Program_files(Prod_dirs_files):

    dir_ignore = ['media', 'static', '.git', 'fixtures', 'ancien*', 'test*']

    @ classmethod
    def files_dirs(cls, config, install):
        """ copy all program files that is files from top-dir except media, virtalenv, static ,.git, fixtures
        and python configuration files (except settings.py) from  project_root
        # no longer used in prod configuration copies also extra directory"""
        base_dir = config.getp('repo_paths', 'base_dir')
        #try:
        top_dir = config.getp('repo_paths', 'top_dir')
        #except configparser.NoOptionError:
            #top_dir = base_dir

        virtual_env = config.get('pnames', 'venvname')
        confname = config.get('pnames', 'confname')
        dir_ignore = cls.dir_ignore + [virtual_env, confname]
        fn_ignore = shutil.ignore_patterns(*dir_ignore)
        dest_progs = config.getp('paths', 'progs')

        # copy all program-files from top_dir
        shutil.copytree(base_dir, dest_progs, ignore=fn_ignore, dirs_exist_ok=True)
        new_ignore = dir_ignore + [base_dir.parts[-1]]
        new_fn = shutil.ignore_patterns(*new_ignore)
        shutil.copytree(top_dir, dest_progs.parent, ignore=new_fn, dirs_exist_ok=True)

        #copy extra directory if it exists
        #extra_name = config.getp('repo_paths', 'extra_name')
        #if extra_name != '':
            #extra_dest = config.getp('paths', 'extra_dir')
            #extra_source = Path(top_dir.parent, extra_name)
            #shutil.copytree(extra_source, extra_dest, ignore=fn_ignore, dirs_exist_ok=True)

        # copy py files from project configuration except settings.py
        conf_name = config.getp('pnames', 'confname')
        dest_conf = Path(dest_progs, conf_name)

        dest_conf.mkdir(exist_ok=True, parents=True)
        for fd in Path(base_dir, confname).iterdir():
            if fd.is_file() and fd.stem != "settings" and fd.suffix == '.py':
                shutil.copyfile(fd, Path(dest_conf, fd.name))

        import re
        rexcl = re.compile(config.get('pnames', 'venvname'))
        compileall.compile_dir(dest_progs, rx=rexcl,
                               quiet=1)
        shutil.chown(dest_progs, group=config.get('users', 'django_user'))
        dest_progs.chmod(cls.rights)


class Configuration(Prod_dirs_files):

    @ classmethod
    def files_dirs(cls, config, install):
        """copy of configuration files that is settings.py + all files which are not .py; do not copy env.cfg for updates
        need to copy requirements so as to verify that requirements are not changed from last installation
        for an update, do not copy env.cfg
        """
        base_dir = config.getp('repo_paths', 'base_dir')
        project_root = Path(base_dir, config.get('pnames', 'confname'))
        files_cfg = [f for f in project_root.iterdir() if f.is_file() and not f.name == 'env.cfg'
                     and (not f.suffix == '.py' or f.stem == 'settings' )]
        if install == 'new':
            files_cfg.append(Path(project_root, 'env.cfg'))
        dest = config.getp('paths', 'conf')
        dest.mkdir(exist_ok=True, parents=True)
        # change owners of the directory and permissions
        django_user = config.get('users', 'django_user')
        shutil.chown(dest, user=None, group=django_user)
        dest.chmod(cls.rights)
        # dest.chmod(0o000)
        for f in files_cfg:
            pf = Path(dest, f.relative_to(project_root))
            shutil.copy(f, pf)
            pf.chmod(cls.rights)
            # pf.chmod(0o750)
            shutil.chown(pf, user=None, group=django_user)
        #copy requirements
        #destreq = Path(dest, 'installed_requirements')
        #shutil.copytree(Path(project_root, 'installed_requirements'), destreq, dirs_exist_ok=True)


class Virtual_env (Prod_dirs_files):

    @ classmethod
    def files_dirs(cls, config, install):
        """ create a symlink to env.cfg"""
        if install == 'new':
            venv_path = config.getp('paths', 'venv')
            conf_dir = config.getp('paths', 'conf')
            if not Path(venv_path, 'env.cfg').is_symlink():
                Path(venv_path, 'env.cfg').symlink_to(Path(conf_dir, 'env.cfg'))


class Media_files (Prod_dirs_files):

    @ classmethod
    def files_dirs(cls, config, install):
        """ copy also media directory;
        change rights because Django writes to this directory when the user uploads
        warning if a file already exists in dest directory it will be replaced
        no risk to erase an existing file because django create a new version of the file at each modification of a media file
        """
        base_dir = config.getp('repo_paths', 'base_dir')
        django_user = config.get('users', 'django_user')

        if install == 'new':
            source = Path(base_dir, 'media')
            dest = Path(config.getp('paths', 'media'))

        else:
            source = Path(base_dir, 'media', 'progs_media')
            dest = Path(config.getp('paths', 'media'), 'progs_media')
        shutil.copytree(source, dest, dirs_exist_ok=True)
        shutil.chown(dest, user=django_user)
        shutil.chown(dest.parent, user=django_user)
        for f_or_dir in dest.iterdir():
            if f_or_dir.is_dir():
                shutil.chown(f_or_dir, user=django_user)


class Log_files(Prod_dirs_files):
    """create log dirs and files for Django,and dma"""

    @ classmethod
    def files_dirs(cls, config, install):
        if install == 'new':
            dest = config.getp('paths', 'logs')
            dest.mkdir(exist_ok=True, parents=True)
            shutil.chown(dest, config.get('users', 'django_user'))
            #create the log file
            project_name = config.get('pnames', 'project_name')
            log_path = Path(dest, project_name + '.log')
            log_path.touch()
            django_user = config.get('users', 'django_user')
            shutil.chown(log_path, user=django_user)
            log_path.chmod(cls.rights)
            #create logs for mail
            varlog = dest.parent
            mailog = Path(varlog, 'mail.log')
            mailerr = Path(varlog, 'mail.err')
            mailog.touch()
            mailerr.touch()
            mailog.chmod(cls.log_rights)
            mailerr.chmod(cls.log_rights)


class Static_files(Prod_dirs_files):

    @ classmethod
    def files_dirs(cls, config, install):
        """  create the directories and change owners and permissions so that collectstatic copies the files
        create also the cache directory
        to be done for updates also"""
        djangouser = config.get('users', 'django_user')
        static_dir = config.getp('paths', 'statics')
        #because collectstatic is ran as root in repo_clone change the rights of
        static_parent = static_dir.parent
        static_parent.mkdir(exist_ok=True, parents=True)
        #chown(static_parent, djangouser)
        static_parent.chmod(cls.other_rights)

        static_dir.mkdir(exist_ok=True, parents=True)
        shutil.chown(static_dir, djangouser)
        static_dir.chmod(cls.other_rights)
        cache_dir = Path(static_dir, 'cache')
        cache_dir.mkdir(exist_ok=True, parents=True)
        shutil.chown(cache_dir, djangouser)


class Nginx(Prod_dirs_files):

    @ classmethod
    def files_dirs(cls, config, install):
        """ create configuration files for nginx"""
        if install == 'new':
            # create sites enabled file
            conf_path = config.getp('paths', 'nginx')
            #dns = config.get('host', 'dns')
            #if dns == True:
                #domain = config.get('host', 'domain')
            #else:
                #domain = config.get('host', 'host_ip')
            domain = config.get('host', 'domain')
            static_prod = Path(config.get('paths', 'statics'), 'static')
            media_prod = config.get('paths', 'media')
            origin = Path(Path(__file__).parent, 'prod_files', 'Nginx_files')
            sites_enabled = Path(conf_path, 'sites-enabled')
            sites_enabled.mkdir(exist_ok=True, parents=True)
            sites_available = Path(sites_enabled.parent, 'sites-available')
            sites_available.mkdir(exist_ok=True, parents=True)
            with open(Path(sites_available, domain), 'w') as out:
                with open(Path(origin, 'nginx.conf'), 'r') as inp:
                    line = inp.readline()
                    while line:
                        line = line.replace('$DOMAIN', domain)
                        line = line.replace('$STATIC_PROD', str(static_prod) + '/')
                        line = line.replace('$MEDIA_PROD', media_prod + '/')
                        out.write(line)
                        line = inp.readline()
            # in sites_available add a link towards sites available
            if not Path(sites_enabled, domain).exists():
                Path(sites_enabled, domain).symlink_to(Path(sites_available, domain))
            # modify the default server page so that it displays a not found instead of the welcome to nginx
            default_nginx = Path(sites_enabled, 'default')
            shutil.copyfile(Path(origin, 'default'), default_nginx)

            # add a default file to display in case the website is unavailable
            default = config.getp('paths', 'www')
            default.mkdir(exist_ok=True, parents=True)
            with open(Path(default, 'index.html'), 'w') as out:
                with open(Path(origin, 'index.html'), 'r') as inp:
                    line = inp.readline()
                    line = line.replace('$DOMAIN', domain)
                    out.write(line)


def get_key_classes(module):
    """ returns the classes which are subclasses of Prod_dirs_files"""
    classes = inspect.getmembers(module, inspect.isclass)
    subclasses_names = [p.__name__ for p in Prod_dirs_files.__subclasses__()]
    childclasses = [c[1] for c in classes if c[1].__name__ in subclasses_names]
    return childclasses


def copy_repo_to_prod(config, install='new'):
    """ copy all files except statics from base repo to prod  according to configuration"""

    module = __import__(Path(__file__).stem)
    keyclasses = get_key_classes(module)
    for aclass in keyclasses:
        aclass.files_dirs(config, install)






