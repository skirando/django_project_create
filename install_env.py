#!/usr/bin/env python
# coding:utf-8
"""License
=======

 Copyright (C) <2022>  <Odile Troulet-Lambert> (odile.troulet-lambert.tex@orange.fr )
 and <Chritian Lambert> (vaujanycl@orange.fr).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



@author: Odile Troulet-Lambert Christian Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""


from pathlib import Path
import configparser as cfgparser
import sys
import os

import create_django_env.helpers as h
import create_django_env.dispatch_files as dispatch_files


def subparser_add_args(subparser):
    """ creates the parser"""

    subparser.add_argument('load',
                           choices=['DEFAULT', 'fix', 'dump', 'inspect'],
                           help="""DEFAULT does nothing,
                           fix to load the most recent fixture,
                           dump  to load a dump.sql file,
                           inspect to run an inspectdb""",
                           )
    subparser.add_argument('new_machine',
                           choices=['new', 'update'],
                           help=""" new for a first install on this machine; 
                           update for updating requirements and application software from repo, after a git pull from repo
                           does not modify env.cfg""",
                           )


def parse_args(subparser, arguments):
    return subparser.parse_args(arguments)


def inspect_db():
    """ create generic draft models.py"""
    # try:
        # completedProcess = subprocess.run([sysexec, '-m', 'manage.py', 'inspectdb'],
                                          # check=True,
                                          # capture_output=True,
                                          # encoding='utf-8'
                                          # )
    # except subprocess.CalledProcessError as err:
        # print(f'could not launch inspectdb')
        # print(f'error : {err.stderr}')
        # print(f'command ; {err.cmd}')
        # print(f'arguments : {err.args}')
        # print(f'{err.stdout}')
        # exit()
    from django.core import management
    with open('models.py', 'w') as file:
        management.call_command('inspectdb', stdout=file)
    print("""inspect_db created the model if the database had tables;
           check the model out, dont forget to put managed = True, move the model into your app
           then make migrations and migrate """)


def get_pwd(config):
    pwd_path = Path(config.getp('paths', 'conf'), 'pwd.txt')
    with open(pwd_path, 'r') as f:
        pwd = f.read()
    return pwd


def create_super_user(config):
    # user is the login who ran this script
    from django.core import management
    pwd = get_pwd(config)
    dbuser = config.get('users', 'user_admin')
    email = config.get('users', 'email_admin')
    os.environ['DJANGO_SUPERUSER_PASSWORD'] = pwd
    os.environ['DJANGO_SUPERUSER_USERNAME'] = dbuser
    os.environ['DJANGO_SUPERUSER_EMAIL'] = email
    try:
        management.call_command('createsuperuser', '--noinput')
        print(f'superuser {dbuser} created')
    except management.base.CommandError as e:
        if not 'already taken' in e.args[0]:
            raise e
        print(f'no super user created; kept existing')


def create_secret_key(project_root):
    from django.core.management import utils
    secret_key = utils.get_random_secret_key()
    key_file = Path(project_root, 'secret_key')
    with open(key_file, 'w') as f:
        f.write(secret_key)


def check_dump_dir(config, project_root):
    """ dump dir is in repo path"""
    try:
        dbtype = config.get('database', 'dbtype')
        if dbtype != 'mysql':
            raise cfgparser.NoOptionError
    except cfgparser.NoOptionError:
        print(f""" dump load is only possible for mysql database""")
        return None
    dumps_path = Path(project_root, 'dumps')
    dump_files = [f for f in dumps_path.iterdir() if f.is_file() and f.suffix == '.sql']
    if not dump_files or len(dump_files) > 1:
        print(f"""there is no dump files or there are several dump files in {dumps_path}; cannot choose;
        rename or remove non relevant files so there is only one .sql dumpfile""")
        return None
    else:
        return dump_files[0]


#def get_apps(config):
    #from django.apps import apps

    #def get_static_dir(appConfig):
        #d = appConfig.path
        #static_dir = [child for child in d.iterdir() if child.is_dir() and child.match('static')]
        #if static_dir:
            #return static_dir

    #apps = apps.get_app_configs()
    #apps_static = (get_static_dir(appConfig) for appConfig in apps)
    #config.apps_static = apps_static
    #return config


def mmain(namespace, cwd):
    """cwd is supposed to be base_dir_repo or top_dir"""

    import os
    base_dir = h.get_base_dir_repo(cwd)
    if not base_dir:
        return
    print(f'base_dir is {base_dir}')
    dic = vars(namespace)
    load = dic['load']
    install = dic['new_machine']
    project_root = h.get_project_root_repo(base_dir)
    if not project_root:
        return
    # use the repo env.cfg
    cfg_path = Path(project_root, 'env.cfg')
    config = h.PathConfigParser.create_from_file(cfg_path)
    env_type = config.get('configuration', 'env_type')
    venv_path = config.getp('paths', 'venv')
    top_dir = config.getp('repo_paths', 'top_dir')
    if env_type == 'prod' and not h.check_root():
        return
    try:
        virtual_env = os.environ['VIRTUAL_ENV']
    except KeyError:
        print(f'no virtual environment activated')
        package = Path(__file__).parent

        if env_type == 'prod':
            print(f"""to activate the virtual env enter :
            su root if you are not root
            cd {str(venv_path)}
            source bin/activate
            cd {str(top_dir)}
            {Path(venv_path, 'bin', 'python3')} {package} install_env """)
        else:
            print(f'''to activate virtual env, type :  {venvpath}/bin/activate''')
        return
    if load == 'dump':
        dump_file = check_dump_dir(config, project_root)
        if not dump_file:
            return
    print(f'virtual_env is {virtual_env}')
    create_secret_key(project_root)
    BASECONF = config.getp('paths', 'conf')
    BASEPROGS = config.getp('paths', 'progs')
    BASEREPO = config.getp('repo_paths', 'base_dir')

    req_path = Path(project_root, 'requirements')
    #check if requirements have been modified
    import shutil
    if install == 'update':
        if not Path(BASECONF, 'installed_requirements').exists():
            print('no installed requirements found; is this a new install?')
            return
        new_requirements = not h.is_same_stat(req_path, Path(BASECONF, 'installed_requirements'))
        if new_requirements:
            #delete old installed requirements
            print('requirements were modified; installing new requirements')
            shutil.rmtree(Path(BASECONF, 'installed_requirements'))
            shutil.copytree(req_path, Path(BASECONF, 'installed_requirements'))
            reqs = Path(req_path, env_type + '.txt')
            pipargs = ['install', '-r', str(reqs)]
            returncode = h.call_pip(venv_path, pipargs)
            if returncode != 0:
                if env_type == 'prod':
                    sudo = 'sudo'
                else:
                    sudo = ''
                print('''installation of requirements failed ; look into the output for detailed error messages. hints to help solve the issues:\n''')
                if config.get('database', 'dbtype') == 'mysql':
                    print('''check into package documentation mysql_and_other_dependencies for all LINUX packages required for using mysql''')
                print(f''' if a specific package was not installed, check compatibility constraints between the system python version and your requirements file''')
                print(f''' if necessary change your requirements file on your repository''')

    if env_type == 'prod':
        dispatch_files.copy_repo_to_prod(config, install=install)
        print('files dispatched for production environment')
        # we need to call django.setup to configure the apps
        #for collectstatic we want to use the apps in the repo as the static dirs of the apps are not copied by dispatch files
        #for migrations we want to use the apps in the progs dir because migrations might be created
        #thus we must call django setup twice, once with settings located in BASEREPO, another time with settings located in BASEPROG
        os.environ['DJANGO_SETTINGS_MODULE'] = Path(project_root).parts[-1] + '.settings'
        os.sys.path.append(str(BASEREPO))
        import django
        try:
            django.setup()
            print('django setup OK')
        except ModuleNotFoundError as ex:
            print(ex.args)
            print('''could not start django
            did you forget to add a package in your requirements''')
            return
        from django.core import management
        from django.core.management.base import CommandError
        print('collecting static files')
        try:
            management.call_command('collectstatic')
        except CommandError as ex:
            print(f'error in collecting static files {ex.args}')
            print(f'beware it is not possible to call collectstatic with manage.py in production environment')
            print(f'''you can call collectstatic from within the repo directory
            typing in the virtual env : python3 manage.py collectstatic ''')
        os.sys.path.remove(str(BASEREPO))

    """ we need now need to use settings from the production path thus need to modify sys.path"""
    os.environ['DJANGO_SETTINGS_MODULE'] = Path(BASECONF).parts[-1] + '.settings'
    os.sys.path.append(str(BASECONF.parent))
    os.sys.path.append(str(BASEPROGS))

    #import ipdb
    #ipdb.set_trace()
    #if not base_dir == top_dir:
        #os.sys.path.append(str(top_dir))
    #import pprint
    #pprint.pprint(sys.path)
    #django setup is necessary to configure the apps
    import django
    try:
        django.setup()
        print('django setup OK')
    except ModuleNotFoundError as ex:
        print(ex.args)
        print('''could not start django''')
        print('''did you forget to add a package in your requirements or do you have some other modules under the top_dir 
        or base_dir which are not yet installed?
        If so,  can either terminate manually the installation (make migrations and migrate and loading of the database)
        or you can erase the project directory and start again from repo_clone command''')
        return
    #if env_type == 'prod':
        #config = get_apps(config)
        #dispatch_files.copy_static_to_prod(config, install=install)

    if load == 'inspect':
        inspect_db()
        create_super_user(config)
        return

    from django.core import management
    management.call_command('makemigrations')
    print('done with makemigrations')
    management.call_command('migrate')
    print('migrations OK')

    if load != 'DEFAULT':
        print(f'load = {load}')
    from create_django_env.load_database import load_database_yaml_fixture, load_database_mysql_dump
    if load == 'fix':
        # get fixture from the repository
        base_dir = config.getp('repo_paths', 'base_dir')
        if env_type == 'prod':
            #do not load authentication fixture
            loadauth = False
        else:
            loadauth = True
        load_database_yaml_fixture(settings, Path(base_dir), loadauth=loadauth)
    elif load == 'dump':
        load_database_mysql_dump(config, dump_file, settings)
    # create super user
    create_super_user(config)
    if env_type != 'prod':

        print(f'''the environment is ready;
        type cd {base_dir}
        python3  manage.py runserver 0.0.0.0:8000 to start ''')
    elif install == 'update':
        project_name = config.get('pnames', 'project_name')
        print(f'update of software terminated restart gunicorn')
        print(f'''type in sudo service {project_name} stop
        sudo service {project_name}  start ''')
    else:
        django_user = config.get('users', 'django_user')
        venvpath = config.getp('paths', 'venv')
        #dns = config.get('host', 'dns')
        #if dns == True:
        domain = config.get('host', 'domain')
        #else:
            #domain = config.get('host', 'host_ip')
        baserepo = config.get('repo_paths', 'base_dir')
        #ipaddress = config.get('host', 'host_ip')

        print(f'''check now that your django project is correctly configured (without static files) 
        you can ignore warning about directory which does not exist in Staticfiles dir:
        deactivate
        go to settings directory: cd {BASECONF}
        systemctl stop nginx
        su {django_user}
        source {venvpath}/bin/activate
        python3 {BASEPROGS}/my_manage.py runserver 0.0.0.0:8000
        check in your browser http://{domain}
        type in CTRL C to stop the server
        
        type in :service nginx start 
        service nginx reload
        python3 {BASEPROGS}/my_manage.py runserver 0.0.0.0:8000
        and check in your browser http://{domain}
        you should see your application with statics
        
        Its now time to configure gunicorn
        sudo -i
        source {venvpath}/bin/activate
        cd {baserepo}
        python3  {Path(__file__).parent} conf_guni 
        ''')


if __name__ == '__main__':
    from pathlib import Path
    import argparse
    cwd = Path.cwd()
    parser = argparse.ArgumentParser()
    subparser_add_args(parser)
    ns = parse_args(parser, sys.argv[2:])

    mmain(ns, cwd)





